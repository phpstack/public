<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Layout;
use Core;
/**
 * Class manages generic function of the different layout classes, including the extra variables
 *
 * @package \Core\Library\Layout\Layout
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Generic extends Core\Library\BaseObject{
    /**
     * An array to hold all the models that are constructed in the init() method
     * @var array
     */
    private $models=array();
    
    /**
     * The model which holds the variables for the class extending this generic. Identified by tables ending in '_vars'
     * @var mixed
     */
    private $varModel=null;
    
    /**
     * Array to hold all the extra variables
     */
    private $vars=array();
    
    
    /**
     * Initialize the models defined in the config, and add them to the class
     * @return null
     */
    public function init(){
        $config = $this->getConfig();
        if(!empty($config)){
            $config = $this->load->library('ModuleConfig', array(null, $config));
            foreach($config->tables as $table){
                if(substr($table['name'],-5) == '_vars'){
                     $this->varModel = $this->load->moduleClass('Model', array($table['name'], null, $config), Core\Library\Loader::NAMESPACE_APPLICATION_CORE, 'Base', Core\Library\Loader::NAMESPACE_TYPE_MODEL);
                }else{
                    $this->{$table['name'].'_model'} = $this->load->moduleClass('Model', array($table['name'], null, $config), Core\Library\Loader::NAMESPACE_APPLICATION_CORE, 'Base', Core\Library\Loader::NAMESPACE_TYPE_MODEL);
                }
            }
        }
    }
    
    /**
     * Load the variables from the database and populate them in this object.
     * The parameter for this method is the ID from the database of the object to load variables for.
     * i.e. If it's a Layout object this is the layout id, if this is a Column object this is the column id, etc.
     * 
     * @return null
     * @param int The ID of the object for which to load the variables. 
     */
    public function loadVars($objectId){
        $q = $this->registry->load->database('Query', array($this->varModel->getTable()));
        $fieldName = '';
        switch(get_class($this)){
            case "Core\Library\Layout\Layout":
                $fieldName = 'layout_id';
                break;
            case "Core\Library\Layout\Row":
                $fieldName = 'layout_row_id';
                break;
            case "Core\Library\Layout\Column":
                $fieldName = 'layout_column_id';
                break;
        }
        if(!empty($fieldName)){
            $q->type($q::QUERY_TYPE_SELECT)
                ->fields('*')
                ->where(array(
                        array(
                            'field'=>$this->varModel->getTable()->getField($fieldName)->name,
                            'comparison'=>'=',
                            'value'=>$objectId
                        )
                    )
                );
            $records = $q->execute(false);
            if($records->numRecords() > 0){
                $this->vars = array();
                foreach($records as $record){
                    $name = $record->name;
                    if(array_key_exists($name, $this->vars)){
                        if(!is_array($this->vars[$name])){
                            $this->vars[$name] = array($this->vars[$name]);
                        }
                        $this->vars[$name][] = $record->value;
                    }else{
                        $this->vars[$name] = $record->value;
                    }
                }
            }
        }
    }
    
    /**
     * Set a variable to the layout object extending this class
     * @return mixed Return value is $this for chainability
     * @param string $name The name of the variable to set
     * @param array|string The value of the variable to set
     */
    public function setVar($name, $value){
        $this->vars[$name] = $value;
        return $this;
    }
    
    /**
     * get a variable for the layout object extending this class. If the variable does not exists, null will be returned
     * @return mixed|null
     * @param string $name The name of the variable to get
     */
    public function getVar($name){
        if(array_key_exists($name, $this->vars)){
            return $this->vars[$name];
        }else{
            return null;
        }
    }
    
    /**
     * Return all the variables for this object as an array
     * @return array
     */
    public function getVarsArray(){
        return $this->vars;
    }
    
    /**
     * Meant to be overloaded in the parent class. Added here to prevent problems
     * @return array();
     */
    protected function getConfig(){
        return array();
    }
    
    /**
     * Magic Method used to allow -> nomanclature for access to the models loaded. If the requested property does not end in '_model'
     * then the request will be passed to the parent object
     *
     * @return mixed
     * @param string $name The name of the property to get
     */
    public function __get($name){
        if(substr($name,-6) == '_model' && array_key_exists($name,$this->models)){
            return $this->models[$name];
        }else{
            return parent::__get($name);
        }
    }
    
    /**
     * Magic Method used to allow -> nomanclature for access to loaded models. If the requested property does not end in '_model'
     * then the request will be passed to the parent object
     *
     * @return mixed
     * @param string $name The name of the property to get
     */
    public function __set($name, $value){
        if(substr($name,-6) == '_model'){
            $this->models[$name] = $value;
        }else{
            parent::__set($name,$value);
        }
    }
}