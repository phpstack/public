<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Layout;
use Core;
/**
 * Class manages the layout for a particular page.
 *
 * @package \Core\Library\Layout\Layout
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Layout extends Core\Library\Layout\Generic{
    
    /**
     * An Array of the {@see \Core\Library\Layout\Row} objects belonging to this layout
     * @var \Core\Library\Layout\Row[]
     */
    private $rows=array();
    
    /**
     * The name of the layout. Available after the layout is loaded
     * @var string
     */
    private $name=null;
    
    /**
     * The page from which this layout was loaded
     * @var string
     */
    private $page=null;
    
    /**
     * Add a row to this layout
     * @return \Core\Library\Layout\Layout
     * @param \Core\Library\Layout\Row $row The row to add to this layout
     */
    public function addRow($row){
        $this->rows[] = $row;
        end($this->rows);
        $index = key($this->rows);
        $this->rows[$index]->setIndex($index);
        $this->rows[$index]->setLayout($this);
        return $this;
    }
    
    /**
     * Get the HTML for this layout
     * @return string The HTML output for this layout
     */
    public function getHTML(){
        $html = '<div class="container">';
        foreach($this->rows as $row){
            $html .= $row->getHTML();
        }
        $html .= '</div>';
        return $html;
    }
    
    /**
     * Load a layout from the DB by it's ID number. If an ID number is not provided, a basic layout will be loaded consisting of a single row and a single column
     * @return \Core\Library\Layout\Layout return value is $this for chainablity
     * @param int $id (optional) The ID number to load. If not provided, a basic layout will be loaded consisting of a single row and a single column
     */
    public function load($id=null){
        $record = null;
        if($id!=null){
            $rowModel = $this->load->library('Layout\Row')->getLayoutRowModel();
            $q = $this->registry->load->database('Query', array($this->layout_model->getTable()));
            $q->type($q::QUERY_TYPE_SELECT)
                ->fields('*')
                ->where(array(
                        array(
                            'field'=>$this->layout_model->getTable()->getPrimaryField()->name,
                            'comparison'=>'=',
                            'value'=>$id
                        )
                    )
                )
                ->join($rowModel->getTable(),$this->layout_model->getTable()->getPrimaryField(), $rowModel->getTable()->getField('layout_id'),'rows');
            $records = $q->execute(true);
            if($records->numRecords() == 1){
                $this->loadVars($id);
                $record = $records->current();
                $this->name = $record->name;
            }
        }
        if($record!=null){
            $rows = $record->getRelatedRecords('rows');
            if(count($rows) > 0){
                foreach($rows as $r){
                    $this->addRow($this->load->library('Layout\Row')->setLayout($this)->load($r->getPrimaryFieldValue()));
                }
            }else{
                $this->addRow($this->load->library('Layout\Row')->setLayout($this)->load());
            }
        }else{
            $this->addRow($this->load->library('Layout\Row')->setLayout($this)->load());
        }
        return $this;
    }
    
    /**
     * Get the name of the loaded layout
     * @return string|null The name of the loaded layout, or null if a layout isn't loaded
     */
    public function getName(){
        return $this->name;
    }
    
    /**
     * Set the page to which this layout belongs
     * @return \Core\Library\Layout\Layout return value is $this for chainablity
     * @param \Core\Module\Base\Anonymous\Page\Page $page The page to which this layout belongs
     */
    public function setPage($page){
        if(is_object($page) && (get_class($page) == 'Core\Module\Base\Anonymous\Page\Page' || is_subclass_of($page, 'Core\Module\Base\Anonymous\Page\Page'))){
            $this->page = $page;
        }else{
            throw new \Exception("The page for a layout must be an object of the type Core\Module\Base\Anonymous\Page\Page or an object which overloads it");
        }
        return $this;
    }
    
    /**
     * Get the page to which this layout belongs
     * @return \Core\Module\Base\Anonymous\Page\Page
     */
    public function getPage(){
        return $this->page;
    }
    
    /**
     * Get the config array needed to construct a {@see \Core\Library\Layout\Layout} object
     * @return array An array holding the config needed to create a {@see \Core\Library\Layout\Layout} object
     */
    protected function getConfig(){
        $config = array(
        //############################################## TABLES ##############################################
            'tables'=>array(
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ layout ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                'layout'=>array(
                    'name'      => 'layout',
                    'indexes'   => array(),
                    'relations' => array(),
                    'fields'                => array(
                        'id'                => array('name' => 'id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => true,'primary' => true,'unique' => true),
                        'name'              => array('name' => 'name','datatype' => 'varchar','size' => '255','null' => false,'auto_inc' => false,'primary' => false,'unique' => true),
                    )
                ),
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ layout_to_route ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                'layout_to_route'=>array(
                    'name'      => 'layout_to_route',
                    'indexes'   => array(),
                    'relations' => array(
                        array(
                            'name'          => 'routes',
                            'table'         => 'route',
                            'local_field'   => 'route_id',
                            'remote_field'  => 'id',
                        ),
                        array(
                            'name'          => 'layouts',
                            'table'         => 'layout',
                            'local_field'   => 'layout_id',
                            'remote_field'  => 'id',
                        )
                    ),
                    'fields'    => array(
                        'id'        => array('name' => 'id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => true,'primary' => true,'unique' => true),
                        'route_id'  => array('name' => 'route_id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'layout_id' => array('name' => 'layout_id','datatype' => 'int','size' => '11','null' => true,'auto_inc' => false,'primary' => false,'unique' => false)
                    )
                ),
                'layout_vars'=>array(
                    'name'      => 'layout_vars',
                    'indexes'   => array(),
                    'relations' => array(),
                    'fields'                => array(
                        'id'                => array('name' => 'id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => true,'primary' => true,'unique' => true),
                        'name'              => array('name' => 'name','datatype' => 'varchar','size' => '255','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'value'             => array('name' => 'value','datatype' => 'varchar','size' => '255','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'layout_id'         => array('name' => 'layout_id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                    )
                ),
            )
        );
        return $config;
    }
    
    /**
     * Get the model object for the 'layout_to_route' table
     * @return \Core\Module\Base\Anonymous\Model\Model The specific model for the layout_to_route table
     */
    public function getLayoutToRouteModel(){
        return $this->layout_to_route_model;
    }
    
    /**
     * Convert the object into an array with the important properties
     * @return array An array representation of this object
     */
    public function toArray(){
        $data = array(
            'name'=>$this->name,
            'vars'=>$this->getVarsArray(),
            'rows'=>array()
        );
        foreach($this->rows as $r){
            $data['rows'][$r->getIndex()] = $r->toArray();
        }
        return $data;
    }
}