<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Layout;
use Core;
/**
 * Class manages the layout for a particular column.
 * 
 *
 * @package \Core\Library\Layout\Column
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Column extends Core\Library\Layout\Generic{
    /**
     * The index of this column in the row
     * @var int
     */
    private $index;
    
    /**
     * The layout this column belongs to
     * @var \Core\Library\Layout\Layout
     */
    private $layout;
    
    /**
     * The row in the layout that this column belongs to
     * @var \Core\Library\Layout\Row
     */
    private $row;
    
    /**
     * The Core\Library\Http\Action object which has the page and method which will build the content for this column
     * @var \Core\Module\Base\Anonymous\Block\Block|\Core\Library\Layout\Layout|string
     */
    private $content;
    
     /**
     * The number of bootstrap columns that this column object will occupy in the row
     * @var int
     */
    private $numCols;
    
    /**
     * Load the column from the database with an ID of $id. If $id is not provided, column default of 12 rows will be added.
     * @return \Core\Library\Layout\Column The return value is $this for chainability
     * @param int $id The ID from the 'layout_column' database table to load
     */
    public function load($id=null){
        $record = null;
        if($id!=null){
            $q = $this->registry->load->database('Query', array($this->layout_column_model->getTable()));
            $q->type($q::QUERY_TYPE_SELECT)
                ->fields('*')
                ->where(array(
                        array(
                            'field'=>$this->layout_column_model->getTable()->getPrimaryField()->name,
                            'comparison'=>'=',
                            'value'=>$id
                        )
                    )
                );
            //$this->print_array($q->getStatement(true));
            $records = $q->execute(true);
            if($records->numRecords() == 1){
                $record = $records->current();
            }
        }
        if($record != null){
            $this->loadVars($id);
            //Load config for this column
            $c = $record->content;
            
            if($this->load->classExists($c)){
                $classInfo = $this->load->getClassInfo($c);
                $construct = array();
                if($classInfo['namespaces']['type'] == Core\Library\Loader::NAMESPACE_TYPE_BLOCK){
                    $construct[] = $this->getRow()->getLayout()->getPage();
                }
                $class = $this->load->loadClass($c, $construct);
                if(is_object($class) && method_exists($class,'getHTML') && is_callable(array($class, 'getHTML'))){
                    $this->setContent($class);
                }else{
                    $this->setContent($c);
                }
            }else{
                $this->setContent($c);
            }
            $width = $record->num_cols;
            if(!empty($width)){
                $this->setColumnWidth($width);
            }else{
                $this->setColumnWidth(12);
            }
            $index = $record->index;
            if(!empty($index)){
                $this->setIndex($index);
            }
        }else{
            //Load the default options for this column
            $this->setContent('');
            $this->setColumnWidth(12);
        }
        return $this;
    }
    
    /**
     * Get the config array needed to construct a {@see \Core\Library\Layout\Column} object
     * @return array An array holding the config needed to create a {@see \Core\Library\Layout\Column} object
     */
    protected function getConfig(){
        $config = array(
        //############################################## TABLES ##############################################
            'tables'=>array(
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ layout_column ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                'layout_column' => array(
                    'name'      => 'layout_column',
                    'indexes'   => array(),
                    'relations' => array(),
                    'fields'    =>array(
                        'id'                => array('name' => 'id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => true,'primary' => true,'unique' => true),
                        'layout_row_id'     => array('name' => 'layout_row_id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'index'             => array('name' => 'index','datatype' => 'int','size' => '11','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'name'              => array('name' => 'name','datatype' => 'varchar','size' => '255','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'content'           => array('name' => 'content','datatype' => 'varchar','size' => '255','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                    )
                ),
                'layout_column_vars'=>array(
                    'name'      => 'layout_column_vars',
                    'indexes'   => array(),
                    'relations' => array(),
                    'fields'                => array(
                        'id'                => array('name' => 'id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => true,'primary' => true,'unique' => true),
                        'name'              => array('name' => 'name','datatype' => 'varchar','size' => '255','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'value'             => array('name' => 'value','datatype' => 'varchar','size' => '255','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'layout_column_id'  => array('name' => 'layout_column_id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                    )
                ),
            )
        );
        return $config;
    }
    
    /**
     * Get the model for the 'layout_column' table
     * @return \Core\Module\Base\Anonymous\Model\Model
     */
    public function getLayoutColModel(){
        return $this->layout_column_model;
    }
    
    /**
     * Set the index number of this column in the row
     * 
     * @return \Core\Library\Layout\Column Return value is $this
     * @param int $index The index of this column in the row
     */
    public function setIndex($index){
       $this->index = $index; 
    }
    
    /**
     * Get the index number of this column in the row
     * @return int
     */
    public function getIndex(){
        return $this->index;
    }
    
    /**
     * Set the layout object to which this column belongs.
     *
     * @return \Core\Library\Layout\Column Return value is $this
     * @param \Core\Library\Layout\Layout The layout object to which this column belongs
     */
    public function setLayout($layout){
        $this->layout = $layout;
        return $this;
    }
    
    /**
     * Set the row object to which this column belongs.
     *
     * @return \Core\Library\Layout\Column Return value is $this
     * @param \Core\Library\Layout\Row The row object to which this column belongs
     */
    public function setRow($row){
        $this->row = $row;
        return $this;
    }
    
    /**
     * Get the row to which this column belongs
     * @return \Core\Library\Layout\Row
     */
    public function getRow(){
        return $this->row;
    }
    
    /**
     * The object which has the page and method which will build the content for this column.
     *
     * There are 3 valid types of content:
     * 1. Core\Library\Http\Action object. This object contains the Core\Module\Base\Anonymous\Block\Block object and method which will generate the markup for this column
     * 2. \Core\Library\Layout\Layout. A nested layout object
     * 3. A string of HTML markup. Statically set the output for this column.
     *
     * @return \Core\Library\Layout\Column The return value is $this
     * @param \Core\Library\Http\Action|\Core\Library\Layout\Layout|string $content The action object containing the page and method which will produce the output for this column
     */
    public function setContent($content){
        if(!(is_object($content) && (is_subclass_of($content,'Core\Library\Layout\Layout') || get_class($content) == 'Core\Library\Layout\Layout')) &&
           !(is_object($content) && (is_subclass_of($content,'Core\Module\Base\Anonymous\Block\Block') || get_class($content) == 'Core\Module\Base\Anonymous\Block\Block')) &&
           !is_string($content)){
            throw new \Exception("Invalid content type set for the column at index '".$this->index."'");
        }
        $this->content = $content;
        return $this;
    }
    
    /**
     * Set the number of bootstrap columns that this column object will occupy
     * @return \Core\Library\Layout\Column
     * @param int $width The number of bootstrap columns for this object
     */
    public function setColumnWidth($width){
        $this->numCols = $width;
        return $this;
    }
    
    /**
     * Get the HTML for this column
     * @return string The HTML output for this column
     */
    public function getHTML(){
        if(empty($this->numCols)){
            $this->setColumnWidth(12);
        }
        $html = '<div class="col-'.$this->numCols.'" data-index="'.$this->index.'">';
        $output = '';
        
        if(is_object($this->content)){
            $output = $this->content->getHTML();
        }elseif(is_string($this->content)){
            $output = $this->content;
        }else{
            throw new \Exception("Invalid content type set for the column at index '".$this->index."'");
        }
        
        $html .= $output;
        $html .= '</div>';
        return $html;
    }
    
    /**
     * Convert the object into an array with the important properties
     * @return array An array representation of this object
     */
    public function toArray(){
        $data = array(
            'index'=>$this->index,
            'column_width'=>$this->numCols,
            'vars'=>$this->getVarsArray(),
        );
        if(is_object($this->content)){
            $data['renderer'] = array(
                'class'=>get_class($this->content),
                'data'=>$this->content->toArray()
            );
        }elseif(is_string($this->content)){
            $data['renderer'] = array(
                'class'=>'static',
                'data'=>htmlspecialchars($this->content)
            );
        }elseif($this->content == null){
            $data['renderer'] = array(
                'class'=>'NOT SET',
                'data'=>''
            );
        }else{
            
            $data['renderer'] = array(
                'class'=>'INVALID',
                'data'=>''
            );
        }
        return $data;
    }
}