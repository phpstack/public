<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Layout;
use Core;
/**
 * Class manages the layout for a particular page.
 *
 * @package \Core\Library\Layout\Layout
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Row extends Core\Library\Layout\Generic{
    /**
     * The index of this row in the layout
     * @var int
     */
    private $index;
    
    /**
     * The layout this row belongs to
     * @var \Core\Library\Layout\Layout
     */
    private $layout;
    
    /**
     * An Array of the {@see \Core\Library\Layout\Column} objects belonging to this row
     * @var \Core\Library\Layout\Column[]
     */
    private $cols=array();
    
    /**
     * Get the model for the layout_row table
     * @return \Core\Module\Base\Anonymous\Model\Model The specific model for the layout_row table
     */
    public function getLayoutRowModel(){
        return $this->layout_row_model;
    }
    
    /**
     * Get the config array needed to construct a {@see \Core\Library\Layout\Row} object
     * @return array An array holding the config needed to create a {@see \Core\Library\Layout\Row} object
     */
    protected function getConfig(){
        $config = array(
        //############################################## TABLES ##############################################
            'tables'=>array(
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ layout_column ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                'layout_row' => array(
                    'name'      => 'layout_row',
                    'indexes'   => array(),
                    'relations' => array(),
                    'fields'    =>array(
                        'id'        => array('name' => 'id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => true,'primary' => true,'unique' => true),
                        'layout_id' => array('name' => 'layout_id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'index'     => array('name' => 'index','datatype' => 'int','size' => '11','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'name'      => array('name' => 'name','datatype' => 'varchar','size' => '255','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                    )
                ),
                'layout_row_vars'=>array(
                    'name'      => 'layout_row_vars',
                    'indexes'   => array(),
                    'relations' => array(),
                    'fields'                => array(
                        'id'                => array('name' => 'id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => true,'primary' => true,'unique' => true),
                        'name'              => array('name' => 'name','datatype' => 'varchar','size' => '255','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'value'             => array('name' => 'value','datatype' => 'varchar','size' => '255','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'layout_row_id'     => array('name' => 'layout_row_id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                    )
                ),
            )
        );
        return $config;
    }
    
    /**
     * Load the row from the database with an ID of $id. If $id is not provided, a single row will be loaded
     * @return \Core\Library\Layout\Row Return value is $this for chainability
     * @param int $id The ID from the 'layout_row' database table to load
     */
    public function load($id=null){
        $record = null;
        if($id!=null){
            $colModel = $this->load->library('Layout\Column')->getLayoutColModel();
            $q = $this->registry->load->database('Query', array($this->layout_row_model->getTable()));
            $q->type($q::QUERY_TYPE_SELECT)
                ->fields('*')
                ->where(array(
                        array(
                            'field'=>$this->layout_row_model->getTable()->getPrimaryField()->name,
                            'comparison'=>'=',
                            'value'=>$id
                        )
                    )
                )
                ->join($colModel->getTable(),$this->layout_row_model->getTable()->getPrimaryField(), $colModel->getTable()->getField('layout_row_id'),'cols')
                ->orderBy($colModel->getTable()->getField('index'));
            $records = $q->execute(true);
            if($records->numRecords() == 1){
                $record = $records->current();
            }
        }
        if($record != null){
            $this->loadVars($id);
            $cols = $record->getRelatedRecords('cols');
            if(count($cols) > 0){
                foreach($cols as $c){
                    $this->addCol($this->load->library('Layout\Column')->setRow($this)->load($c->getPrimaryFieldValue()));
                }
            }else{
                $this->addCol($this->load->library('Layout\Column')->setRow($this)->load());
            }
        }else{
            $this->addCol($this->load->library('Layout\Column')->setRow($this)->load());
        }
        return $this;
    }
    
    /**
     * Add a column to this layout
     * 
     * @return \Core\Library\Layout\Row
     * @param \Core\Library\Layout\Column $col The column to add to this row
     */
    public function addCol($col){
        $this->cols[] = $col;
        end($this->cols);
        $index = key($this->cols);
        $this->cols[$index]->setIndex($index);
        $this->cols[$index]->setLayout($this->layout);
        $this->cols[$index]->setRow($this);
        
        return $this;
    }
    
    /**
     * Set the index number of this row in the layout
     * 
     * @return \Core\Library\Layout\Row Return value is $this
     * @param int $index The index of this row in the layout
     */
    public function setIndex($index){
       $this->index = $index; 
    }
    
    /**
     * Get the index number of this row in the layout
     * @return int
     */
    public function getIndex(){
        return $this->index;
    }
    
    /**
     * Set the layout object to which this row belongs. All columns belonging to this row will also have their layout updated.
     *
     * @return \Core\Library\Layout\Row Return value is $this
     * @param \Core\Library\Layout\Layout The layout object to which this row belongs
     */
    public function setLayout($layout){
        $this->layout = $layout;
        foreach($this->cols as $index=>$col){
            $this->cols[$index]->setLayout($layout);
        }
        return $this;
    }
    
    /**
     * Get the layout object to which this row belongs.
     * @return \Core\Library\Layout\Layout
     */
    public function getLayout(){
        return $this->layout;
    }
    
    /**
     * Get the HTML for this row
     * @return string The HTML output for this row
     */
    public function getHTML(){
        $html = '<div class="row">';
        foreach($this->cols as $col){
            $html .= $col->getHTML();
        }
        $html .= '</div>';
        return $html;
    }
    
    /**
     * Convert the object into an array with the important properties
     * @return array An array representation of this object
     */
    public function toArray(){
        $data = array(
            'index'=>$this->index,
            'vars'=>$this->getVarsArray(),
            'cols'=>array()
        );
        foreach($this->cols as $c){
            $data['cols'][$c->getIndex()] = $c->toArray();
        }
        return $data;
    }
    
}