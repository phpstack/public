<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Database;
use Core;
/**
 * A generic wrapper class for standard access to the database.
 *
 * This class loads the driver defined during construction. All subsequent calls to methods in this class are passed to the driver.
 * This allows the framework to remain database independent by standardizing access to the database.
 *
 * @package \Core\Library\Database\DB
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class DB extends Core\Library\BaseObject{
    /**
     * The name of the driver to use. Sent in the {@see \Core\Library\Database\DB::__construct()} method
     */
    private $driverName;
    /**
     * The Driver to use for database access. The driver must extend \Core\Library\Database\Driver\PDOWrapper and implement \Core\Library\Database\Driver\PDOInterface
     * @var mixed
     */
    public $driver;
    
    /**
     * Creates a new instance.
     *
     * @see \Core\Library\Database\DB::loadDriver()
     * 
     * @return \Core\Library\Database\DB
     * @param string $driver The class name of a valid Driver.
     */
    public function __construct($driver){
        $this->driverName = $driver;
        
    }
    
    /**
     * Initialize the driver
     *
     * @return null
     */
    public function init(){
        $this->loadDriver($this->driverName);
    }
    
    /**
     * Loads the $driver, and then calls the drivers loadTables method
     *
     * Note: The driver must extend \Core\Library\Database\Driver\PDOWrapper and implement \Core\Library\Database\Driver\PDOInterface
     *
     * @return null
     * @param string A valid drvier class name.
     */
    public function loadDriver($driver){
        $this->driver = $this->registry->load->database('Driver\\'.$driver, array($this, DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE));
        $this->driver->loadTables(true);
    }
    
    /**
     * Get the JOIN_DELIMITER constant defined in the database driver
     *
     * @return string The delimiter string for joined data
     */
    public function getJoinDelimiter(){
        return constant(get_class($this->driver).'::JOIN_DELIMITER');
    }
    
    /**
     * Access driver's properties with -> nomanclature
     *
     * @return mixed
     * @param string $key The driver's property to return
     */
    public function __get($key){
        if(property_exists($this->driver,$key)){
            return $this->driver->$key;
        }else{
            throw new \Exception("Property '".$key."' does not exist on '".get_class($this->driver)."'");
        }
    }
    
    /**
     * Access to set the driver's properties
     *
     * @return null
     * @param string $key The name of the property to set
     * @param mixed $value The value to set the property to
     */
    public function __set($key, $value){
        if(property_exists($this->driver,$key)){
            $this->driver->$key = $value;
        }else{
            throw new \Exception("Property '".$key."' does not exist on '".get_class($this->driver)."'");
        }
    }
    
    /**
     * Access to call the driver's methods
     *
     * @return mixed Return value of the called method
     * @param string $function The name of the method to call on the driver
     * @param array $args An array of arguments to be passed to the method
     */
    public function __call($function, $args=array()){
        if(method_exists($this->driver, $function) && is_callable(array($this->driver, $function))){
            if(count($args) == 1){
                return $this->driver->$function($args[0]);
            }elseif(count($args) == 2){
                return $this->driver->$function($args[0], $args[1]);
            }elseif(count($args) == 3){
                return $this->driver->$function($args[0], $args[1], $args[2]);
            }elseif(count($args) == 4){
                return $this->driver->$function($args[0], $args[1], $args[2], $args[3]);
            }else{
                return call_user_func_array(array($this->driver, $function), $args);
            }
        }else{
            throw new \Exception("Method '".$function."' does not exist on '".(is_object($this->driver) ? get_class($this->driver) : 'null')."'");
        }
    }
}
?>