<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Database;
use Core;
/**
 * An object that represents a single field in a table.
 *
 * This object does not contain any data from the table's records, but instead represents the field structure.
 *
 * @package \Core\Library\Database\Field
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Field extends Core\Library\BaseObject{
	/**
     * The \Core\Library\Database\Table object to which this field belongs
     * @var \Core\Library\Database\Table
     */
	public $table;
	/**
     * A list of indexes that this field is a part of
     * @var array
     */
	public $indexes=null;
	/**
	 * The name of this field
	 * @var string
	 */
	public $name;
	/**
	 * Boolean indicating if this field allows null values
	 * @var bool
	 */
	public $null;
	/**
	 * The datatype of this field
	 * @var string
	 */
	public $datatype;
	/**
	 * The size of this field
	 * @var int
	 */
	public $size;
	/**
	 * Boolean determining if this field is an auto_increment field
	 * @var bool
	 */
	public $auto_inc;
	/**
	 * Boolean indicating if this field must be unique
	 * @var bool
	 */
	public $unique;
	/**
	 * Boolean indicating if this field is a PRIMARY index field
	 * @var bool
	 */
	public $primary;
	
	/**
	 * If this is an enum type field, this defines the allowed values
	 * @var array
	 */
	public $values=array();
    
	/**
	 * Creates a new Instance.
	 *
	 * @return \Core\Library\Database\Field
	 * @param \Core\Library\Database\Table $table The table object to which this field belongs
	 * @param string $name The name of this field.
	 */
    public function __construct($table, $name){
        $this->table = $table;
        $this->name = $name;
    }
	
	/**
	 * Returns an array of \Core\Library\Database\Index objects representing all the indexes for this field
	 *
	 * @return \Core\Library\Database\Index[]
	 */
	public function getIndexes(){
		if($this->indexes===null){
			//strange as it seems, the database driver object will fill these in, when we request the indexes
			//I mean, we COULD query the db again for this information, but we may as well just use the driver's method...
			$indexes = $this->table->db->getIndexes($this->table->name);
			$this->indexes = array();
			foreach($indexes as $index){
				if($index->column_name == $this->name){
					$this->indexes[] = $index;
				}
			}
		}
		return $this->indexes;
	}
	
	/**
	 * Returns an array of \Core\Library\Database\Index objects representing all the UNIQUE indexes for this field
	 *
	 * @return \Core\Library\Database\Index[]
	 */
	public function getUniqueIndexes(){
		$rtn = array();
		foreach($this->getIndexes() as $index){
			if($index->unique){
				$rtn[] = $index;
			}
		}
		return $rtn;
	}
	
	/**
	 * Get the table object to which this field belongs
	 * @return \Core\Library\Database\Table The table to which this field belongs
	 */
	public function getTable(){
		return $this->table;
	}
	
}
?>