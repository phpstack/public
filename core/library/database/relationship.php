<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Database;
use Core;
/**
 * An object representing the relationship between two \Core\Library\Database\Table objects.
 *
 * Each relationship defined in the modules config.php file will have a unique name, and this class will represent one
 * of those relationships.
 *
 * @package \Core\Library\Database\Table
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Relationship extends Core\Library\BaseObject{
    /**
     * The local table object. Pulled from the $record property used to construct this class.
     * @var \Core\Library\Database\Table
     */
    private $localTable;
    /**
     * The table object which is related to the $localTable property. Defined in the modules config.php file.
     * @var \Core\Library\Database\Table
     */
    private $remoteTable;
    /**
     * The field from the $localTable property which is used to relate to the $remoteField on the $remoteTable property
     * @var \Core\Library\Database\Field
     */
    private $localField;
    /**
     * The field from the $remoteTable property which is used to relate to the $localField on the $localTable property
     * @var \Core\Library\Database\Field
     */
    private $remoteField;
    
    /**
     * The name of the relationship that is loaded. This is not set until the load() method is called.
     * @var string
     */
    public $name;
    
    /**
     * Creates a new instance of the relationship.
     *
     * @return \Core\Library\Database\Relationship
     * @param \Core\Library\Database\Table $table The table to which this relationship applies.
     * @param string $name The name of the relationship (from the module's config.php)
     */
    public function __construct($table, $name, $init=true){
        $this->name = $name;
        $this->localTable = $table;
        if($init){
            $this->init();
        }
    }
    
    /**
     * Initialize the relationship using the config from the table
     *
     * @return null
     */
    public function init(){
        $config = $this->localTable->getConfig();
        
        $pos = array_search($this->name, array_column($config['relations'],'name'));
        if($pos !== false){
            $keys = array_keys($config['relations']);
            $key = $keys[$pos];
            $config = $config['relations'][$key];
            $this->localField = $this->localTable->getField($config['local_field']);
            $this->remoteTable = $this->localTable->getDB()->getTable($config['table'], (!empty($this->localTable->getModel()) ? $this->localTable->getModel() : null));
            $this->remoteField = $this->remoteTable->getField($config['remote_field']);
        }
    }
    
    /**
     * Set the remoteTable for this relationship. Returns $this for chainability
     * @param \Core\Library\Database\Table|string $table The remote table object, or name to which this relationship applies.
     * @return \Core\Library\Database\Relationship
     */
    public function setRemoteTable($table){
        if(!is_object($table)){
            $table = $this->load->database('Table', array($this->localTable->getDB(),$table));
        }
        $this->remoteTable = $table;
        return $this;
    }
    
    /**
     * Set the remoteField for this relationship. Returns $this for chainability
     * @param \Core\Library\Database\Field|string $table The remote field object, or name to which this relationship applies.
     * @return \Core\Library\Database\Relationship
     */
    public function setRemoteField($field){
        if(!is_object($field)){
            if(!empty($this->remoteTable)){
                $field = $this->load->database('Field',array($this->remoteTable, $field));
            }else{
                throw new \Exception("When setting the remote field of a relationship using it's name, the remote table must be set first.");
            }
        }
        $this->remoteField = $field;
        return $this;
    }
    
    /**
     * Set the localField for this relationship. Returns $this for chainability
     * @param \Core\Library\Database\Field|string $table The local field object, or name to which this relationship applies.
     * @return \Core\Library\Database\Relationship
     */
    public function setLocalField($field){
        if(!is_object($field)){
            $field = $this->load->database('Field',array($this->localTable, $field));
        }
        $this->localField = $field;
        return $this;
    }
    
    /**
     * Return the local table for this relationship
     *
     * @return \Core\Library\Database\Table
     */
    public function getLocalTable(){
        return $this->localTable;
    }
    
    /**
     * Return the local field for this relationship
     *
     * @return \Core\Library\Database\Field
     */
    public function getLocalField(){
        return $this->localField;
    }
    
    /**
     * Return the remote table for this relationship
     *
     * @return \Core\Library\Database\Table
     */
    public function getRemoteTable(){
        return $this->remoteTable;
    }
    
    /**
     * Return the local field for this relationship
     *
     * @return \Core\Library\Database\Field
     */
    public function getRemoteField(){
        return $this->remoteField;
    }
    
    /**
     * Convert this relationship to an array
     * @return array An array of key=>value pairs representing this relationship
     */
    public function toArray(){
        return array(
            'name'=>$this->name,
            'local_field'=>$this->localField->name,
            'local_table'=>$this->localTable->name,
            'remote_table'=>$this->remoteTable->name,
            'remote_field'=>$this->remoteField->name,
            'local_table_config'=>$this->localTable->getConfig()
        );
    }
}
?>