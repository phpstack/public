<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Database\Driver;
/**
 * An interface for implementing PDO drivers
 *
 * A PDO Driver must extend \Core\Library\Database\Driver\PDOWrapper and implement this interface in order to be compatible with the Framework. These methods
 * all have some aspect of direct communication with the database server and therefore must be database specific. The implementation of this
 * interface is the only place where database specific queries should be housed. Maintaining this convention will ensure that the entire system
 * remains database independant.
 *
 * @see \Core\Library\Database\Driver\PDOWrapper
 *
 * @package \Core\Library\Database\Driver\PDOInterface
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
interface PDOInterface{
    /**
     * Queries the DB and returns an array containing information about a table
     *
     * @return array
     * @param string $tableName The name of the table to retrieve information about
     */
    public function describeTable($tableName);
    
    /**
     * Method returns a valid PDO connection which is available for use at $this->connection
     *
     * @return \PDO
     * @param string $host The hostname of the database server
     * @param string $user The user name for the database server
     * @param string $password The password for the database server
     * @param string $database The name of the database to load
     */
    public function getConnection($host, $user, $password, $database);
    
    /**
     * Queries the database and returns information about the indexes on $tableName
     *
     * @return array Information about the indexes on the table
     * @param string $tableName The name of the table for which to get index information
     */
    public function showIndex($tableName);
    
    /**
     * Get an array of all the table names in the database
     *
     * @return array List of table names in the database
     */
    public function showTables();
    
    /**
     * Determine if a field has a unique index
     *
     * @return bool
     * @param string $tableName The name of the table to which the field belongs
     * @param string $fieldName The name of the field in $tableName to test for uniqueness
     */
    public function isFieldUnique($tableName, $fieldName);
    
    /**
     * Get an array of \Core\Library\Database\Field objects
     * If $model is sent, it will be passed to the \Core\Library\Database\Table constructor
     *
     * @return \Core\Library\Database\Field[]
     * @param string $tableName The name of the table to get the fields from
     * @param \Core\Library\Model $model The \Core\Library\Model object to be used in constructing \Core\Library\Database\Field objects
     */
    public function getFields($tableName, $model);
    
    /**
     * Determine if $tableName exists in the database
     *
     * @retrun bool
     * @param string $tableName The name of the table to check
     */
    public function tableExists($tableName);
    
    /**
     * Get the query that needs to be executed to create a new table.
     *
     * @return string The query to execute to create $table
     * @param \Core\Library\Database\Table $table A table object that will be created by the query returned
     */
    public function createTableQuery($table);
    
    /**
     * Get an array of index objects for $tableName
     * Index::objTable should be assigned if it is available.
     *
     * @return \Core\Library\Database\Index[]
     * @param string $tableName The name of the table for which to retrieve the indexes
     */
    public function getTableIndexes($tableName);
    
    /**
     * Add $field to $tableName in the database
     *
     * @return null
     * @param string $tableName The name of the table to which $field should be added
     * @param \Core\Library\Database\Field $field An object describing the field that is to be added
     */
    public function addField($tableName, $field);
    
    /**
     * Remove $fieldName from $tableName in the database
     *
     * @return null
     * @param string $tableName The name of the table that $fieldName belongs
     * @param string $fieldName The name of the field to be removed
     */
    public function removeField($tableName, $fieldName);
    
    /**
     * Update the $field in $tableName so it matches the $field object
     * If $field does not exist on $tableName, it will be added
     *
     * @return null
     * @param string $tableName The name of the table to which $field belongs
     * @param \Core\Library\Database\Field An object describing the shape of the field.
     */
    public function updateField($tableName, $field);
    
    /**
     * Add the $index object to $tableName if it doesn't exist
     *
     * @return null
     * @param string $tableName The name of the table to which $index should be added
     * @param \Core\Library\Database\Index $index An object describing the index that should be added to $tableName
     */
    public function addIndex($tableName, $index);
    
    /**
     * Remove $index from $tableName
     *
     * @return null
     * @param string $tableName The name of the table from which to drop $index
     * @param \Core\Library\Database\Index The index object that is to be dropped from $tableName
     */
    public function removeIndex($tableName, $index);
    
    /**
     * Rename the index on $tableName from $oldName to $newName
     *
     * @return null
     * @param string $tableName The name of the table where the index resides
     * @param string $oldName The current name of the index to be renamed
     * @param string $newName The new name of the index
     */
    public function renameIndex($tableName, $oldName, $newName);
    
    /**
     * Return the total number of records returned by the $sql query
     * Since PDO MySQL is not capable of reliably determining the number of affected records for a query, we must implement this method
     *
     * @return int
     * @param string $sql The SQL query for which to find the number of records
     */
    public function totalRecords($sql);
    
    /**
     * Returns the database specific query statement that the \Core\Library\Database\Query object represents
     *
     * @return string
     * @param \Core\Library\Database\Query $query The query object to transoform into a query statement
     * @param bool $loadRelationships Boolean indicating if relationship data should be loaded.
     */
    public function selectStatementFromQuery($query, $loadRelationships=false);
}
?>