<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Database\Driver;
/**
 * MySQL PDO Driver class. Implements PDOInterface to provide access to MySQL databases
 *
 * This class implements the PDOInterface to provide data access to MySQL databases.
 *
 * @todo Change these methods so all parameters are objects. i.e. Change the $tablename parameter to be a \Core\Library\Database\Table object.
 * @todo Possibly update all the methods to isolate just the querys, and move the logic to the parent class
 * @todo Add, Update and Remove foreign keys
 * @todo use table aliases in selectStatementFromQuery method
 *
 * @see \Core\Library\Database\Driver\PDOInterface
 * @see \Core\Library\Database\Driver\PDOWrapper
 *
 * @package \Core\Library\Database\Driver\PDOMySQL
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class PDOMySQL extends PDOWrapper implements PDOInterface{
    /**
     * A constant to define the "Model" namespace. Should be the decimal value of the character
     * @var string
     */
    const JOIN_DELIMITER=29;
    
    /**
     * Internal storage for DESCRIBE <table> results
     * @var array
     */
    public $describes=array();
    /**
     * Internal storage for SHOW INDEX results.
     * $var array
     */
    public $showIndex=array();
    
    /**
     * Maximum number of rows that can be inserted in one query
     * @param int
     */
    private $maxInsertRows=1000;
    
    /**
     * Queries the DB and returns an array containing information about a table
     *
     * @return array
     * @param string $tableName The name of the table to retrieve information about
     * @param bool $forceRefresh When true the method will query the database for fresh data
     */
    public function describeTable($tableName, $forceRefresh=false){
        if($forceRefresh || !array_key_exists($tableName, $this->describes)){
            $this->describes[$tableName] = $this->query("DESCRIBE `".$tableName."`");
        }
        return $this->describes[$tableName];
    }
    
    /**
     * Queries the database and returns information about the indexes on $tableName
     *
     * @return array Information about the indexes on the table
     * @param string $tableName The name of the table for which to get index information
     * @param bool $forceRefresh When true the method will query the database for fresh data
     */
    public function showIndex($tableName, $forceRefresh=false){
        if($forceRefresh || !array_key_exists($tableName, $this->showIndex)){
            $this->showIndex[$tableName] = $this->query("SHOW INDEX FROM `".$tableName."`");
        }
        return $this->showIndex[$tableName];
        
    }
    
    /**
     * Method returns a valid PDO connection which is available for use at $this->connection
     *
     * @return \PDO
     * @param string $host The hostname of the database server
     * @param string $user The user name for the database server
     * @param string $password The password for the database server
     * @param string $database The name of the database to load
     */
    public function getConnection($host, $user, $password, $database){
        if (!defined('PDO::ATTR_DRIVER_NAME')) {
            throw new \Exception('Please install the PHP PDO extension');
        }
        return new \PDO("mysql:dbname=".$database.";host=".$host, $user, $password);
    }
    
    /**
     * Get an array of all the table names in the database
     *
     * @return array List of table names in the database
     */
    public function showTables(){
        $return = array();
        $result = $this->query("SHOW TABLES");
        while($row = array_shift($result)){
            $return[] = array_shift($row);
        }
        return $return;
    }
    
    /**
     * Determine if a field has a unique index
     *
     * @return bool
     * @param string $tableName The name of the table to which the field belongs
     * @param string $fieldName The name of the field in $tableName to test for uniqueness
     * @param bool $forceRefresh When true teh method will query the database for fresh data
     */
    public function isFieldUnique($tableName, $fieldName, $forceRefresh=false){
        $result = $this->showIndex($tableName,$forceRefresh);
        foreach($result as $index){
            
            if($index['Column_name'] == $fieldName && $index['Non_unique'] == "0"){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Get an array of \Core\Library\Database\Field objects
     * If $model is sent, it will be passed to the \Core\Library\Database\Table constructor
     *
     * @return \Core\Library\Database\Field[]
     * @param string $tableName The name of the table to get the fields from
     * @param \Core\Library\Model $model The \Core\Library\Model object to be used in constructing \Core\Library\Database\Field objects
     * @param bool $forceRefresh When true the method will query the database again for field data
     */
    public function getFields($tableName, $model=null, $forceRefresh=false){
        $fields = array();
        if($this->tableExists($tableName, $forceRefresh)){
            $result = $this->describeTable($tableName, $forceRefresh);
            $ai=null;
            $pri = null;
            $t = $this->registry->load->database('Table', array($this->db, $tableName, $model));
            while($row = array_shift($result)){
                $f = $this->registry->load->database('Field', array($t, $row['Field']));
                //Field type
                $temp = explode("(",$row['Type']);
                if(count($temp) > 0){
                    $type = trim($temp[0]);
                }else{
                    $type = "";
                }
                $f->datatype = $type;
                
                //Field Size
                if(count($temp) > 1){
                    $size = trim(str_replace(array(")",'unsigned'),"",$temp[1]));
                }else{
                    $size = "";
                }
                
                if(strtolower($type) == 'enum'){
                    $f->size = "";
                    $f->values = explode(',',str_replace("'","",$size));
                }else{
                    $f->size = $size;
                }
                
                if($row['Extra'] == "auto_increment"){
                    $f->auto_inc = true;
                    $ai = $row['Field'];
                }else{
                    $f->auto_inc = false;
                }
                
                if($row['Key'] == "PRI"){
                    $f->primary = true;
                    $pri = $row['Field'];
                }else{
                    $f->primary = false;
                }
                
                if($row['Key'] == "UNI"){
                    $f->unique = true;
                }else{
                    
                    $f->unique = $this->isFieldUnique($tableName, $f->name, $forceRefresh);
                }
                
                $f->null = ($row['Null'] == 'NO' ? false : true);
                $fields[$f->name] = $f;
            }
        }
        return $fields;
    }
    
    /**
     * Determine if $tableName exists in the database
     *
     * @retrun bool
     * @param string $tableName The name of the table to check
     * @param bool $forceRefresh When true the method will query the database again for fresh data
     */
    public function tableExists($tableName, $forceRefresh=false){
        if(!$forceRefresh && array_key_exists($tableName, $this->tableData)){
            return true;
        }else{
            $result = $this->query("SHOW TABLES LIKE '".$tableName."'");
            if(empty($result)){
                return false;   
            }else{
                return true;
            }
            
        }
    }
    
    /**
     * Get the query that needs to be executed to create a new table.
     *
     * @return string The query to execute to create $table
     * @param \Core\Library\Database\Table $table A table object that will be created by the query returned
     */
    public function createTableQuery($table){
        $tblSQL = 'CREATE TABLE `'.$table->name.'` (';
        $unique = '';
        foreach($table->fields as $fld){
            $tblSQL .= '`'.$fld->name.'` '.$fld->datatype;
            $tblSQL .= (strlen($fld->size) > 0 && $fld->size ? '('.$fld->size.')' : '');
            if($fld->datatype == 'enum'){
                $tblSQL .= '(';
                foreach($fld->values as $val){
                    $tblSQL .= "'".$val."', ";
                }
                $tblSQL = rtrim($tblSQL,', ');
                $tblSQL .= ')';
            }
            $tblSQL .= (!$fld->null ? ' NOT NULL' : '');
            $tblSQL .= ($fld->auto_inc ? ' AUTO_INCREMENT' : '');
            $tblSQL .= ($fld->primary ? ' PRIMARY KEY' : '').', ';

            if($fld->unique){
                if(strlen($unique) <= 0){
                    $unique = 'UNIQUE KEY (';
                }
                $unique .= '`'.$fld->name.'`, ';
            }
        }
        if(strlen($unique) > 0){
            $tblSQL .= substr($unique,0,strlen($unique)-2).'))';
        }else{
            $tblSQL = substr($tblSQL,0,strlen($tblSQL)-2).')';
        }
        return $tblSQL;
    }
    
    /**
     * Get an array of index objects for $tableName
     * Index::objTable should be assigned if it is available.
     *
     * @return \Core\Library\Database\Index[]
     * @param string $tableName The name of the table for which to retrieve the indexes
     * @param bool $forceRefresh when true the method is forced to query the database again.
     */
    public function getTableIndexes($tableName, $forceRefresh=false){
		if($this->tableExists($tableName, $forceRefresh)){	//If the table exists
			if($forceRefresh || !array_key_exists($tableName,$this->tableIndexes)){
				$result = $this->showIndex($tableName, $forceRefresh);
				$temp = array();
				while($row = array_shift($result)){
                    if(array_key_exists($row['Key_name'], $temp)){
                        //This is another field in an existing index
                        $temp[$row['Key_name']]->columns[$row['Column_name']] = $row['Column_name'];
                    }else{
                        $index = $this->registry->load->database('Index');
                        $index->table = $row['Table'];
                        $index->unique = ($row['Non_unique']=='0' ? true : false);
                        $index->key_name = $row['Key_name'];
                        $index->seq_in_index = $row['Seq_in_index'];
                        $index->columns[$row['Column_name']] = $row['Column_name'];
                        $index->collation = $row['Collation'];
                        $index->cardinality = $row['Cardinality'];
                        $index->sub_part = $row['Sub_part'];
                        $index->packed = $row['Packed'];
                        $index->null = ($row['Null']=='YES' ? true : false);
                        $index->index_type = $row['Index_type'];
                        $index->comment = $row['Comment'];
                        $index->index_comment = $row['Index_comment'];
                        
                        if(array_key_exists($tableName, $this->tableData)){
                            $index->objTable = $this->tableData[$tableName];
                        }
                        
                        $temp[$index->key_name] = $index;
                    }
				}
                $this->tableIndexes[$tableName] = $temp;
			}
			return $this->tableIndexes[$tableName];
		}else{
			return array();
		}
	}
    
    /**
     * Add $field to $tableName in the database
     *
     * @return null
     * @param string $tableName The name of the table to which $field should be added
     * @param \Core\Library\Database\Field $field An object describing the field that is to be added
     */
    public function addField($tableName, $field){
        if($this->tableExists($tableName, true) && !$this->fieldExists($tableName,$field->name, true)){
			$tblSQL = 'ALTER TABLE `'.$tableName.'` ADD ';
			$unique = '';
			if(strtolower($field->datatype) == 'enum'){
				$tblSQL .= '`'.$field->name.'` '.$field->datatype.'(';
				if(!is_array($field->values)){
					$field->values = explode(",",str_replace("'","",$field->values));
				}
				foreach($field->values as $value){
					$tblSQL .= "'".$value."',";
				}
				$tblSQL = rtrim($tblSQL,",").')';
			}else{
				$tblSQL .= "`".$field->name.'` '.$field->datatype;
                $tblSQL .= (isset($field->size) && strlen($field->size) > 0 && $field->size ? '('.$field->size.')' : '');
			}
			$tblSQL .= (isset($field->null) && !$field->null ? ' NOT NULL' : '');
            $tblSQL .= (isset($field->auto_inc) && $field->auto_inc ? ' AUTO_INCREMENT' : '');
            $tblSQL .= (isset($field->primary) && $field->primary ? ' PRIMARY KEY' : '');
			if(isset($field->unique) && $field->unique){
				$unique = 'ALTER TABLE `'.$tableName.'` ADD UNIQUE (`'.$field->name.'`)';
			}
            $idField = $this->getPrimaryIndexField($tableName, true);
            if(isset($field->primary) && $field->primary && $idField !== false && $field->name != $this->getPrimaryIndexField($tableName, true)->name){
				//If we are going to try and add a primary key, then we need to make sure to drop the existing key
				$this->query('ALTER TABLE `'.$tableName.'` DROP PRIMARY KEY');
			}
            
			$this->query($tblSQL);
			if(strlen($unique) > 0){
				$this->query($unique);
			}
			unset($this->tableData[$tableName]);
            unset($this->describes[$tableName]);
		}
    }
    
    /**
     * Remove $fieldName from $tableName in the database
     *
     * @return null
     * @param string $tableName The name of the table that $fieldName belongs
     * @param string $fieldName The name of the field to be removed
     */
    public function removeField($tableName, $fieldName){
        if($this->tableExists($tableName, true) && $this->fieldExists($tableName,$fieldName, true)){
			$this->query('ALTER TABLE `'.$tableName.'` DROP COLUMN `'.$fieldName.'`');
            unset($this->tableData[$tableName]);
            unset($this->describes[$tableName]);
		}
    }
    
    /**
     * Update the $field in $tableName so it matches the $field object
     * If $field does not exist on $tableName, it will be added
     *
     * @return null
     * @param string $tableName The name of the table to which $field belongs
     * @param \Core\Library\Database\Field An object describing the shape of the field.
     */
    public function updateField($tableName, $field){
        if($this->tableExists($tableName) && $this->fieldExists($tableName,$field->name)){
			$oldField = $this->getField($tableName,$field->name, true);
            
            //This field still exists, does it need an update?
            $update=array();
            if($oldField->null != $field->null){
                $update['null'] = $field->null;
            }elseif($oldField->datatype != $field->datatype){
                $update['datatype'] = $field->datatype;
            }elseif($oldField->size != $field->size){
                $update['size'] = $field->size;
            }elseif($oldField->auto_inc != $field->auto_inc){
                $update['auto_inc'] = $field->auto_inc;
            }elseif($oldField->unique != $field->unique){
                $update['unique'] = $field->unique;
            }elseif($oldField->primary != $field->primary){
                $update['primary'] = $field->primary;
            }
            
            if($update){
                $tblSQL = 'ALTER TABLE `'.$tableName.'` MODIFY ';
                $unique = '';
                if(strtolower($field->datatype) == 'enum'){
                    $tblSQL .= '`'.$field->name.'` '.$field->datatype.'(';
                    if(!is_array($field->values)){
                        $field->values = explode(",",str_replace("'","",$field->values));
                    }
                    foreach($field->values as $value){
                        $tblSQL .= "'".$value."',";
                    }
                    $tblSQL = rtrim($tblSQL,",").')';
                }else{
                    $tblSQL .= '`'.$field->name.'` '.$field->datatype.(isset($field->size) && strlen($field->size) > 0 && $field->size ? '('.$field->size.')' : '');
                }
                $tblSQL .= (isset($field->null) && !$field->null ? ' NOT NULL' : '');
                $tblSQL .= (isset($field->auto_inc) && $field->auto_inc ? ' AUTO_INCREMENT' : '');
                $tblSQL .= (isset($field->primary) && $field->primary && $field->name != $this->getPrimaryIndexField($tableName, true)->name ? ' PRIMARY KEY' : '');
                
                if(isset($field->unique) && $field->unique){
                    $unique = 'ALTER TABLE `'.$tableName.'` ADD UNIQUE (`'.$field->name.'`)';
                }
                
                if(isset($field->primary) && $field->primary && $field->name != $this->getPrimaryIndexField($tableName, true)->name){
                    //If we are going to try and add a primary key, then we need to make sure to drop the existing key
                    $this->query('ALTER TABLE `'.$tableName.'` DROP PRIMARY KEY');
                }
                $this->connection->query($tblSQL);
                if(strlen($unique) > 0 && !$oldField->unique) //It wasnt unique before, but it is now.
                    $this->query($unique);
                //Empty out whatever cache we might have stored
                unset($this->tableData[$tableName]);
                unset($this->describes[$tableName]);
            }
		}elseif($this->tableExists($tableName, true) && !$this->fieldExists($tableName,$field->name, true)){
            $this->addField($tableName, $field);
        }
    }
    
    /**
     * Add the $index object to $tableName if it doesn't exist
     *
     * @return null
     * @param string $tableName The name of the table to which $index should be added
     * @param \Core\Library\Database\Index $index An object describing the index that should be added to $tableName
     */
    public function addIndex($tableName, $index){
        $exist = $this->getIndex($tableName, $index, true);
        $tester = $this->showIndex($tableName, true);
        if(empty($tester)){
            $pos=false;
        }else{
            $pos = array_search($index->key_name,array_column($tester,'Key_name'));
        }
        if($pos === false){
            if($index->key_name == 'PRIMARY'){
                $keys = array_keys($index->columns);
                $key = array_shift($keys);
                $sql = "ALTER TABLE `".$tableName."` ADD PRIMARY KEY(".$index->columns[$key].")";
            }else{
                $table = $this->getTable($tableName, null, true);
                $type = ($index->index_type=='FULLTEXT' ? $index->index_type.' ' : '');
                $type = (empty($type) && $index->unique ? 'UNIQUE ' : $type);
                $columnString = '';
                foreach($index->columns as $column){
                    $columnString .= "`".$column."`, ";
                }
                $columnString = rtrim($columnString,", ");
                
                $sql = 'ALTER TABLE `'.$tableName.'` ADD '.$type.'INDEX `'.$index->key_name.'` ('.$columnString.')'.($index->index_type=='BTREE' || $index->index_type=='HASH' ? ' USING '.$index->index_type : '')." COMMENT '".$index->comment."'";
            }
            if(strlen($sql) > 0){
                $this->connection->query($sql);
            }
            unset($this->tableData[$tableName]);
            unset($this->tableIndexes[$tableName]);
            unset($this->showIndex[$tableName]);
        }
    }
    
    /**
     * Remove $index from $tableName
     *
     * @return null
     * @param string $tableName The name of the table from which to drop $index
     * @param \Core\Library\Database\Index The index object that is to be dropped from $tableName
     */
    public function removeIndex($tableName, $index){
        $exist = $this->getIndexByName($tableName, $index->key_name, true);
        if($this->tableExists($tableName, true) && !empty($exist)){
            $sql = "DROP INDEX `".$index->key_name."` ON `".$tableName."`";
            $this->connection->query($sql);
            unset($this->tableData[$tableName]);
            unset($this->tableIndexes[$tableName]);
            unset($this->showIndex[$tableName]);
        }
    }
    
    /**
     * Rename the index on $tableName from $oldName to $newName
     *
     * @return null
     * @param string $tableName The name of the table where the index resides
     * @param string $oldName The current name of the index to be renamed
     * @param string $newName The new name of the index
     */
    public function renameIndex($tableName, $oldName, $newName){
        $sql = "ALTER TABLE `".$tableName."` RENAME INDEX ".$oldName." TO ".$newName;
        $this->query($sql);
        unset($this->tableIndexes[$tableName]);
        unset($this->showIndex[$tableName]);
    }
    
    /**
     * Return the total number of records returned by the $sql query
     * Since PDO MySQL is not capable of reliably determining the number of affected records for a query, we must implement this method
     *
     * @return int
     * @param string $sql The SQL query for which to find the number of records
     */
    public function totalRecords($sqlData){
        $sql = "SELECT COUNT(*) as count FROM (".$sqlData['statement'].") as subquery";
        $statement = $this->connection->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_SCROLL));
        $statement->execute($sqlData['parameters']);
        $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
        if(!empty($result)){
            $result = array_shift($result);
        }
        if(!empty($result['count'])){
            return $result['count'];
        }else{
            return 0;
        }
    }
    
    /**
     * Returns the database specific "SELECT" query statement and parameters that the \Core\Library\Database\Query object represents
     * if $loadRelationships is true, then the query should join necessary tables so relationship data is loaded during the query
     *
     * returned value should be an array with the information needed to use the PDO::prepare statement.
     * return array('statement'=>'SQL Statement', 'parameters'=>'params to use for PDO::execute()');
     *
     * Fields are all aliased so that we can retreive all relational information in one query, and then parse the data back out into
     * the record, and it's relationship data. Aliases need to follow a convention so we know how to separate the information in the result.
     *
     * Aliases from fields in a joined table should be prefixed by the constant \Core\Library\Database\Query::FIELD_TYPE_JOIN
     * Aliases from the SELECTed table should be prefixed with the constant \Core\Library\Database\Query::FIELD_TYPE_DEFAULT, and the relationshipName should be \Core\Library\Database\Query::FIELD_TYPE_DEFAULT
     *
     * Values from joined tables are GROUP_CONCATed and separated by self::JOIN_DELIMITER character. This way, we get the correct number of rows, and
     * the recordset can parse the records as they pass through, maintaining minimal memory usage regardless of the size of the recordset
     *
     * Note: If a relationship object does not have a name defined, then the name should be set to the string 'custom'
     * 
     * All fields returned should be aliased as follows:
     *
     * <\Core\Library\Database\Recordset::FIELD_TYPE_DEFAULT|\Core\Library\Database\Query::FIELD_TYPE_JOIN>.<tablename>.<relationshipName|\Core\Library\Database\Query::FIELD_TYPE_DEFAULT>.<fieldName>
     *
     * @todo when loading relationships, allow only specific fields to be returned
     * @todo change the fields to return from string type field names to Core\Library\Database\Field objects
     *
     * @return string
     * @param \Core\Library\Database\Query $query The query object to transform into a query statement
     * @param bool $loadRelationships Boolean indicating if the records relationship data should be loaded
     */
    public function selectStatementFromQuery($query, $loadRelationships=false){
        //Make sure that the \Core\Library\Database\Recordset object code is included
        $aliases = array();
        $remoteAliases = array();
        $orderBy = $query->getOrderBy();
        if($query->getType() == \Core\Library\Database\Query::QUERY_TYPE_SELECT){
            $queryTable = $query->getTable();
            $aliases[$queryTable->name] = 'qt_'.$queryTable->name;
            //If the table in the tabledata array doesn't have the model set, then we'll set it here.
            if(!empty($queryTable->getModel()) && array_key_exists($queryTable->name, $this->tableData) && empty($this->tableData[$queryTable->name]->getModel())){
                $this->tableData[$queryTable->name]->setModel($queryTable->getModel());
            }
            $this->registry->load->includeRequired('Core\Library\Database\Recordset');
            $fieldList='';
            $groupFields='';
            
                
            $allFields = false;
            foreach($query->getFields() as $field){
                if($field == '*'){
                    $allFields = true;
                }
                
                foreach($queryTable->getFields() as $f){
                    $fieldAlias = '`'.\Core\Library\Database\Query::FIELD_TYPE_DEFAULT.'.'.$queryTable->name.'.'.\Core\Library\Database\Query::FIELD_TYPE_DEFAULT.'.'.$f->name.'`';
                    if($allFields){                        
                        $fieldList .= "\n\t`".$aliases[$queryTable->name]."`.`".$f->name."` as ".$fieldAlias.", ";
                    }
                    if(!empty($orderBy) && $loadRelationships){
                        $groupFields .= "\n\t`order_temp`.".$fieldAlias.", ";
                        if($f->name == $orderBy['field']->name && $f->getTable()->name == $orderBy['field']->getTable()->name){
                            $orderBy['alias'] = $fieldAlias;
                        }
                    }
                }
                $fieldAlias = '`'.\Core\Library\Database\Query::FIELD_TYPE_DEFAULT.'.'.$queryTable->name.'.'.\Core\Library\Database\Query::FIELD_TYPE_DEFAULT.'.'.$field.'`';
                $fieldSQL = ".`".$field.'` as '.$fieldAlias.', ';
                if(!$allFields){
                    $fieldList .= "\n\t`".$aliases[$queryTable->name]."`".$fieldSQL;
                }
                if(!$allFields && !empty($orderBy) && $loadRelationships){
                    $groupFields .= "\n\t\`order_temp`".$fieldSQL;
                    if($field == $orderBy['field']->name && $queryTable->name == $orderBy['field']->getTable()->name){
                        $orderBy['alias'] = $fieldAlias;
                    }
                }
                
                //if($allFields){
                //    //All fields are requested, as well as relationship data, so we should list the fields with aliases
                //    foreach($queryTable->getFields() as $field){
                //        
                //        $fieldSQL = "\n\t`".$aliases[$queryTable->name]."`.`".$field->name.'` as `'.\Core\Library\Database\Query::FIELD_TYPE_DEFAULT.'.'.$queryTable->name.'.'.\Core\Library\Database\Query::FIELD_TYPE_DEFAULT.'.'.$field->name.'`, ';
                //        if(!empty($orderBy)){
                //            $groupFields .= $fieldSQL;
                //        }
                //        $fieldList .= $fieldSQL;
                //    }
                //}else{
                //    $fieldSQL = "\n\t`".$aliases[$queryTable->name]."`.`".$field.'` as `'.\Core\Library\Database\Query::FIELD_TYPE_DEFAULT.'.'.$queryTable->name.'.'.\Core\Library\Database\Query::FIELD_TYPE_DEFAULT.'.'.$field.'`, ';
                //    if(!empty($orderBy)){
                //        $groupFields .= $fieldSQL;
                //    }
                //    $fieldList .= $fieldSQL;
                //}
            }
            
            
            
            //First we want to go through the relationships and build a field list, and a join list
            $joins = '';
            $tableJoins = $query->getJoins();
            if($loadRelationships || !empty($tableJoins)){
                $relsCompleted=array(); //Keep track of which relationships have been done so we don't do one more than once
                //Here we should join the necessary relationships to the query
                $relations = array();
                if($loadRelationships){
                    $relations = $queryTable->getRelationships();
                }
                $relations = array_merge($relations,$tableJoins);
                if(!empty($orderBy) && empty($orderBy['alias'])){
                    foreach($relations as $rel){
                        foreach($rel->getRemoteTable()->getFields() as $field){
                            if($field->name == $orderBy['field']->name && $field->getTable()->name == $orderBy['field']->getTable()->name){
                                $orderBy['alias'] = '`'.\Core\Library\Database\Query::FIELD_TYPE_JOIN.'.'.$rel->getLocalTable()->name.'.'.$rel->name.'.'.$field->name.'`';
                            }
                        }
                    }
                }
                while($rel = array_shift($relations)){
                    $localTable = $rel->getLocalTable();
                    if(!in_array($localTable->name.'.'.$rel->name, $relsCompleted)){
                        $relsCompleted[] = $localTable->name.'.'.$rel->name;
                        $localField = $rel->getLocalField();
                        $remoteTable = $rel->getRemoteTable();
                        $remoteField = $rel->getRemoteField();
                        if(empty($rel->name)){
                            $rel->name = 'custom';
                        }
                        
                        if(!array_key_exists($localTable->name, $aliases)){
                            $aliases[$localTable->name] = 'lt_'.$localTable->name;
                            $localAlias = 'lt_'.$localTable->name;
                        }else{
                            
                        }
                        if(!array_key_exists($remoteTable->name, $aliases)){
                            $aliases[$remoteTable->name] = 'rt_'.$remoteTable->name;
                            $remoteAlias = 'rt_'.$remoteTable->name;
                            $remoteAliases[$remoteTable->name]=$remoteAlias;
                        }else{
                            $count=0;
                            $remoteAlias = 'rt_'.$remoteTable->name.'_'.$count;
                            while(in_array($remoteAlias, $remoteAliases)){
                                $count++;
                                $remoteAlias = 'rt_'.$remoteTable->name.'_'.$count;
                            }
                            $remoteAliases[$remoteTable->name]=$remoteAlias;
                        }
                        
                        
                        foreach($remoteTable->getFields() as $field){
                            $fieldAlias = '`'.\Core\Library\Database\Query::FIELD_TYPE_JOIN.'.'.$localTable->name.'.'.$rel->name.'.'.$field->name.'`';
                            if($allFields){
                                if(!empty($orderBy)){
                                    $fieldList .= "\n\t`".$remoteAlias."`.`".$field->name."` as ".$fieldAlias.", ";
                                    
                                }else{
                                    $fieldList .= "\n\tGROUP_CONCAT(`".$remoteAlias."`.`".$field->name.'` SEPARATOR 0x'.dechex(self::JOIN_DELIMITER).') as '.$fieldAlias.', ';
                                }
                            }else{
                                //Which fields do I select?
                            }
                            if(!empty($orderBy)){
                                //$groupFields .= "\n\t`order_temp`.".$fieldName.", ";
                                $groupFields .= "\n\tGROUP_CONCAT(`order_temp`.".$fieldAlias.' ORDER BY `order_temp`.'.$orderBy['alias'].' '.$orderBy['direction'].' SEPARATOR 0x'.dechex(self::JOIN_DELIMITER).') as '.$fieldAlias.', ';
                            }
                        }
                        //One more time through the fields now that we have the field alias set
                        
                        
                        //if($allFields){
                        //    foreach($remoteTable->getFields() as $field){
                        //        $fieldList .= "\n\tGROUP_CONCAT(`".$remoteAlias."`.`".$field->name.'` SEPARATOR 0x'.dechex(self::JOIN_DELIMITER).') as `'.\Core\Library\Database\Query::FIELD_TYPE_JOIN.'.'.$localTable->name.'.'.$rel->name.'.'.$field->name.'`, ';
                        //        
                        //    }
                        //}else{
                        //    //Errrmmmmm...which fields do I grab?
                        //}
                        
                        $joins .= "\nLEFT JOIN `".$remoteTable->name."` as `".$remoteAlias."` ON \n\t`".$aliases[$localTable->name]."`.`".$localField->name."` = `".$remoteAlias."`.`".$remoteField->name."` ";
                        //$relations = array_merge($relations, $remoteTable->getRelationships());
                    }
                }
            }
            
            $sql = 'SELECT '.$fieldList;
            $sql = trim($sql,', ').' ';
            $sql .= "\nFROM \n\t`".$queryTable->name."` as `".$aliases[$queryTable->name]."` ".rtrim($joins).' ';
            $conditions = $query->getConditions();
            
            
            $params = array();
            if(!empty($conditions)){
                $first=true;
                foreach($conditions as $condition){
                    if($first){
                        $sql .= "\nWHERE ";
                        $first=false;
                    }else{
                        $sql .= "\n".$condition['appendOperator'].' ';
                    }
                    $sql .= "\n\t(";
                    foreach($condition['fields'] as $data){
                        $useQuote=false;
                        if(!is_int($data['value'])){
                            $useQuote=true;
                        }
                        $tableName = (array_key_exists($data['field']->getTable()->name, $remoteAliases) ? $remoteAliases[$data['field']->getTable()->name] : $aliases[$data['field']->getTable()->name]);
                        $paramName = ':'.$tableName.'_'.$data['field']->name.count($params);
                        $sql .= "`".$tableName."`.`".$data['field']->name."` ".$data['comparison'].' '.$paramName.' '.$condition['fieldComparison'].' ';
                        $params[$paramName] = $data['value'];
                    }
                    $sql = trim($sql, $condition['fieldComparison'].' ').") \n";
                }
                
            }
            
            $idField = $this->getPrimaryIndexField($queryTable->name);
            if(!empty($orderBy)){
                $sql .= "ORDER BY \n\t`".$aliases[$orderBy['field']->getTable()->name].'`.`'.$orderBy['field']->name.'` '.$orderBy['direction']." ";
            }
            
            if($loadRelationships){
                if(!empty($orderBy)){
                    
                    $sql = "SELECT \n\t".rtrim($groupFields,", ")." \nFROM \n\t(\n\t\t".str_replace(array("\n"),array("\n\t\t"),$sql)."\n\t) as `order_temp` \n";
                    $sql .= "GROUP BY \n\t`order_temp`.`".\Core\Library\Database\Query::FIELD_TYPE_DEFAULT.'.'.$queryTable->name.'.'.\Core\Library\Database\Query::FIELD_TYPE_DEFAULT.'.'.$idField->name.'`';
                }else{
                    $sql .= "\nGROUP BY `".$aliases[$queryTable->name].'`.`'.$idField->name.'` ';
                }
            }
            
            $sql = trim($sql);
            //$this->print_array(array('statement'=>$sql,'parameters'=>$params));
            return array('statement'=>$sql,'parameters'=>$params);
        }
    }
    

    /**
     * Method will insert the $records into the database. Insert statement is built by the db specific
     * drivers \Core\Library\Database\Driver\<driverClass>::insertRecordStatement() method.
     *
     * If all records can not be processed, an exception is thrown and the database will be returned to the state it was in before
     * calling this method.
     *
     * Note: When more than 6 records are to be inserted, this method uses a specific set of 6 queries, utilizing temporary tables,
     * and temporary triggers in order to execute multiple insert statements at once. At most, this function should execute roundup(n/1000)+6 queries.
     *
     * @todo implement the case where $insertRelationships is true
     *
     * @throws \Exception 
     * @return array The ids of the records inserted
     * @param \Core\Library\Database\Record[] $records The records to be inserted.
     * @param bool (optional) Boolean indicating if related records loaded in the $records should be inserted as well. Defaults to false
     */
    public function insertRecords($records, $insertRelationships=false){
        //If there are less than 6 records, don't do anything fancy, just insert one at a time.
        if(count($records) <= 6){
            try{
                $this->connection->beginTransaction();
                $ids = array();
                foreach($records as $record){
                    $table = $record->getTable();
                    $fields = $table->getFields();
                    $setFields = $record->getSetFields();
                    $idField = $table->getPrimaryField();
                    $sql = "INSERT INTO ".$record->getTable()->name.' (';
                    $values = ' VALUES (';
                    $vals = array();
                    foreach($fields as $field){
                        $sql .= "`".$field->name."`, ";
                        if($field->auto_inc && $field->name == $idField->name){
                            $values .= "NULL, ";
                        }else{
                            if(in_array($field->name, $setFields)){
                                $values .= "?, ";
                                $vals[] = $record->{$field->name};
                            }else{
                                $sql .= '`'.$table->name.'`.`'.$field->name.'`, ';
                            }
                        }
                    }
                    $sql = rtrim($sql, ', ').') ';
                    $values = rtrim($values, ', ').')';
                    $sql = $sql.$values;
                    $statement = $this->connection->prepare($sql);
                    $statement->execute($vals);
                    $ids[] = $this->connection->lastInsertId();
                }
                $this->connection->commit();
                return $ids;
            }catch(\PDOException $e1){
                try{
                    $this->connection->rollBack();
                }catch(\PDOException $e2){
                    
                }
                throw new \Exception($e1->getMessage());
            }
        }else{  //If there are more than 6 records, then the following will insert them 1000 at a time, and then return the ids
            try{
                $this->connection->beginTransaction();
                $result = $this->query('SELECT UUID() as uuid');
                $result = array_shift($result);
                $uuid = $result['uuid'];
                $this->connection->query('CREATE TEMPORARY TABLE IF NOT EXISTS `insertids_'.$uuid.'` (`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, `inserted` int(11) NOT NULL)');
                $table=null;
                $fields=null;
                $sql='';
                $sqlHead = '';
                while(count($records) > 0){
                    $counter=0;
                    $sql = $sqlHead;
                    $values = array();
                    while(count($records) > 0 && $counter < $this->maxInsertRows){
                        $record = array_shift($records);
                        
                        if($table===null){
                            $table = $record->getTable();
                            $fields = $table->getFields();
                            $idField = $table->getPrimaryField();
                            $sqlHead = 'INSERT IGNORE INTO '.$record->getTable()->name.' (';
                            foreach($fields as $field){
                                $sqlHead .= '`'.$field->name.'`, ';
                            }
                            $sqlHead = rtrim($sqlHead, ', ').') VALUES ';
                            $sql = $sqlHead;
                            $trigger = '
                            CREATE trigger `inserted_'.$uuid.'` AFTER INSERT ON `'.$table->name.'`
                            FOR EACH ROW
                            BEGIN
                                INSERT INTO `insertids_'.$uuid.'` (`id`, `inserted`) VALUES (NULL, NEW.`'.$idField->name.'`);
                            END;';
                            $this->connection->query($trigger);
                        }
                        
                        $setFields = $record->getSetFields();
                        $sql .= '(';
                        foreach($fields as $field){
                            if($field->auto_inc && $field->name == $idField->name){
                                $sql .= 'NULL, ';
                            }else{
                                if(in_array($field->name, $setFields)){
                                    $sql .= "?, ";
                                    $values[] = $record->{$field->name};
                                }else{
                                    $sql .= '`'.$table->name.'`.`'.$field->name.'`, ';
                                }
                            }
                        }
                        $sql = rtrim($sql,', ').'), ';
                        $counter++;
                        
                    }
                    $sql = rtrim($sql, ', ');
                    $statement = $this->connection->prepare($sql);
                    $statement->execute($values);
                }
                $insertedIds = $this->query('SELECT inserted FROM `insertids_'.$uuid."`");
                $this->connection->query('DROP TRIGGER `inserted_'.$uuid."`");
                $this->connection->query('DROP TABLE `insertids_'.$uuid."`");
                $this->connection->commit();
            }catch(\PDOException $e1){
                try{
                    
                }catch(\PDOException $e2){
                    
                }
                if(!empty($uuid)){
                    $this->connection->query('DROP TRIGGER `inserted_'.$uuid."`");
                    $this->connection->query('DROP TABLE `insertids_'.$uuid."`");
                }
                throw new \Exception($e1->getMessage().'<br>'.$e1->getTraceAsString());
            }
            return array_column($insertedIds,'inserted');
        }
    }
    
    /**
     * Get the query statement to update the record in the database
     * $record must contain the primary key field, and must have a value for each non-null field.
     *
     * If all records can not be updated an exception will be thrown, and the database will be returned to the state it was in before
     * calling this method.
     *
     * @todo implement the case where $updateRelationships is true
     * @throws \Exception 
     * @return null
     * @param \Core\Library\Database\Record[] the record objects to be updated
     * @param bool (optional) Boolean indicating if related records loaded in the $records should be updated as well. defaults to false
     */
    public function updateRecords($records, $updateRelationships=false){
        try{
            $this->connection->beginTransaction();
            foreach($records as $record){
                $table = $record->getTable();
                $idField = $table->getPrimaryField();
                $fields = $table->getFields();
                $sql = "UPDATE `".$table->name."` SET ";
                $values = array();
                foreach($fields as $field){
                    if($field->name != $idField->name){
                        $sql .= "`".$field->name."` = ?, ";
                        $values[] = $record->{$field->name};
                    }
                }
                $sql = rtrim($sql,', ');
                $sql .= " WHERE `".$idField->name."` = ?";
                $values[] = $record->{$idField->name};
                $statement = $this->connection->prepare($sql);
                if(!$statement->execute($values)){
                    try{
                        $this->connection->rollBack();
                    }catch(\PDOException $e2){
                        
                    }
                    throw new \Exception(print_r($this->connection->errorInfo(),true));
                }
            }
            $this->connection->commit();
        }catch(\PDOException $e1){
            try{
                $this->connection->rollBack();
            }catch(\PDOException $e2){
                
            }
            throw new \Exception($e1->getMessage().'<br>'.$e1->getTraceAsString());
        }
    }
    
    /**
     * Get the query statement to remove the record from the database
     * $record must contain the primary key field
     *
     * If all records can not be processed, the transaction will be rolled back, and the databse will be left
     * in the state it was in before calling this method.
     *
     * @todo implement the case where $deleteRelatedRecords is true
     *
     * @throws \Exception 
     * @return null
     * @param \Core\Library\Database\Record[] $records A list of records to insert.
     * @param bool (optional) Boolean indicating if related records loaded in the $records should be deleted as well. defaults to false
     */
    public function deleteRecords($records, $deleteRelatedRecords=false){
        try{
            $this->connection->beginTransaction(); 
            foreach($records as $record){
                $table = $record->getTable();
                $idField = $table->getPrimaryField();
                $sql = "DELETE FROM `".$table->name."` WHERE `".$table->name."`.`".$idField->name."` = ?";
                $statement = $this->connection->prepare($sql);
                $statement->execute(array($record->{$idField->name}));
            }
            $this->connection->commit();
        }catch(\PDOException $e1){
            try{
                $this->connection->rollBack();
            }catch(\PDOException $e2){
                
            }
            throw new \Exception($e1->getMessage().'<br>'.$e1->getTraceAsString());
        }
    }
    
    
    /**
     * Will return the query with the values replaced
     *
     * @return string
     * @param \PDOStatement the statement to interpolate
     * @param $params The parameters to place into the query
     */
    public function interpolateQuery($statement, $params) {
        $keys = array();
    
        # build a regular expression for each parameter
        foreach ($params as $key => $value) {
            if (is_string($key)) {
                $keys[] = '/:'.$key.'/';
            } else {
                $keys[] = '/[?]/';
            }
        }
    
        $query = preg_replace($keys, $params, $statement->queryString, 1, $count);
    
        #trigger_error('replaced '.$count.' keys');
    
        return $query;
    }
  
}
?>