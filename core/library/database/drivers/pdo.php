<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Database\Driver;
use Core;
/**
 * A generic class to provide common functionality to specific PDO driver implementations
 *
 * This class is meant to be extended, and provides common functionality to the parent classes which extend it.
 * Parent classes are to implement the database specific queries, and this class is to provide functionality which does not
 * require specific database queries.
 *
 * @package \Core\Library\Database\Driver\PDOWrapper
 */
class PDOWrapper extends Core\Library\BaseObject{
    /**
     * The parent DB object
     * @var \Core\Library\Database\DB
     */
    public $db;
    /**
     * A PDO connection object
     * @var \PDO
     */
    protected $connection;
    /**
     * Internal storage for data related to tables
     * @var array
     */
    protected $tableData=array();
    /**
     * Internal storage for data related to table indexes
     * @var array
     */
    protected $tableIndexes=array();
    
    /**
     * An Array to store the names of all the tables that have been updated
     * @var array
     */
    protected $updatedTables=array();
    
    /**
     * The host for the connection
     * @var string
     */
    private $host;
    
    /**
     * The db name for the connection
     * @var string
     */
    private $dbName;
    
    /**
     * Create a new Instance.
     *
     * @return \Core\Library\Database\Driver\PDOWrapper
     * @param \Core\Library\Database\DB $db The parent DB object
     * @param string $host The hostname of the database server
     * @param string $user The user name for the database server
     * @param string $password The password for the database server
     * @param string $database The name of the database to load
     */
    public function __construct($db, $host, $user, $password, $database){
        $this->db = $db;
        try{
            $this->connection = $this->getConnection($host, $user, $password, $database);
            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->host = $host;
            $this->dbName = $database;
        }catch(\PDOException $e){
            throw new \Exception($e->getMessage());
        }
    }
    
    /**
     * Get the host name for the database connection
     * @return string The host name
     */
    public function getHost(){
        return $this->host;
    }
    
    /**
     * Get the database name for the database connection
     * @return string The database name
     */
    public function getDatabaseName(){
        return $this->dbNamr;
    }
    
    /**
     * Method for execution of a generic query. Results will be returned as an array.
     * This function is for use in \Core\Library\Database\Driver\<driver> classes, and not directly from anywhere else
     * Function utilizes the PDO::query() and PDO::fetch() methods. Results will be an associative array
     *
     * @return array The records retreived by the query (if any)
     * @param string $sql The query to be executed on the database
     */
    public function query($sql){
        $return = array();
        try {
            $result = $this->connection->query($sql);
            if($result !== false){
                while($row = $result->fetch(\PDO::FETCH_ASSOC)){
                    $return[] = $row;
                }
                
                return $return;
            }else{
                
                return $return;
            }
        }catch(PDOException $e){
            throw new \Exception($e->getMessage());
        }
    }
    
    /**
     * Loads all \Core\Library\Database\Table objects into memory
     *
     * @see \Core\Library\Database\Driver\PDOWrapper
     *
     * @return null
     * @param bool $forceRefresh When true this method will query the database for fresh data
     */
    public function loadTables($forceRefresh=false){
        if($forceRefresh || empty($this->tableData)){
            if($forceRefresh){
                $this->tableData = array();
            }
            foreach($this->showTables() as $tableName){
                $this->tableData[$tableName] = $this->getTable($tableName);
            }
        }
    }
    
    /**
     * Get a Table object with the 'fields' and 'indexes' properties filled in
     * If $model is sent, it will be passed to the \Core\Library\Database\Table constructor
     * Boolean false is returned if the table can not be fetched
     *
     * @return \Core\Library\Database\Table|false
     * @param string $tableName The name of the table to return
     * @param \Core\Library\Model If $model is provided, it will be associated with the \Core\Library\Database\Table object
     * @param bool $forceRefresh When true, forces the method to query the db again for data
     */
    public function getTable($tableName, $model=null, $forceRefresh=false){
        if($this->tableExists($tableName, $forceRefresh)){
            
            if($forceRefresh || !array_key_exists($tableName, $this->tableData)){
                //The table exists, but it's not in the tableData, So we will build the object from scratch
                $t = $this->registry->load->database('Table', array($this->db, $tableName, $model));
                $t->fields = $this->getFields($tableName, $model, $forceRefresh);
                
                $this->tableData[$tableName] = $t;
                //Build out this tables indexes
                $indexes = $this->getTableIndexes($tableName, $forceRefresh);
                foreach($indexes as $index){
                    $index->objTable = $t;
                    $this->tableData[$tableName]->indexes[$index->key_name] = $index;
                }
                
                
                //set the primary key
                $primary = $this->getPrimaryIndexField($tableName, false);
                if(is_object($primary)){
                    $this->tableData[$tableName]->primaryFieldName = $primary->name;
                }else{
                    throw new \Exception("Unable to find a primary key for table '".$tableName."'. The framework should have created one, but apparently it didn't....grrr...");
                }
            }
            if(!empty($model) && empty($this->tableData[$tableName]->getModel())){
                $this->tableData[$tableName]->setModel($model);
            }
            return $this->tableData[$tableName];
        }else{
            
            $t = $this->registry->load->database('Table', array($this->db, $tableName, $model));
            return $t;
            //return false;
        }
    }
    
    /**
     * Returns the \Core\Library\Database\Field object with a name of $fieldName from the table $tableName, or null if $tableName or $fieldName doesn't exist
     *
     * @return \Core\Library\Database\Field|null
     * @param string $tableName The name of the table to which $fieldName belongs
     * @param string $fieldName The name of the field in $tableName to fetch
     * @param bool $forceRefresh When true the method will query the db for fresh data
     */
    public function getField($tableName, $fieldName, $forceRefresh=false){
        $table = $this->getTable($tableName, null, $forceRefresh);
        foreach($table->fields as $fieldIndex=>$field){
            if($field->name == $fieldName){
                return $table->fields[$fieldIndex];
            }
        }
        return null;
    }
    
    /**
     * Check if $fieldName is a valid column on $tableName
     *
     * @return bool
     * @param string $tableName The name of the table to check if $fieldName exists
     * @param string $fieldName The name of the field to check if it exists on $tableName
     * @param bool $forceRefresh When true the method will query the database for fresh data
     */
    public function fieldExists($tableName, $fieldName, $forceRefresh=false){
        $field = $this->getField($tableName, $fieldName, $forceRefresh);
        if(empty($field)){
            return false;
        }else{
            return true;
        }
    }
    
    /**
     * Returns a \Core\Library\Database\Field object that can be used to uniquely identify a single record in $tableName, or false if one can't be found.
     * If there is a primary index, it will be returned
     * If there is no primary index and an auto increment field then the auto increment field will be returned
     * If there is no primary index and no auto increment field and there is a unique index, the field for the unique index will be used
     *
     * @return \Core\Library\Database\Field|false
     * @param string $tableName The name of the table to get a primary index from.
     * @param bool $forceRefresh When true the method will query the database again for fresh data
     */
    public function getPrimaryIndexField($tableName, $forceRefresh=false){
        $table = $this->getTable($tableName, null, $forceRefresh);
        if($table !== false){
			
			if(!$forceRefresh && isset($table->primaryFieldName) && isset($table->fields[$table->primaryFieldName])){
				return $table->fields[$table->primaryFieldName];
			}else{
                $primary = null;
                $auto_inc = null;
                $unique = null;
				foreach($table->fields as $fld){
					if($fld->primary){
                        $primary = $fld;
					}elseif($fld->auto_inc){
                        $auto_inc = $fld;
                    }elseif($fld->unique){
                        $unique = $fld;
                    }
				}
                if($forceRefresh){
                    $this->tableData[$table->name]->primaryFieldName = null;
                }
                if(!empty($primary)){
                    $this->tableData[$table->name]->primaryFieldName = $primary->name;
                    return $primary;
                }elseif(!empty($auto_inc)){
                    $this->tableData[$table->name]->primaryFieldName = $auto_inc->name;
                    return $auto_inc;
                }elseif(!empty($unique)){
                    $this->tableData[$table->name]->primaryFieldName = $unique->name;
                    return $unique;
                }
			}
        }
		return false;
    }
    
    /**
     * Creates the table described by the $table object. Indexes in the passed $table will be added.
     * A boolean value will be returned indicating if the table creation was successful
     *
     * @return boolean
     * @param \Core\Library\Database\Table $table A table object describing the table that should be added to the database
     */
    public function createTable($table){
        if(is_object($table) && !empty($table->fields)){
            if($this->tableExists($table->name, true)){
                return true;
            }else{
                $this->connection->query($this->createTableQuery($table));
                //Need to process all the indexes ont the table object.
                
                foreach($table->indexes as $index){
                    $this->tableIndexes[$table->name] = array();
                    $this->addIndex($table->name, $index);
                }
                unset($this->tableIndexes[$table->name]);
                unset($this->tableData[$table->name]);
                $this->tableData[$table->name] = $this->getTable($table->name, null, true);
                return true;
            }
            
        }else{
            return false;
        }
    }
    
    /**
     * Updates the databse table to match the \Core\Library\Database\Table object passed.
     * If $table doesn't exist it will be created in the database. Indexes on the passed table object will be added/updated
     * if $model is sent, it will be passed to the \Core\Library\Database\Table constructor
     *
     * @return null
     * @param \Core\Library\Database\Table $table A table object describing the what the final shape of the table should be
     * @param \Core\Library\Model $model (optional) If the $model is passed, it will be associated with the resulting \Core\Library\Database\Table object
     */
    public function updateTable($table, $model=null){
        if(!$this->hasTableUpdated($table->name)){
            $this->setTableUpdated($table->name);
            if(!$this->tableExists($table->name)){
                $this->createTable($table);
            }else{
                //Should check to make sure the table is up to date.
                foreach($table->fields as $field){
                    $this->updateField($table->name, $field);
                }
                //unset($this->tableIndexes[$table->name]);
                //unset($this->tableData[$table->name]);
                $existing = $this->getTable($table->name, $model, true);
                foreach($existing->fields as $field){
                    $found=false;
                    foreach($table->fields as $f){
                        if($f->name == $field->name){
                            $found=true;
                            break;
                        }
                    }
                    if(!$found){
                        $this->removeField($table->name, $field->name);
                    }
                }
    
                //Now we need to check the indexes
                foreach($table->indexes as $index){
                    $this->updateIndex($table->name, $index);
                }
                //unset($this->tableIndexes[$table->name]);
                //unset($this->tableData[$table->name]);
                $existing = $this->getTable($table->name, $model, true);
                //now remove any un-needed indexes
                foreach($existing->indexes as $index){
                    $found=false;
                    foreach($table->indexes as $i){
                       
                       
                        if(count($i->columns) == count($index->columns) && $i->unique == $index->unique && $i->index_type == $index->index_type){
                            $foundAllCols=true;
                            foreach($i->columns as $c){
                                $foundColumn=false;
                                foreach($index->columns as $col){
                                    if($col == $c){
                                        $foundColumn=true;
                                        break;
                                    }
                                }
                                if(!$foundColumn){
                                    $foundAllCols=false;
                                    break;
                                }
                            }
                            if($foundAllCols){
                                $found=true;
                                break;
                            }
                        }
                       
                       
                       
                    }
                    if(!$found){
                        $this->removeIndex($table->name, $index);
                    }
                }
            }
        }
        
    }
    
    /**
     * Returns the \Core\Library\Database\Index object with identical columns, unique, and type propertis as $index
     * If the index doesnt exist, and empty array will be returned.
     *
     * Note: This function could return more than one index, if there are duplicate indexes
     * 
     * @return \Core\Library\Database\Index[]
     * @param string $tableName The name of the table to query for $index
     * @param \Core\Library\Database\Index An index object defining the shape of the index to get
     * @param bool $forceRefresh When true the method will query the database for fresh data
     */
    public function getIndex($tableName, $index, $forceRefresh=false){
        $table = $this->getTable($tableName, null, $forceRefresh);
        $rtn=array();
        if(!empty($table) && !empty($table->indexes)){
            foreach($table->indexes as $indexIndex=>$i){
                if(count($i->columns) == count($index->columns) && $i->unique == $index->unique && $i->index_type == $index->index_type){
                    $foundAllCols=true;
                    foreach($i->columns as $c){
                        $foundColumn=false;
                        foreach($index->columns as $col){
                            if($col == $c){
                                $foundColumn=true;
                                break;
                            }
                        }
                        if(!$foundColumn){
                            $foundAllCols=false;
                            break;
                        }
                    }
                    if($foundAllCols){
                        $rtn[] = $table->indexes[$indexIndex];
                    }
                }
            }
        }
        return $rtn;
    }
    
    /**
     * Returns the \Core\Library\Database\Index object from $tableName with the name $indexName, or false if the index does not exist
     *
     * @return \Core\Library\Database\Index|false
     * @param string $tableName The name of the table to query for $indexName
     * @param string $indexName The name of the index requested from $tableName
     * @param bool $forceRefresh When true this method will query the database for fresh data
     */
    public function getIndexByName($tableName, $indexName, $forceRefresh=false){
        $table = $this->getTable($tableName, null, $forceRefresh);
        if(!empty($table->indexes)){
            foreach($table->indexes as $indexIndex=>$index){
                if($index->key_name == $indexName){
                    return $table->indexes[$indexIndex];
                }
            }
        }
        return false;
    }
    
    /**
     * Check if $indexName exists on $tableName
     *
     * @return bool
     * @param string $tableName The name of the table to check for $index
     * @param \Core\Library\Database\Index $index An index object defining the index being checked
     * @param bool $forceRefresh When true this method will query the database for fresh data
     */
    public function indexExists($tableName, $index, $forceRefresh=false){
        $index = $this->getIndex($tableName, $index, $forceRefresh);
        if(empty($index)){
            return false;
        }else{
            return true;
        }
    }
    
    /**
     * Update index in $tableName to match $index object
     * If $index does NOT exist on $tableName it will be added
     * If there are duplicate indexes, they will be removed.
     *
     * @return null
     * @param string $tableName The name of the table to which $index belongs
     * @param \Core\Library\Database\Index $index An index object to which the database index should match
     */
    public function updateIndex($tableName, $index){
        //$exist = $this->getIndex($tableName, $index);
        $exist = array($this->getIndexByName($tableName,$index->key_name, true));
        $existing = array_shift($exist);
        if(!empty($existing)){
            //$existing could hold more than one index, if there are duplicates...
            
            $update=array();
            if($existing->unique != $index->unique){
                $update['unique']=$index->unique;
            }
            if($existing->key_name != $index->key_name){
                $update['key_name']=$index->key_name;
            }
            if($existing->columns != $index->columns){
                $update['columns']=$index->columns;
            }
            if($existing->index_type != $index->index_type){
                $update['index_type']=$index->index_type;
            }
            if($existing->collation != $index->collation){
                $update['collation']=$index->collation;
            }
            if($existing->cardinality != $index->cardinality){
                $update['cardinality']=$index->cardinality;
            }
            if($existing->sub_part != $index->sub_part){
                $update['sub_part']=$index->sub_part;
            }
            if($existing->null != $index->null){
                $update['null']=$index->null;
            }
            if($existing->comment != $index->comment){
                $update['comment']=$index->comment;
            }
            if($existing->index_comment != $index->index_comment){
                $update['index_comment']=$index->index_comment;
            }
            if(!empty($update)){
                if(count($update) == 1 && array_key_exists('key_name',$update)){
                    //This index just needs to be renamed.
                    //If there is already an index with this name, then just remove this index
                    $removed=false;
                    if(!empty($exist)){ //Remove any duplicate indexes.
                        foreach($exist as $i){
                            if($i->key_name == $index->key_name){
                                $removed=true;
                                $this->removeIndex($tableName,$index);
                                break;
                            }
                        }
                    }
                    if(!$removed){
                        //We actually need to rename the index
                        $this->renameIndex($tableName, $existing->key_name, $update['key_name']);
                    }
                }else{
                    //something other than the key_name should be updated
                    $doUpdate = true;
                    if(count($index->columns) == 1){
                        //There is only one column in this index
                        $idField = $this->getPrimaryIndexField($tableName);
                        $keys = array_keys($index->columns);
                        $key = array_shift($keys);
                        if($index->columns[$key] == $idField->name && $idField->auto_inc && $idField->primary){
                            //The only column in this index is the ID column, which is the primary key
                            $all = $this->getTableIndexes($tableName);
                            if(count($all) == 1){
                                //This is the only index on this table, and the only column on it is the id field which is an auto increment
                                $update=false;
                            }
                        }
                        
                    }
                    if($update){
                        $this->removeIndex($tableName, $index);
                        $this->addIndex($tableName,$index);
                    }
                }
            }else{
            }
        }else{
            $this->addIndex($tableName, $index);
        }
    }
    
    /**
     * Fetch a record by it's id and return a \Core\Library\Database\Record object
     * If the record can not be fetched, the function returns false
     *
     * @see \Core\Library\Database\Record
     *
     * @return \Core\Library\Database\Record|false
     * @param string $tableName The name of the table to be queried for the record
     * @param mixed $id The ID of the record to be fetched
     * @param bool $loadRelationship Boolean indicating if the records should load relationship data. Defaults to false
     */
    public function fetchRecord($tableName, $id, $loadRelationship=false){
        $idField = $this->getPrimaryIndexField($tableName);
        $query = $this->registry->load->database('Query', array($this->getTable($tableName)));
        $query->type($query::QUERY_TYPE_SELECT)->fields('*')->where(array(array('field'=>$idField->name, 'comparison'=>'=','value'=>$id)));
        $recordset = $this->fetchSet($query, $loadRelationship);
        if($recordset->numRecords() > 0){
            foreach($recordset as $record){
                return $record;
            }
        }else{
            return false;
        }
    }
    
    /**
     * Fetch all records from $tableName and return them
     *
     * @see \Core\Library\Database\Recordset
     *
     * @return \Core\Library\Database\Recordset
     * @param string $tableName The name of the table to query
     * @param bool $loadRelationships Boolean indicating if the records should load relationship data. defaults to false
     */
    public function fetchAll($tableName, $loadRelationships=false){
        $query = $this->registry->load->database('Query', array($this->getTable($tableName)));
        return $this->fetchSet($query->type($query::QUERY_TYPE_SELECT)->fields('*'), $loadRelationships);
    }
    
    /**
     * Function will fetch a recordset from $tableName based on the data in $queryData
     * null is returned if the statement can not be executed.
     *
     * @return \Core\Library\Database\Recordset|null
     * @param \Core\Library\Database\Query $query Query object representing the query to be executed
     * @param bool $loadRelationships Boolean indicating if the records relationship data should be loaded. Defaults to false
     */
    public function fetchSet($query, $loadRelationships=false){
        $sqlData = $this->statementFromQuery($query, $loadRelationships);
        $statement = $this->connection->prepare($sqlData['statement'], array(\PDO::ATTR_CURSOR => \PDO::CURSOR_SCROLL));
        if($statement){
            //print $sqlData['statement'].'<br>';
            $statement->execute($sqlData['parameters']);
            $recordset = $this->registry->load->database('Recordset', array($query->getTable(), $statement, $this->totalRecords($sqlData), $loadRelationships));
            return $recordset;
        }
    }
    
    /**
     * Method will return the query that is to be executed on the database
     *
     * @return string The query
     * @param \Core\Library\Database\Query $query The query object
     * @param bool $loadRelationships Boolean indicating if relationship data should be included in the query
     */
    public function statementFromQuery($query, $loadRelationships=false){
        switch($query->getType()){
            case \Core\Library\Database\Query::QUERY_TYPE_SELECT:
                return $this->selectStatementFromQuery($query, $loadRelationships);
                break;
            case \Core\Library\Database\Query::QUERY_TYPE_INSERT:
            case \Core\Library\Database\Query::QUERY_TYPE_UPDATE:
            case \Core\Library\Database\Query::QUERY_TYPE_DELETE:
                return "Not implemented";
            default:
                throw new \Exception("Query Type '".$query->getType()."' is not valid");
        }
    }
    
    /**
     * Determine if a table has already been updated
     *
     * @return bool True if a table has been updated, otherwise false
     * @param string $tableName The name of the table to check.
     */
    public function hasTableUpdated($tableName){
        return array_key_exists($tableName,$this->updatedTables);
    }
    
    /**
     * Mark a table as having been updated
     *
     * @return null
     * @param string $tableName The name of the table to mark
     */
    public function setTableUpdated($tableName){
        $this->updatedTables[$tableName] = $tableName;
    }
}
?>