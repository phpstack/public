<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Database;
use Core;
/**
 * An object to represent a query against a database
 *
 * Methods in this object generally return $this in order to maintain chainability of method calls.
 *
 * <b>Benefits:</b>
 * 
 * Although you can directly execute queries on the database object, it is recommended that queries be constructed utilizing this
 * class. This has several advantages including:
 * 1. This class standardizes the way in which a database is queried, which maintains database independence.
 * 2. Query optimization is handled by the specific database driver, ensuring that the queries executed are as quick as possible
 * 3. Relational data is retreived automatically when requested. Relationships need to be defined in a modules config.php, or a join statement added to the query for this to work
 * 
 * <b>Constructing Queries:</b>
 * 
 * The statements below can be executed in any order, however for readablity it is recomended to use the order below because
 * it closely resembles an actual query statement and will make your code much more readable.
 * 
 * Generally the process of constructing a query is:
 * 1. Define the query type (QUERY_TYPE_SELECT, QUERY_TYPE_INSERT, QUERY_TYPE_UPDATE, QUERY_TYPE_DELETE)
 * 2. Define the fields to be returned
 * 3. Define the table to be queried (may use a table name, a \Core\Library\Database\Table object, or a \Core\Library\Model object)
 * 4. Define any 'joins' that should be a part of this query
 * 5. Define the condition sets of field comparisons for the query (see below for info on condition sets)
 * 6. Execute the query (with or without relationship information).
 *
 * <b>Query Types:</b>
 *
 * This query class can handle the 4 CRUD query types.
 * 1. QUERY_TYPE_SELECT: this is the most complicated to construct using sets of field comparisons
 * 2. QUERY_TYPE_INSERT: Inserting a record requires you set the table, and the {@see \Core\Library\Database\Record} object to be inserted.
 * This record must contain all of the required fields except for the primary index.
 * 3. QUERY_TYPE_UPDATE: Updating a record requires you set the table, and the {@see \Core\Library\Database\Record} object to be inserted
 * This record must contain the primary index, and all required fields. Only fields that have been explicitly set on the record object
 * will be updated.
 * 4. QUERY_TYPE_DELETE: Deleting a record requires you to set the table, and the {@see \Core\Library\Database\Record} object to be deleted.
 * At a minimum the record object must contain a value for the primary index field. All other fields are optional.
 *
 * <b>Fields to return:</b>
 *
 * This is defined using the {@see \Core\Library\Database\Query::fields()} method. There are 3 ways to define the fields that should be returned (Note: if a particular field is from a joined table, you must use either option 1, or option 3)
 * 1. The literal string '*', indicating that all fields should be returned in the result
 * 2. An array of field names, indicating that only the fields in the array should be returned
 * 3. An array of {@see \Core\Library\Database\Field} objects, indicating that only the fields in the array should be returned.
 *
 * <b>Defining the Table</b>
 *
 * Defing which table to query can be done with any of the following methods:
 * 1. {@see \Core\Library\Database\Query::from()} method      : Takes either a {@see \Core\Library\Database\Table} object, or a string that is the table name
 * 2. {@see \Core\Library\Database\Query::fromModel()} method : Takes an {@see \Core\Library\Model} object.
 * 3. {@see \Core\Library\Database\Query::into()} method      : Alias of {@see \Core\Library\Database\Query::from()}. Takes either a {@see \Core\Library\Database\Table} object, or a string that is the table name
 * 4. {@see \Core\Library\Database\Query::intoModel()} method : Alias of {@see \Core\Library\Database\Query::fromModel()}. Takes an {@see \Core\Library\Model} object.
 *
 * <b>Define Joins: </b>
 *
 * You can define as many joined tables as you like with a single query. This is done with the {@see \Core\Library\Database\Query::join()}
 * method. Remember, relationships defined in a modules config.php file can have their records automatically returned without specifying
 * a join. A joined table can be specified using ->join('tableToJoin', 'fieldFromQueriedTable', 'fieldFromTableToJoin'). See example below
 * 
 * <b>Field Comparisons: </b>
 * 
 * To compare a field to a value, you will use field comparisons. The anatomy of a field comparison requires that each
 * field comparison contain 3 pieces of information:
 * 1. 'field'       : Either the name of the field to compare, or a {@see \Core\Library\Database\Field} object. Note: if the field is from a joined table, you must use a field object
 * 2. 'comparison'  : The comparison operator to use to compare the 'field' to the 'value' i.e. '=', '>', '<', '>=', '<=', '!=', 'LIKE'
 * 3. 'value'       : The value to compare the field to
 *
 * <b>Condition Sets: </b>
 * 
 * A condition set is an array of one or more field comparisons (a.k.a. $fieldData). When multiple field comparisons are present in
 * a condition set, they are added to the query one at a time using either ->where(), ->andWhere, or ->orWhere(), and the condition sets
 * are concatenated together with either 'AND' or 'OR' (which is the second parameter of ->where, ->andWhere() and ->orWhere(): a.k.a $fieldComparison).
 *
 * <b>Executing a Query</b>
 *
 * Executing a query is done with the {@see \Core\Library\Database\Query::execute()} method. This method accepts one parameter, a boolean value
 * indicating if you want the result to contain records from related tables. In order to get relationship information, a relationship must
 * be defined in the modules config.php file. The result of the execution of a query will be a {@see \Core\Library\Database\Recordset} object.
 *
 * Alternatively one can call the {@see \Core\Library\Database\Query::getStatement()} method, in which case the query will not be executed,
 * but rather the query generated will be returned.
 *
 * <b>Deconstructed MySQL query:</b>
 * 
 * ```php
 * $sql = "
 * SELECT                   //The query type i.e ->type('SELECT')
 *  *                       //Fields to return i.e. ->fields('*')
 * FROM
 *  tableToQuery            //The table to query i.e. ->from('tableToQuery')
 * LEFT JOIN tableToJoin ON //Table join defined with ->join('tableToJoin', 'queryTableFieldName', 'joinTableFieldName')
 *  tableToQuery.queryTableFieldName = tableToJoin.joinTableFieldName
 * WHERE                    //Begin defining condition sets
 * (                        //i.e. ->where(array(
 *  fieldName='value'       //              array('field'=>'fieldNameOrObject', 'comparison'=>'=', 'value'=>'value')
 *  OR
 *  fieldName='value2'      //              array('field'=>'fieldNameOrObject', 'comparison'=>'=', 'value'=>'value2')
 * )                        //            , 'OR')
 * AND                      //We can use andWhere(), or where() with the $appendOperator='AND', but andWhere will be more readable
 * (                        //i.e. ->andWhere(array(
 *  field2 >= 3             //                  array('field'=>'field2NameOrObject', 'condition'=>'>=', 'value'=>'3')
 * )                        //               );
 * ";
 * ```
 *
 * <b>Example Queries:</b>
 * 
 * ```php
 *
 * //In MySQL The following query is roughly equivelent to
 * //"SELECT * FROM tableToQuery WHERE (fieldName='valueToCompare1' OR fieldName='valueToCompare2') AND (anotherField <= 'valueToCompare3')
 * 
 * $query = $registry->load->database('Query', array('tableToQuery'))
 *  ->type('SELECT')
 *  ->fields('*')
 *  ->from('tableToQuery')
 *  ->join('tableToJoin', 'queryTableFieldName', 'joinTableFieldName')
 *  ->where(array(
 *              'field'=>'fieldNameOrObject',
 *              'comparison'=>'=',
 *              'value'=>'valueToCompare1'
 *          ),
 *          array(
 *              'field'=>'fieldNameOrObject',
 *              'comparison'=>'=',
 *              'value'=>'valueToCompare2'
 *          ),
 *      ), 'OR')
 *  ->where(array(
 *              'field'=>'anotherFieldNameOrObject',
 *              'comparison'=>'<=',
 *              'value'=>'valueToCompare3'
 *      ), null, 'AND');    //since there is only one field in this condition set, the second parameter is not used, so we'll just use null
 *
 * //andWhere() and orWhere() can be used in place of where(), and can make the conditions more readable.
 * //The same query written another way using the andWhere() method instead of 2 where() calls as below
 *
 * $query = $registry->load->database('Query', array('tableToQuery'))
 *  ->type('SELECT')
 *  ->fields('*')
 *  ->from('tableToQuery')
 *  ->join('tableToJoin', 'queryTableFieldName', 'joinTableFieldName')
 *  ->where(array(
 *              'field'=>'fieldNameOrObject',
 *              'comparison'=>'=',
 *              'value'=>'valueToCompare1'
 *          ),
 *          array(
 *              'field'=>'fieldNameOrObject',
 *              'comparison'=>'=',
 *              'value'=>'valueToCompare2'
 *          ),
 *      ), 'OR')
 *  ->andWhere(array(
 *              'field'=>'anotherFieldNameOrObject',
 *              'comparison'=>'<=',
 *              'value'=>'valueToCompare3'
 *  ));
 *
 * ```
 *
 * @package \Core\Library\Database\Query
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Query extends Core\Library\BaseObject{
    /**
     * A constant to define the default field type
     * @var string
     */
    const FIELD_TYPE_DEFAULT='default';
    
    /**
     * A constant to define the default field type
     * @var string
     */
    const FIELD_TYPE_JOIN='join';
    
    /**
     * A constant to define the "SELECT" type query
     * @var string
     */
    const QUERY_TYPE_SELECT='SELECT';
    
    /**
     * A constant to define the "INSERT" type query
     * @var string
     */
    const QUERY_TYPE_INSERT='INSERT';
    
    /**
     * A constant to define the "UPDATE" type query
     * @var string
     */
    const QUERY_TYPE_UPDATE='UPDATE';
    
    /**
     * A constant to define the "DELETE" type query
     * @var string
     */
    const QUERY_TYPE_DELETE='DELETE';
    
    /**
     * The \Core\Library\Database\Table to be queried
     * @var \Core\Library\Database\Table
     */
    private $table;
    
    /**
     * The parameter used to instantiate this class
     * @var mixed
     */
    private $constructTable;
    
    /**
     * The type of query to be executed.
     * @var string
     */
    private $queryType;
    
    /**
     * The fields to be returned by the query
     * @var array
     */
    private $fields=array('*');
    
    /**
     * The conditions for this query
     * @var array
     */
    private $conditions=array();
    
    /**
     * The joins for this query
     * @var array
     */
    private $joins=array();
    
    /**
     * The orderBy data for this query
     * @var array
     */
    private $orderBy=array();
    
    /**
     * Creates a new instance.
     *
     * @see \Core\Library\Database\Query::setTable()
     *
     * @return \Core\Library\Database\Query
     * @param \Core\Library\Database\Table|string $table The table object or the table name to be queried
     */
    public function __construct($table){
        $this->queryType = self::QUERY_TYPE_SELECT;
        $this->constructTable = $table;
    }
    
    /**
     * initialize the query. Sets the ->from to the table that the query was created with
     *
     * @return null
     */
    public function init(){
        $this->from($this->constructTable);
    }
    
    /**
     * Set the table to be queried
     * Accepts either a valid table name, or a \Core\Library\Database\Table object. If a table name is sent, the the table object will be
     * pulled from the db object in the registry.
     * 
     * @return \Core\Library\Database\Query $this
     * @param \Core\Library\Database\Table|string The table to be queried. If a string is sent, the table object is pulled from the db in the registry
     */
    public function from($table){
        if(!is_object($table) || get_class($table) != 'Core\Library\Database\Table'){
            $table = $this->registry->db->getTable($table);
            if(!$table){
                throw new \Exception("Invalid table '".$table."'");
            }
        }
        if($table && is_object($table) && get_class($table) == 'Core\Library\Database\Table'){
            $this->table = $table;
        }else{
            throw new \Exception('Core\Library\Database\Query::setTable expects either a Core\Library\Database\Table object, or a valid table name');
        }
        return $this;
    }
    
    /**
     * Alias of \Core\Library\Database\Query::from()
     *
     * @see \Core\Library\Database\Query::from()
     *
     * @return \Core\Library\Database\Query $this
     * @param \Core\Library\Database\Table|string The table to be queried. If a string is sent, the table object is pulled from the db in the registry
     */
    public function into($table){
        return $this->from($table);
    }
    
    /**
     * Alias of \Core\Library\Database\Query::fromModel()
     *
     * @see \Core\Library\Database\Query::fromModel()
     *
     * @return \Core\Library\Database\Query
     * @param \Core\Library\Model $model A module's model to associate with this Query's \Core\Library\Database\Table object
     */
    public function intoModel($model){
        return $this->fromModel($model);
    }
    
    /**
     * This method is NOT used in \Core\Library\Database\Query::QUERY_TYPE_SELECT type queries
     * $record is an array of record objects. Multiple records can be processed at a time.
     * This method will overwrite any previous records that were set.
     *
     * @return \Core\Library\Database\Query
     * @param \Core\Library\Database\Record[] $records The records to be inserted/updated/deleted
     */
    public function records($records){
        $this->records = $records;
        return $this;
    }
    
    /**
     * Get the table object for this query
     *
     * @return \Core\Library\Database\Table
     */
    public function getTable(){
        return $this->table;
    }
    
    /**
     * Uses the $model to set the table to be queried
     * This is preferred to setTable method, as we can ensure that the \Core\Library\Database\Table object contains the model
     * Returns $this to allow chainablity
     *
     * @return \Core\Library\Database\Query
     * @param \Core\Library\Model $model A module's model to associate with this Query's \Core\Library\Database\Table object
     */
    public function fromModel($model){
        if(!is_object($model) || !(get_class($model) == 'Core\Library\Model' || is_subclass_of($model,'Core\Library\Model'))){
            throw new \Exception("Core\Library\Database\Query::setModel expects parameter to be of the type Core\Library\Model");
        }
        $this->from($model->getTable());
        return $this;
    }
    
    /**
     * Sets the query type.
     *
     * @return \Core\Library\Database\Query
     * @param string $type The type of query. Valid types are:
     * \Core\Library\Database\Query::QUERY_TYPE_INSERT
     * \Core\Library\Database\Query::QUERY_TYPE_SELECT
     * \Core\Library\Database\Query::QUERY_TYPE_UPDATE
     * \Core\Library\Database\Query::QUERY_TYPE_DELETE
     */
    public function type($type){
        $this->queryType = $type;
        return $this;
    }
    
    /**
     * Get the query type.
     *
     * @return string
     */
    public function getType(){
        return $this->queryType;
    }
    
    /**
     * Adds the fields that are to be returned to this query.
     * If this method is not called, the default is to return all fields '*'
     * If this method is called multiple times, only fields from the last call will be returned
     * This method is not used for query types QUERY_TYPE_INSERT, QUERY_TYPE_UPDATE, or QUERY_TYPE_DELETE
     *
     * @todo allow this method to accept field objects
     * @todo convert all field names to field objects
     *
     * @return \Core\Library\Database\Query
     * @param \Core\Library\Database\Field[]|string[]|string $fields An array of field names to be included. May also be the string '*' to return all fields
     */
    public function fields($fields){
        if(is_array($fields)){
            $this->fields = $fields;
        }elseif($fields == '*'){
            $this->fields = array($fields);
        }else{
            throw new \Exception(get_class($this)."::addFields() requires either an array of field names, field objects, or the string '*'");
        }
        return $this;
    }
    
    /**
     * Get an array of field objects that are to be returned in the query
     *
     * @return \Core\Library\Database\Field[]
     */
    public function getFields(){
        return $this->fields;
    }
    
    /**
     * Get an array of conditions for this query
     *
     * @return array
     */
    public function getConditions(){
        return $this->conditions;
    }
    
    /**
     * Get all the relationship records that are manually joined in this query
     * @return \Core\Library\Database\Relationship[] The relationship records for joined tables
     */
    public function getJoins(){
        return $this->joins;
    }
    
    /**
     * Alias of \Core\Library\Database\Query::where();
     * 
     * Adds an "AND" condition set to this query
     * This method is not used for query types QUERY_TYPE_INSERT, QUERY_TYPE_UPDATE, or QUERY_TYPE_DELETE
     *
     * @see \Core\Library\Database\Query::where()
     *
     * @return \Core\Library\Database\Query
     * @param array $fieldData The fields that are included in this condition. This array should be in the format array(array('field'=>'fieldnameOrObject', 'comparison'=>'comaprison type', 'value'=>'comparison value'))
     * @param string $fieldComparison (optional) This should be the comparison type for the field=>value pairs in $fieldData. Valid values are 'AND' and 'OR'
     */
    public function andWhere($fieldData, $fieldComparison='AND'){
        return $this->where($fieldData, $fieldComparison, 'AND');
    }
    
    /**
     * Alias of \Core\Library\Database\Query::where();
     * 
     * Adds an "OR" condition set to this query
     * This method is not used for query types QUERY_TYPE_INSERT, QUERY_TYPE_UPDATE, or QUERY_TYPE_DELETE
     *
     * @see \Core\Library\Database\Query::where()
     *
     * @return \Core\Library\Database\Query
     * @param array $fieldData The fields that are included in this condition. This array should be in the format array(array('field'=>'fieldnameOrObject', 'comparison'=>'comaprison type', 'value'=>'comparison value'))
     * @param string $fieldComparison (optional) This should be the comparison type for the field=>value pairs in $fieldData. Valid values are 'AND' and 'OR'
     */
    public function orWhere($fieldData, $fieldComparison='AND'){
        return $this->where($fieldData, $fieldComparison, 'OR');
    }
    
    /**
     * Adds a condition set to this query.
     * Note: This method is called by {@see \Core\Library\Database\Query::andWhere()} and {@see \Core\Library\Database\Query::orWhere()}
     * Note: This method is not used for query types QUERY_TYPE_INSERT, QUERY_TYPE_UPDATE, or QUERY_TYPE_DELETE
     *
     * If this is the only condition set, or the first condition set then $appendOperator is not needed or used.
     * Subsequent condition sets MUST have the $appendOperator parameter (although it defaults to 'AND', so isn't
     * needed if the value should be 'AND')
     * 
     * See the class description for more information on condition sets.
     *
     * @return \Core\Library\Database\Query
     * 
     * @param array $fieldData The fields that are included in this condition. This array should have one record for each field, and be in the format array(array('field'=>'fieldnameOrObject', 'comparison'=>'comaprison type', 'value'=>'comparison value'))
     * @param string $fieldComparison (optional) This should be the evaluation type for the field=>value pairs in $fieldData. Valid values are 'AND' and 'OR'. Defaults to 'AND'
     * @param string $appendOperator (optional) When more than one condition set is present, this is how this condition sets will be appended. Valid values are 'AND' and 'OR'. Defaults to 'AND'
     */
    public function where($fieldData, $fieldComparison='AND', $appendOperator='AND'){
        if(!empty($fieldData)){
            foreach($fieldData as $i=>$compare){
                if(!is_object($compare['field'])){
                    $fieldData[$i]['field'] = $this->getTable()->getField($compare['field']);
                }
            }
        }
        $this->conditions[] = array(
            'fieldComparison'=>$fieldComparison,
            'fields'=>$fieldData,
            'appendOperator'=>$appendOperator
        );
        return $this;
    }
    
    /**
     * Validate this query to make sure it follows all the "rules"
     * This method will throw an exception if an invalid query is detected
     *
     * @return null
     * @param bool $validateRelationships (optional) Boolean indicating if validation should include relationship data. Defaults to false
     */
    
    /**
     * Join another table to this query. Returns $this for chainability
     *
     * <b>Example:</b>
     * ```php
     * 
     * ```
     * 
     * @return \Core\Library\Database\Query
     * @param \Core\Library\Database\Table|string $table The table object or name to join with this query
     * @param \Core\Library\Database\Field|string $localField The local field object or name to join $table on
     * @param \Core\Library\Database\Field|string $remoteField The remote field object or name from $table to join $table on
     * @param string $relationshipName A user defined name to identify the related data. If one is not used, the name will be custom_{index of the join statement}
     */
    public function join($table, $localField, $remoteField, $relationshipName=null){
        if(!is_object($table)){
            $table = $this->load->database('Table', array($this->table->getDB(),$table));
        }
        if(!is_object($localField)){
            $localField = $this->load->database('Field',array($this->table, $localField));
        }
        if(!is_object($remoteField)){
            $remoteField = $this->load->database('Field',array($this->table, $remoteField));
        }
        if(empty($relationshipName)){
            $relationshipName = 'custom_'.count($this->joins);
        }
        $rel = $this->load->database('Relationship',array($this->table,$relationshipName,false));
        $rel->setRemoteTable($table)->setRemoteField($remoteField)->setLocalField($localField);
        $this->joins[] = $rel;
        return $this;
    }
    
    /**
     * Set the order of the results of this query. The $field parameter may either be a field object, or a field name.
     * 
     * @return \Core\Library\Database\Query Return value is $this for chainability
     * @param \Core\Library\Database\Field|string $field Either the field object, or the field name to order by
     * @param string $direction (optional) The direction to sort by. Either 'ASC' or 'DESC'. Defaults to 'ASC'
     */
    public function orderBy($field, $direction='ASC'){
        if(!is_object($field)){
            $field = $this->load->database('Field',array($this->table, $field));
        }
        $this->orderBy = array(
            'field'=>$field,
            'direction'=>$direction
        );
    }
    
    /**
     * Get the orderBy data for this query
     * @return array An array containing the data about how to order the results of this query
     */
    public function getOrderBy(){
        return $this->orderBy;
    }
    
    
    /**
     * Validate this query to make sure that all the rules have been followed. An exception will be thrown if the query is not valid
     * @return null
     * @param bool $validateRelationships A boolean indicating if the relationships should be validated as well
     */
    private function validate($validateRelationships=false){
        //Make sure we have a table set, and it is a valid table
        if(empty($this->table) || get_class($this->table) !== 'Core\Library\Database\Table'){
            throw new \Exception("No table was set for the query");
        }
        if($this->getType() == self::QUERY_TYPE_SELECT){
            $this->validateFields();
            $this->validateConditions();
        }else{
            $this->validateWriteData();
        }
        
    }
    
    /**
     * Method will use the \Core\Library\Database\Query::validate() method to validate the query, however with this method
     * if the query is not valid an exception will not be thrown, but false will be returned. true is returned for valid queries
     *
     * @see \Core\Library\Database\Query::validate()
     *
     * @return bool
     * @param bool $validateRelationships (optional) Boolean indicating if validation should include relationship data. Defaults to false
     */
    public function isValid($validateRelationships=false){
        try{
            $this->validate();
            return true;
        }catch(\Exception $e){
            return false;
        }
    }
    
    /**
     * Only used for QUERY_TYPE_SELECT type queries
     * Method will verify that all fields are valid for the table(s) being queried
     * If no fields have been set, or there is an invalid field the Method will throw an Exception, otherwise true will be returned
     * Fields within conditions will be validated by the validateConditions() method
     *
     * @see \Core\Library\Database\Query::validate()
     * @todo Need to update \Core\Library\Database\Query::validateFields() method to utilize the $validateRelationships parameter
     * 
     * @return bool
     * @param bool $validateRelationships (optional) Not implemented yet. Boolean indicating if validation should include relationship data. defaults to false
     */
    private function validateFields($validateRelationships=false){
        if($this->queryType == self::QUERY_TYPE_SELECT){
            if(!empty($this->fields)){
                $keys = array_keys($this->fields);
                if(count($this->fields) == 1 && $this->fields[array_shift($keys)] == '*'){
                    //This is fine....carry on
                }else{
                    foreach($this->fields as $field){
                        if($field == '*' || !$this->table->fieldExists($field)){
                            throw new \Exception("Field '".$field."' is not valid for table '".$this->table->name."'");
                        }
                    }
                }
                
                return true;
            }else{
                throw new \Exception("At least one field should be set for this query");
            }
        }else{
            return true;
        }
    }
    
    /**
     * Only used for QUERY_TYPE_SELECT type queries
     * Method will verify that all conditions are valid for the table(s) being queried
     * If there is an invalid condition the method will throw an Exception, otherwise true will be returned
     *
     * @see \Core\Library\Database\Query::validate()
     * @todo Need to update \Core\Library\Database\Query::validateConditions() method to utilize the $validateRelationships parameter
     * @todo Need to update \Core\Library\Database\Query::validateConditions() method to allow for fields from related tables
     * 
     * @return bool
     * @param bool $validateRelationships (optional) Not implemented yet. Boolean indicating if validation should include relationship data. defaults to false
     */
    private function validateConditions($validateRelationships=false){
        if($this->getType() == self::QUERY_TYPE_SELECT){
            if(!is_array($this->conditions)){
                throw new \Exception("Field Data for a where condition should be a 2-dimensional array containing 3 fields array(array('field'=>'fieldName','comparison'=>'=','value'=>'value'))");
            }
            //$this->registry->utility::print_array($this->conditions);
            foreach($this->conditions as $condition){
                foreach($condition['fields'] as $field){
                    //Make sure the condition has the proper fields
                    if(!array_key_exists('field', $field) || !array_key_exists('comparison', $field) || !array_key_exists('value', $field)){
                        throw new \Exception("Field Data for a where condition should be a 2-dimensional array containing 3 fields array(array('field'=>'fieldName','comparison'=>'=','value'=>'value'))");
                    }
                    
                    //Make sure the 'field' field is a valid field for this table
                    if(!$this->table->fieldExists($field['field']->name)){
                        //Check to see if the field exists in one of the joined tables
                        $found = false;
                        foreach($this->joins as $rel){
                            if($rel->getRemoteTable()->name == $field['field']->getTable()->name && $rel->getRemoteTable()->fieldExists($field['field']->name)){
                                $found=true;
                                break;
                            }
                        }
                        if(!$found){
                            throw new \Exception("Queried field '".$field['field']->name."' is not valid for table '".$this->table->name."'");
                        }
                    }
                    
                    //Make sure the comparison field contains valid data
                    switch($field['comparison']){
                        case "=":
                        case "!=":
                        case ">":
                        case ">=":
                        case "<":
                        case "<=":
                        case "LIKE":
                            break;
                        default:
                            throw new \Exception("Comparison Operator '".$field['comparison']."' used for field '".$field['field']."' is not valid.");
                    }
                }
            }
        }
    }
    
    /**
     * Only used for QUERY_TYPE_INSERT, QUERY_TYPE_UPDATE, and QUERY_TYPE_DELETE type queries
     * Method will validate that the data needed for the write operation is valid.
     * Query must have reocrd(s) set, and each record must have the primary key field filled in
     * QUERY_TYPE_INSERT type queries must have data for all non-null fields.
     * QUERY_TYPE_UPDATE type queries will only update fields that have been explicitly set. An explicitly set field can not have a null value, if the db doesn't accept null values for the field
     *
     * If data is found to be invalid an exception will be thrown, otherwise true will be returned
     *
     * This function is only intended to validate the structure of the query, and does not validate the uniqueness of uniqe fields
     * or any other validations specific to the data being written.
     *
     * @return bool
     */
    private function validateWriteData(){
        //Make sure we have records
        if(empty($this->records)){
            throw new \Exception("Query type '".$this->queryType."' must have at least one record set with ->records()");
        }
        //Make sure the records we have are an array
        if(!is_array($this->records)){
            throw new \Exception("Invalid record. An array of records must be set for query type '".$this->queryType."'.");
        }
        $idField = $this->table->getPrimaryField();
        foreach($this->records as $record){
            //Each element in the records array must be the right type
            if(!is_object($record) || get_class($record) !== 'Core\Library\Database\Record'){
                //$this->registry->utility::print_array($record);
                throw new \Exception("Record type '".get_class($record)."' is not valid. Each record must be a Core\Library\Database\Record object");
            }
            
            if($this->queryType == self::QUERY_TYPE_UPDATE || $this->queryType == self::QUERY_TYPE_DELETE){
                //Each record in the array must have the primary key field filled in.
                $idval = $record->{$idField->name};
                if(empty($idval)){
                    throw new \Exception("Each record set for a query must have the primary key field filled in.");
                }
            }
            
            //If this is an insert query, then all non-null fields must have a value
            if($this->queryType == self::QUERY_TYPE_INSERT){
                foreach($this->table->getFields() as $field){
                    $val = $record->{$field->name};
                    if($field->name != $idField->name && !$field->null && empty($val)){
                        throw new \Exception("Field '".$field->name."' on the table '".$this->table->name."' can not be null. Set the value for this field before inserting");
                    }
                }
            }elseif($this->queryType == self::QUERY_TYPE_UPDATE){
                //If this is an update type query non-null fields that have been explicitly set can not be null
                foreach($record->getSetFields() as $fieldName){
                    $f = $this->table->getField($fieldName);
                    $val = $record->{$fieldName};
                    if(!empty($f) && !$f->null && empty($val)){
                        throw new \Exception("Field '".$f->name."' on the table '".$this->table->name."' can not be null. Set the field to a non-null value before updating");
                    }
                }
            }
        }
    }
    
    /**
     * Returns the database specific query statement that is generated by this query object.
     * Method will throw an exception if the query can not be validated
     *
     * @see \Core\Library\Database\Query::validate()
     *
     * @return string
     * @param bool $loadRelationships Boolean indicating if the query should fetch relationship data. defaults to false
     */
    public function getStatement($loadRelationships=false){
        $this->validate($loadRelationships);
        return $this->table->getDB()->statementFromQuery($this, $loadRelationships);
    }
    
    /**
     * Execute this query, and return the resulting recordset
     * Method will throw an exception if the query can not be validated
     *
     * @see \Core\Library\Database\Driver\PDOWrapper::fetchSet()
     * @see \Core\Library\Database\Query::validate()
     *
     * @return \Core\Library\Database\Recordset
     * @param bool $loadRelationships Boolean indicating if relationship data should be included in the records. defaults to false.
     */
    public function execute($loadRelationships=false){
        $this->validate($loadRelationships);
        if($this->queryType == self::QUERY_TYPE_SELECT){
            return $this->table->getDB()->fetchSet($this, $loadRelationships);
        }elseif($this->queryType == self::QUERY_TYPE_INSERT){
            return $this->table->getDB()->insertRecords($this->records, $loadRelationships);
        }elseif($this->queryType == self::QUERY_TYPE_UPDATE){
            return $this->table->getDB()->updateRecords($this->records, $loadRelationships);
        }elseif($this->queryType == self::QUERY_TYPE_DELETE){
            return $this->table->getDB()->deleteRecords($this->records, $loadRelationships);
        }
    }
    
}