<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Database;
use Core;
/**
 * An Iterable class representing a collection of records as a result of a database query
 * 
 * The class must be given a PDOStatement object when being constructed, which will subsequently be used to fetch the records
 * in the recordset.
 * 
 * This class is an iterable set of \Core\Library\Database\Record objects, and can be used directly in a foreach statement. The class
 * also implements PHP's ArrayAccess, allowing the results to be accessed in a manner similar to an array.
 *
 * **Cahe Mode:**
 *
 * When $this->cacheMode is true, results from the query will be added to memory ($this->rows) as they are accessed.
 * With large result sets, this will have high memory usage.
 * This mode is much faster, IF (and only if) the results are accessed more than once, otherwise there is no speed difference.
 * 
 * **NO Cache Mode:**
 *
 * When $this->cacheMode is false, this class will only ever hold ONE record in memory at a time, making it possible to
 * work through an unlimited number of rows with lmited memory. In this mode, EACH time the recordset it iterated through,
 * The Database query which produced the results will be executed again, making it slower on the 2nd-nth iteration.
 *
 * The parameter $this->maxToCache is the maximum number of records that will be cached. If the recordset has more than this
 * number of records, then cacheMode will be set to false;
 *
 *
 * @package \Core\Library\Database\Recordset
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Recordset extends Core\Library\BaseObject implements \Iterator, \ArrayAccess{

    /**
     * The table that was queried. Used when creating \Core\Library\Database\Record objects
     * @var \Core\Library\Database\Table
     */
    private $table;
    /**
     * The current position of the iterator
     * @var int
     */
    private $position=0;
    /**
     * The number of records returned in the PDOStatement
     * @var int
     */
    private $totalRows;
    /**
     * The PDOStatement that was executed to produce this result set
     * @var \PDOStatement
     */
    private $PDOStatement;
    /**
     * The current \Core\Library\Database\Record object
     * @var \Core\Library\Database\Record
     */
    private $record;
    /**
     * Determines if this object is to be run in cached mode
     * @var bool
     */
    private $cacheMode=true;
    /**
     * When run in cached mode, this array holds the records in the recordset
     * @var array
     */
    private $rows = array();
    /**
     * An array to track the records that have been accessed
     * @var array
     */
    private $accessed=array();
    /**
     * The position of the currently loaded record
     * @var int
     */
    private $recordPosition=null;
    /**
     * When run in cahced mode, the maximum number of records to cache
     * @var int
     */
    public $maxToCache=100;
    
    /**
     * Boolean indicating if the recordset contains relationship data
     * @param bool
     */
    private $loadRelationship;
    
    /**
     * Creates a new instance.
     * 
     * Note: If $totalRows is greater than the $maxToCache property, this object will **not** run in cached mode
     *
     * @return \Core\Library\Database\Recordset
     * @param \Core\Library\Database\Table $table the Table object the records in this set are from
     * @param \PDOStatement $PDOStatement The PHP PDOStatement object of the query that was executed
     * @param int $totalRows The total number of rows in this recordset. since PDOStatment can't reliably determine the number of records in a data set, this parameter is required
     * @param bool $loadRelationship Boolean indicaing if relationship information is included in the results
     */
    public function __construct($table, $PDOStatement, $totalRows, $loadRelationship){
        $this->table = $table;
        $this->PDOStatement = $PDOStatement;
        $this->loadRelationship = $loadRelationship;
        //$this->PDOStatementClone = clone $PDOStatement;
        $this->totalRows = $totalRows;
        if($totalRows > $this->maxToCache){
            $this->cacheMode=false;
        }
        if(!empty($this->registry)){
            $this->record = $this->registry->load->database('Record', array($this->table, null));
            $this->PDOStatement->setFetchMode(\PDO::FETCH_INTO, $this->record);
        }
        
    }
    
    /**
     * Method rewinds the pointer to the beginning of the recordset
     * 
     * Note: if this object is **not** in cached mode, the PDOStatement will be executed against the database again.
     */
    public function rewind(){
        if(!$this->cacheMode && !empty($this->accessed)){
            //Excute the query again, so we can move through the result set again.
            $this->PDOStatement->closeCursor();
            $this->PDOStatement->execute();
            $this->accessed = array();
        }
        $this->recordPosition = null;
        $this->position = 0;
    }
    
    /**
     * Returns the \Core\Library\Database\Record that is currently pointed to
     *
     * @return \Core\Library\Database\Record
     */
    public function current(){
        if(array_key_exists($this->position,$this->rows)){  //Either cached, or has been manually set
            return $this->rows[$this->position];
        }else{
            if(empty($this->record) && !empty($this->registry)){
                $this->record = $this->registry->load->database('Record', array($this->table, null));
                $this->PDOStatement->setFetchMode(\PDO::FETCH_INTO, $this->record);
            }
            if(!$this->cacheMode && in_array($this->position, $this->accessed)){
                //We aren't caching, and we need to get an element that is not in the PDO anymore. No choice but to execute again.
                $position = $this->position;
                $this->rewind();
                $this->position = $position;
            }
            $this->record = $this->PDOStatement->fetch(\PDO::FETCH_INTO, \PDO::FETCH_ORI_ABS, $this->position);
            $this->record = $this->parseRelationship($this->record);
            if($this->cacheMode){
                $this->rows[$this->position] = clone $this->record;
            }else{
                $this->accessed[$this->position] = $this->position;
            }
            
        }
        $this->recordPosition = $this->position;
        
        return $this->record;
    }
    
    /**
     * Get the key currently pointed to by the pointer
     *
     * @return int
     */
    public function key(){
        return $this->position;
    }
    
    /**
     * Move the pointer to the next position in the recordset
     *
     * @return null
     */
    public function next(){
        ++$this->position;
    }
    
    /**
     * Determine if the position currently pointed to is valid
     *
     * @return bool
     */
    public function valid(){
        if($this->position < $this->totalRows){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Determin if the $offset is valid
     *
     * @return bool
     * @param $offset The array key to determine if it exists
     */
    public function offsetExists($offset){
        return ($offset < $this->totalRows);
    }
    
    /**
     * Return the record positioned at $offset.
     * Allows for array like direct access to a record in the set
     *
     * @return \Core\Library\Database\Record
     * @param int|string $offset The array index to get
     */
    public function offsetGet($offset){
        $before = $this->position;
        $this->position = $offset;
        $record = $this->current();
        $this->position = $before;
        return $record;
    }
    
    /**
     * Required to implement ArrayAccess, but can not be used in this context
     * 
     * @return null
     * @param mixed $offset The array index to set
     * @param mixed $value The value to set $offset to
     */
    public function offsetSet($offset, $value){
        if(!is_object($value) || get_class($value) !== '\Core\Library\Database\Record'){
            throw new \Exception("Only \Core\Library\Database\Record objects can be added to a Recordset");
        }
        if($offset >= $this->totalRows){
            $this->totalRows++;
        }
        $this->rows[$offset] = $value;
    }
    
    /**
     * Required to implement ArrayAccess, but can not be used in this context
     *
     * @return null
     * @param int|string $offset The offset to remove from the dataset
     */
    public function offsetUnset($offset){
        throw new \Exception("Unsetting a value for '".get_class($this)."' is not allowed.");
    }
    
    /**
     * Get the number of records in this recordset
     *
     * @return int
     */
    public function numRecords(){
        return $this->totalRows;
    }
    
    /**
     * Retrun an HTML table for $record. If $record is not passed, then a table will be built for the current record
     * Relationship data will be output recursively. Related records will be output into a table contained within a field.
     *
     * @return string
     * @param bool $outputRelated Boolean value indicating if related recrods should be included in the table.
     */
    public function toHTML($outputRelated=false){
        $html = '
        <table class="table" border="1">';
        $subs = array();
        if($this->totalRows > 0){
            $first=true;
            $this->rewind();
            while($this->valid()){
                $rec = $this->current();
                if($first){
                    $html .= $this->toHTMLHeadRow($rec,$outputRelated).'<tbody>';
                }
                $html .= $this->toHTMLRow($rec, $outputRelated);
                
                $this->next();
            }
            $html .= '
                    </tbody>';
        }else{
            $html .= '
            <tbody>
                <tr>
                    <td>No records in the recordset</td>
                </tr>
            </tbody>';
        }
        $html .= '</table>';
        return $html;
    }
    
    /**
     *  Builds the <thead> portion of the toHTML method
     *  if $outputRelated is true, then a column will be added for each relationship and the record rows
     *  will contain a table with each realted record for that relationship within those columns 
     *
     *  @return string The HTML markup of the <thead> row
     *  @param \Core\Library\Database\Record $record The record being output to HTML
     *  @param bool $outputRelated (optional) Boolean indicating if related records should be output. Defaults to false
     */
    private function toHTMLHeadRow($record, $outputRelated=false){
        $html = '<thead><tr>';
        $data = $record->toArray();
        foreach($data as $field=>$value){
            if(is_array($value)){
                if($outputRelated){
                    foreach($value as $relateName=>$value){
                        $records = $record->getRelatedRecords($relateName);
                        foreach($records as $r){
                            $relate = $r->getParentRecord();
                            if(!empty($relate)){
                                $relate = $relate->getTable();
                                if(!empty($relate)){
                                    $relate = $relate->getRelationship($relateName);
                                    
                                }
                            }
                            break;
                        }
                        if(!empty($relate)){
                            $table = ' (table: '.$relate->getRemoteTable()->name.')';
                        }else{
                            $table = '';
                        }
                        $html .= '<th>'.$relateName.$table.'</th>';
                    }
                }
            }else{
                $html .= '<th>'.$field.'</th>';
            }
        }
        $html .= '</thead>';
        return $html;
    }
    
    /**
     *  Builds a the <tr> portion of the toHTML method for $record
     *  if $outputRelated is true, then a column will be added for each relationship and the record rows
     *  will contain a table with each realted record for that relationship within those columns 
     *
     *  @return string The HTML markup of the <tr> row
     *  @param \Core\Library\Database\Record $record The record being output to HTML
     *  @param bool $outputRelated (optional) Boolean indicating if related records should be output. Defaults to false
     */
    private function toHTMLRow($record, $outputRelated=false){
        $html = '<tr>';
        $data = $record->toArray();
        foreach($data as $field=>$value){
            if(is_array($value)){
                if($outputRelated){
                    foreach($value as $relName=>$array){
                        $html .= '<td><table border="1">';
                        $records = $record->getRelatedRecords($relName);
                        $first = true;
                        foreach($records as $rel){
                            if($first){
                                $first = false;
                                $html .= $this->toHTMLHeadRow($rel,$outputRelated).'<tbody>';
                            }
                            $html .= $this->toHTMLRow($rel, $outputRelated);
                        }
                        $html .= '</tbody></table></td>';
                    }
                }
            }else{
                $html .= '<td>'.$value.'</td>';
            }
        }
        $html .= '</tr>';
        return $html;
    }
    
    /**
     * Parses the \Core\Library\Database\Record object, breaking out relationship data into formatted relationships on the record
     *
     * @todo Implement the parseRelationship method.
     *
     * @return \Core\Library\Database\Record
     * @param \Core\Library\Database\Record $record The record to be parsed.
     */
    public function parseRelationship($record){
        $newRecord = $this->registry->load->database('Record',array($this->table));
        $fields = $record->getSetFields();
        $related = array();
        $joinDelim = $this->table->getDB()->getJoinDelimiter();
        foreach($fields as $field){
            $parts = explode('.',$field);
            $type = array_shift($parts);
            $table = array_shift($parts);
            $relationshipName = array_shift($parts);
            $fieldName = array_shift($parts);
            if(!empty($parts)){
                $fieldName .= '.'.implode('.',$parts);
            }
            if($type == \Core\Library\Database\Query::FIELD_TYPE_JOIN){
                //Finish parsing out the field data
                if(!array_key_exists($relationshipName,$related)){
                    
                    $related[$relationshipName] = array();
                }
                
                $vals = explode(chr(hexdec("\\x".strtoupper(dechex($joinDelim)))),$record->getFieldValue($field));
                foreach($vals as $index=>$val){
                    if(!array_key_exists($index, $related[$relationshipName])){
                        $relatedTable = $this->table->getDB()->getTable($table);
                        $related[$relationshipName][$index] = $this->registry->load->database('Record',array($relatedTable));
                        $related[$relationshipName][$index]->setParentRecord($record);
                    }
                    $related[$relationshipName][$index]->{$fieldName} = $val;
                }
            }elseif($type == \Core\Library\Database\Query::FIELD_TYPE_DEFAULT){
                $newRecord->{$fieldName} = $record->getFieldValue($field);
            }else{
                throw new \Exception("Can't parse the field '".$field."' in a record with the type '".$type."'");
            }
        }
        if(!empty($related)){
            foreach($related as $index=>$recs){
                if(!empty($recs)){
                    $used=array();
                    foreach($recs as $rec){
                        $primary = $rec->getTable()->getPrimaryField()->name;
                        $id = $rec->{$primary};
                        if(!empty($id) && !in_array($id,$used)){
                            //Need to filter out duplicates
                            $newRecord->addRelatedRecord($index,$rec);
                            $used[] = $rec->{$primary};
                        }
                    }
                }
                
            }
        }
        return $newRecord;
    }
}
?>