<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Database;
use Core;
/**
 * Represents a single record in the database
 *
 * A \Core\Library\Database\Table object is required to construct this object, and represents the databaste table to which
 * this record belongs.
 *
 * If an $id is passed during construction, the record will be loaded by the constructor
 *
 * Once a record is loaded, fields can be accessed with -> nomanclature
 *
 * **Example:**
 *
 * ```php
 * $model = $registry->load->model('URL', array(), 'Router');
 * $record = $model->fetchRecord(1); //Fetch \Core\Library\Database\Record object with ID=1
 * print $record->name; //This will print the 'name' field of the record with ID=1
 * ```
 * @package \Core\Library\Database\Record
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Record extends Core\Library\BaseObject{
    /**
     * The \Core\Library\Database\Table object to which this record belongs
     * @var \Core\Library\Database\Table
     */
    public $table;
    /**
     * Storage array to hold the data for this record
     * @var array
     */
    private $data=array();
    /**
     * Boolean describing if a record has been loaded
     * @var bool
     */
    private $loaded=false;
    /**
     * An array containing all the \Core\Library\Database\Relationship objects to this record.
     * @var \Core\Library\Database\Relationship[]
     */
    public $relationships=array();
    
    /**
     * When this record is loaded as a related record, this is the parent record
     * @var \Core\Library\Database\Record
     */
    private $parentRecord=null;
    
    /**
     * Create a new instance. If $id is passed, the record will be loaded by the constructor
     *
     * @return \Core\Library\Database\Record
     * @param \Core\Library\Database\Table $table The table object to which this record belongs
     * @param mixed $id The record id to be loaded
     */
    public function __construct($table, $id=null){
        $this->table = $table;
        if($id != null){
            $this->load($id);
        }
    }
    
    /**
     * Loads the record identified by $id. Optionally any related records can be loaded
     *
     * @return null
     * @param mixed $id The ID for the record to be loaded
     * @param bool $loadRelationship Boolean indicating if related records should also be loaded.
     */
    public function load($id, $loadRelationship=false){
        if($this->id != $id){
            $record = $this->table->fetchRecord($id);
            $this->data=array();
            $this->relationships=array();
            foreach($this->table->fields as $field){
                $this->{$field->name} = $record->{$field->name};
            }
            if($loadRelationship){
                $this->loadRelationships();
            }
        }
        $this->loaded=true;
    }
    
    /**
     * Returns the value in the records primary key. i.e. get the records ID
     *
     * @return mixed The value in this records primary key field.
     */
    public function getPrimaryFieldValue(){
        return $this->getFieldValue($this->getTable()->getPrimaryField()->name);
    }
    
    /**
     * When this record is related to another record, this sets the parent Record in the relationship
     *
     * @return null
     * @param \Core\Library\Database\Record $record The parent record
     */
    public function setParentRecord($record){
        $this->parentRecord = $record;
    }
    
    /**
     * When this record is related to another record, this gets the parent Record in the loaded relationship
     * If the record is not loaded as part of a relationship then null is returned
     *
     * @return \Core\Library\Database\Record|null $record The parent record
     */
    public function getParentRecord(){
        return $this->parentRecord;
    }
    
    /**
     * Return the table object for this record.
     *
     * @return \Core\Library\Database\Table|null
     */
    public function getTable(){
        return $this->table;
    }
    
    /**
     * Return the config values for this record's table object
     *
     * @return array|null
     */
    public function getTableConfig(){
        if(!empty($this->table)){
            return $this->table->getConfig();
        }else{
            return null;
        }
    }
    
    /**
     * Load \Core\Library\Database\Relationship objects into $this->relationships. If no record is loaded, and exception will be thrown
     *
     * @return null
     */
    public function loadRelationships(){
        if(!empty($this->table->model)){
            if($this->loaded){
                if(!empty($this->table->model->tableConfig['relations'])){
                    foreach($this->table->model->tableConfig['relations'] as $relation){
                        $rel = $this->registry->load->database('Relationship', array($this));
                        $rel->load($relation['name']);
                        $this->relationships[] = $rel;
                    }
                }
            }else{
                throw new \Exception("No record loaded. Please load a record with \Core\Library\Database\Record::load(\$id) before loading relationships");
            }
            
        }else{
            throw new \Exception("\Core\Library\Database\Record object can not fetch relationships without a model on \Core\Library\Database\Table object. Use \Core\Library\Database\Table::setModel(\$model) to set the model before attempting to load relationships.");
        }
    }
    
    /**
     * Return all the related records for $relationshipName
     * If there are no related records, or relationships are not loaded, then an empty array is returned.
     *
     * @return \Core\Library\Database\Record[]
     * @param string $relationshipName The name of the relationship to get records for
     */
    public function getRelatedRecords($relationshipName){
        if(!empty($this->relationships) && array_key_exists($relationshipName, $this->relationships)){
            return $this->relationships[$relationshipName];
        }else{
            return array();
        }
    }
    
    /**
     * Rerturn this record as an associative array
     *
     * @return array
     */
    public function toArray(){
        $rtn = $this->data;
        if(!empty($this->relationships)){
            $rtn['relationships'] = array();
            foreach($this->relationships as $name=>$relates){
                $rtn['relationships'][$name] = array();
                foreach($relates as $related){
                    $rtn['relationships'][$name][] = $related->toArray();
                }
            }
        }
        return $rtn;
    }
    
    /**
     * Return the name of all the fields that are currently set in this record.
     *
     * @return array
     */
    public function getSetFields(){
        if(!empty($this->data)){
            $keys = array_keys($this->data);
            return $keys;
        }else{
            return array();
        }
    }
    
    /**
     * Access loaded record's field data
     * If field doesn't exist null will be returned
     *
     * @return mixed|null
     * @param string $name The name of the record property (field) to get
     */
    public function getFieldValue($name){
        if(array_key_exists($name,$this->data)){
            return $this->data[$name];
        }else{
            return null;
        }
    }
    
    /**
     * Add a record to this records relationship data.
     *
     * @return null
     * @param string $relationshipName The name of the relationship for the record to be added
     * @param \Core\Library\Database\Record $record The record object to add to the relationship data
     */
    public function addRelatedRecord($relationshipName, $record){
        if(!array_key_exists($relationshipName, $this->relationships)){
            $this->relationships[$relationshipName] = array();
        }
        $this->relationships[$relationshipName][] = $record;
    }
    
    /**
     * Access loaded record's field data with -> nomanclature
     * If field doesn't exist null will be returned
     *
     * @return mixed|null
     * @param string $name The name of the record property (field) to get
     */
    public function __get($name){
        return $this->getFieldValue($name);
    }
    
    /**
     * Set a loaded record's field data with -> nomanclature
     * If field doesn't exist an exception will be thrown
     *
     * @return null
     *
     * @param string $name The name of the field to set the $value
     * @param mixed $value The value to be set on the records $name field
     */
    public function __set($name, $value){
        $this->data[$name] = $value;
    }
    
    /**
     * Update the object after cloning
     *
     * @return null
     */
    public function __clone(){
        
    }
}
?>