<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Database;
use Core;
/**
 * Object representing a table in the database.
 *
 * This object contains the table name, an array of \Core\Library\Database\Field objects for each field in the table, and an array of
 * \Core\Library\Database\Index objects for each index on the table. Object will use the \Core\Library\Database\DB object to access the database
 * keeping the class database independant.
 *
 * @package \Core\Library\Database\Table
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Table extends Core\Library\BaseObject{
    /**
     * the DB() object to which this table belongs. Must be sent in the constructor
     * @var \Core\Library\Database\DB
     */
    private $db;
    /**
     * the table name. Must be sent in the constructor
     * @var string
     */
    public $name;
    
    /**
     * The config for this table for the module (if the model is set)
     * @var array
     */
    private $config;
	
    /**
     * (optional, but recommended) The module model to which this table belongs
     * @var \Core\Library\Model
     */
    public $model;
    /**
     * Array of field objects. One for each field in this table
     * @var \Core\Library\Database\Field
     */
	public $fields = array();
    /**
     * Array of index objects. One for each index on this table
     * @var \Core\Library\Database\Index
     */
	public $indexes = null;	//null value indicates db has not yet been queried....an array value (even empty array) indicates db has been queried
    
    /**
     * The last time the config file for this table was updated
     * @var int
     */
    private $configUpdatedAt;
    
    /**
     * The cache key used to check for the last update of this table
     * @var string
     */
    private $updateCacheKey;
    
    /**
     * Creates a new Instance. A {@see \Core\Library\Table} object should be passed whenever possible, as some methods can not operate without it.
     *
     * @return \Core\Library\Database\Table
     * @param \Core\Library\Database\DB $db The database object to be used to access this table
     * @param string $name The name of the table
     * @param \Core\Library\Model (optional) The module model to which this table belongs
     */
    public function __construct($db, $name, $model=null){
        $this->name = $name;
        $this->db = $db;
        $this->model = $model;
        $this->updateCacheKey = 'lastTableUpdate-'.$this->db->getHost().'-'.$this->db->getDatabaseName().'-'.$this->name;
    }
    
    /**
     * Initialize the table object.
     * Loads the table configuration for the module, if the model is loaded in this object
     * Updates the table
     *
     * @see \Core\Library\Database\Table::loadConfig()
     * @see \Core\Library\Database\Table::updateTable()
     *
     * @return null
     */
    public function init(){
        
        if(!empty($this->model)){
            $this->config = $this->getConfig();
            $this->updateTable();
        }
    }
    
    /**
     * If the model exists on this object, then the config for this table is loaded from the model's module
     *
     * @return array
     */
    public function getConfig(){
        if(empty($this->config)){
            if(!empty($this->model)){
                $config = $this->model->getConfig();
                if(!empty($config)){
                    $this->configUpdatedAt = $config->getLastUpdate();
                    $pos = array_search($this->name, array_column($config->tables,'name'));
                    if($pos === false){
                        $config = $this->load->loadClass('Core\Module\Base\Anonymous\Model\Model',array($this->name))->getConfig();
                        if(!empty($config)){
                            $pos = array_search($this->name, array_column($config->tables,'name'));
                        }
                        if($pos === false){
                            throw new \Exception("Configuration for the '".$this->model->getModule()."' module is missing the table '".$this->name."' being accessed by '".get_class($this)."'");
                        }
                    }
                    $keys = array_keys($config->tables);
                    $key = $keys[$pos];
                    $this->config = $config->tables[$key];
                }
            }
        }
        return $this->config;
    }
    
    /**
     * Set the {@see \Core\Library\Model} object after instantiation
     * This method will re-initialize the object.
     *
     * @see \Core\Library\Database\Table::init()
     *
     * @return null
     * @param \Core\Library\Model $model The model to which this table belongs.
     */
    public function setModel($model){
        if(empty($this->model) || $this->model != $model){
            $this->model = $model;
            $this->init();
        }
    }
    
    /**
     * Get the model to which this table belongs, or null if it is not set.
     * 
     * @return \Core\Library\Model|null
     */
    public function getModel(){
        return $this->model;
    }
    
    /**
     * Get the DB object to which this table belongs.
     * 
     * @return \Core\Library\Database\DB
     */
    public function getDB(){
        return $this->db;
    }
    
    /**
     * Returns an array of \Core\Library\Database\Relationship objects, built from the models config
     * Relationships from related tables will also be returned
     *
     * @return \Core\Library\Database\Relationship[]|array
     */
    public function getRelationships(){
        $config = $this->getConfig();
        if(!empty($config['relations'])){
            $rtn = array();
            foreach($config['relations'] as $relation){
                $new = $this->registry->load->database('Relationship', array($this, $relation['name']));
                //$newRels = $new->getRemoteTable()->getRelationships();
                //foreach($newRels as $nr){
                //    $rtn[] = $nr;
                //}
                $rtn[] = $new;
            }
            return $rtn;
        }else{
            return array();
        }
    }
    
    /**
     * Get the relationship object for the relationship with the name $relationshipName
     * Returns null if the relationship can't be found.
     *
     * @return \Core\Library\Database\Relationship|null
     * @param string $relationshipName The name of the relationship to retrieve
     */
    public function getRelationship($relationshipName){
        $rels = $this->getRelationships();
        foreach($rels as $rel){
            if($rel->name == $relationshipName){
                return $rel;
            }
        }
        return null;
    }
    
    /**
     * Get the primary field object for this table.
     *
     * @return \Core\Library\Database\Field
     */
    public function getPrimaryField(){
        return $this->db->getPrimaryIndexField($this->name);
    }
    
    /**
     * Determine if a field exists on this Table
     *
     * @return bool
     * @param string $fieldName The name of the field to check
     */
    public function fieldExists($fieldName){
        return $this->db->fieldExists($this->name,$fieldName);
    }
    
    /**
     * Get the \Core\Library\Database\Field object identified by $fieldName. Returns null if the field does not exist on this table
     *
     * @see \Core\Library\Database\Field
     *
     * @return \Core\Library\Database\Field|null
     * @param string $fieldName The name of the field to get
     */
    public function getField($fieldName){
        foreach($this->fields as $fieldIndex=>$field){
            if($field->name == $fieldName){
                return $this->fields[$fieldIndex];
            }
        }
        return $this->db->getField($this->name, $fieldName);
    }
    
    /**
     * Get an array of all the \Core\Library\Database\Field objects that belong to this table.
     *
     * @return \Core\Library\Database\Field[]
     */
    public function getFields(){
        return $this->fields;
    }
    
    /**
     * Function ensures that the database table matches the structure set in the module's config.php
     *
     * @return null
     */
    public function updateTable(){
        if(!$this->db->hasTableUpdated($this->name)){
            
            $cahceUpdated = $this->cache->getCreatedDate($this->updateCacheKey);
            if(empty($cahceUpdated) || $cahceUpdated < $this->configUpdatedAt){
                if(!empty($this->config)){
                    if(!empty($this->config['fields'])){
                        foreach($this->config['fields'] as $field){
                            $f = $this->registry->load->database('Field', array($this, $field['name']));
                            $f->null = $field['null'];
                            $f->datatype = $field['datatype'];
                            if($field['datatype'] != 'enum'){
                                $f->size = $field['size'];
                            }else{
                                $f->size = null;
                                $f->values = explode(',',$field['size']);
                            }
                            $f->auto_inc = $field['auto_inc'];
                            $f->unique = $field['unique'];
                            $f->primary = $field['primary'];
                            $this->fields[$field['name']] = $f;
                        }
                    }else{
                        throw new \Exception("The config file for the module '".(!empty($this->model) ? $this->model->getModule() : '')."' does not have any fields defined.");
                    }
                    if(!empty($this->config['indexes'])){
                        foreach($this->config['indexes'] as $index){
                            $i = $this->registry->load->database('Index');
                            $i->objTable = $this;
                            
                            $i->unique = $index['unique'];
                            $i->key_name = $index['name'];
                            foreach($index['columns'] as $column){
                                $i->columns[$column] = $column;
                            }
                            $i->index_type = $index['type'];
                            
                            
                            $i->collation = (array_key_exists('collation', $index) ? $index['collation'] : 'A');
                            $i->cardinality = (array_key_exists('cardinality', $index) ? $index['cardinality'] : '0');
                            $i->sub_part = (array_key_exists('sub_part', $index) ? $index['sub_part'] : null);
                            $i->packed = (array_key_exists('packed', $index) ? $index['packed'] : null);
                            $i->null = (array_key_exists('null', $index) ? $index['null'] : '');
                            $i->comment = (array_key_exists('comment', $index) ? $index['comment'] : '');
                            $i->index_comment = (array_key_exists('index_comment', $index) ? $index['index_comment'] : '');
                            $this->indexes[$i->key_name] = $i;
                        }
                    }
                    $this->db->updateTable($this, (!empty($this->model) ? $this->model : null));
                    $this->cache->set($this->updateCacheKey, time());
                }
            }else{
                $this->db->setTableUpdated($this->name);
            }
        }
    }
    
    /**
     * Fetches a record from this table by it's ID field (as determined by the database driver)
     * Additionally, if the $loadRelationship parameter is set to true then related records, as determined
     * by the relationships defined in the module's config.php will be included in the return value
     *
     * @see \Core\Library\Database\Record
     *
     * @return \Core\Library\Database\Record|null
     * @param int $id The ID of the record to fetch
     * @param bool $loadRelationship Boolean indicating if related records should be included in the return value.
     */
    public function fetchRecord($id, $loadRelationship=false){
        $q = $this->registry->load->database('Query', array($this));
        $q->type($q::QUERY_TYPE_SELECT)->fields('*')->where(array(
            array(
                'field'=>$this->getPrimaryField()->name,
                'comparison'=>'=',
                'value'=>$id
            )
        ));
        $records = $q->execute($loadRelationship);
        if($records->numRecords() == 1){
            return $records->current();
        }else{
            return null;
        }
    }
    
    /**
     * Function will return an iterable \Core\Library\Database\Recordset of all the rows in the table
     *
     * @see \Core\Library\Database\Recordset
     *
     * @return \Core\Library\Database\Recordset
     * @param bool $loadRelationships Boolean indicating if records should contain relationship data. defaults to false
     */
    public function fetchAll($loadRelationships=false){
        $q = $this->registry->load->database('Query', array($this));
        return $q->type($q::QUERY_TYPE_SELECT)->fields('*')->execute($loadRelationships);
    }
    
    /**
     * Fetch an arbitrary set of records from this table according to the \Core\Library\Database\Query object
     *
     * @return \Core\Library\Database\Recordset
     * @param \Core\Library\Database\Query $query The query object that should be executed
     * @param bool $loadRelationships Boolean indicating if relationship data should be fetched for the records. defaults to false.
     */
    public function fetchSet($query, $loadRelationships=false){
        return $query->execute($loadRelationships);
    }
    
    /**
     * Insert one or multiple {@see \Core\Library\Database\Record} records into this table
     *
     * @return null
     * @param \Core\Library\Database\Record[] An array of records to be inserted
     * @param bool $insertRelatedRecords (optional) Boolean indicating if the related records should be inserted as well. Defaults to false
     */
    public function insertRecords($records, $insertRelatedRecords=false){
        $q = $this->registry->load->database('Query', array($this));
        return $q->type($q::QUERY_TYPE_INSERT)->records($records)->execute($insertRelatedRecords);
    }
    
    /**
     * Update one or multiple {@see \Core\Library\Database\Record} records into this table
     *
     * @return null
     * @param \Core\Library\Database\Record[] An array of records to be updated
     * @param bool $updateRelatedRecords (optional) Boolean indicating if the related records should be updated as well. Defaults to false
     */
    public function updateRecords($records, $updateRelatedRecords=false){
        $q = $this->registry->load->database('Query', array($this));
        return $q->type($q::QUERY_TYPE_UPDATE)->records($records)->execute($updateRelatedRecords);
    }
    
    /**
     * Delete one or multiple {@see \Core\Library\Database\Record} records from this table
     *
     * @return null
     * @param \Core\Library\Database\Record[] An array of records to be removed
     * @param bool $deleteRelatedRecords (optional) Boolean indicating if the related records should also be removed. Defaults to false
     */
    public function deleteRecords($records, $deleteRelatedRecords=false){
        $q = $this->registry->load->database('Query', array($this));
        return $q->type($q::QUERY_TYPE_DELETE)->records($records)->execute($deleteRelatedRecords);
    }
}
?>