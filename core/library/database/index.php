<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Database;
use Core;
/**
 * An object representing an Index on a Table.
 *
 * @package \Core\Library\Database\Index
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Index extends Core\Library\BaseObject{
    /**
     * The table object that this Index belongs to
     * @var \Core\Library\Database\Table
     */
    public $objTable;
    /**
     * The table name from the index
     * @var string
     */
	public $table;
    /**
     * Defines if this is a unique index
     * @var bool
     */
	public $unique;
    /**
     * The name of the index.
     * @var string
     */
	public $key_name;
    /**
     * The sequence number of this index within $key_name
     * @var int
     */
	public $seq_in_index;
    /**
     * The fields included in this index
     * @var array
     */
	public $columns=array();
    /**
     * The collation of the index
     * @var string
     */
	public $collation;
    /**
     * The cardinality of the index
     * @var string
     */
	public $cardinality;
    /**
     * The sub_part of the index
     * @var string
     */
	public $sub_part;
    /**
     * The packed property of the Index
     * @var string
     */
	public $packed;
    /**
     * Determines if null is allowed
     * @var bool
     */
	public $null;
    /**
     * The index type.
     * @var string
     */
	public $index_type;
    /**
     * A comment for the index
     * @var string
     */
	public $comment;
    /**
     * The global Registry object
     * @var \Core\Library\Registry
     */
	public $index_comment;
	
    /**
     * Drop this index from the database
     *
     * @return null
     */
	public function removeIndex(){
		return $this->tableObj->db->removeIndex($this->table->name, $this);
	}
}
?>