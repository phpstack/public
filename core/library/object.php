<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library;
/**
 * This is the base object which classes should ultimately exitend.
 * This class is intended to maintain a reference to the
 * {@see /Core/Registry} object, as well as give access to it's properties via the __get and __set magic methods. This class
 * implements the setRegistry method. Even though it's possible to overload the setRegistry method, it probably shouldn't be done.
 * Plus, there is no practical case to do so.
 *
 * @package \Core\Library\BaseObject
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 * 
 */
class BaseObject{
    /**
     * The global registry object
     * @var \Core\Library\Registry
     */
    protected $registry;
    
    /**
     * Magic Method used to allow -> nomanclature for access to registry values and objects
     *
     * @return mixed
     * @param string $name The name of the property to get
     */
    public function __get($name){
        if(!empty($this->registry)){
            return $this->registry->get($name);
        }else{
            return null;
        }
    }
    
    /**
     * Magic Method used to allow -> nomanclature for access to registry values and objects
     *
     * @return mixed
     * @param string $name The name of the property to get
     */
    public function __set($name, $value){
        if(!empty($this->registry)){
            $this->registry->set($name, $value);
        }
    }
    
    /**
     * Set a reference to the global registry object. If this method is overloaded, then the overloading method should call
     * parent::setRegistry($registry);
     *
     * Several classes wait on this method to be called before they will initialize. The loader will automatically call this method
     * if the class is loaded using it. If not, you should call this method immediately after instantiation of a new object.
     *
     * @see \Core\Module\Base\Anonymous\Model\Model::setRegistry()
     * @see \Core\Library\Database\DB::setRegistry()
     * @see \Core\Library\Http\Request::setRegistry()
     * @see \Core\Library\Database\Query::setRegistry()
     *
     * @return null
     * @param \Core\Library\Registry $registry The global registry object
     */
    public function setRegistry($registry){
        $this->registry = $registry;
    }
    
    /**
     * Alias of the {@see \Core\Library\Utility::print_array()} method
     *
     * @return null
     * @param mixed $object The object to be printed
     * @param bool $removeRegistry Boolean indicates if the registry object should be printed when encountered
     */
    public function print_array($object, $removeRegistry=true){
        
        if(!empty($this->registry->utility)){
            $this->registry->utility->print_array($object, $removeRegistry);
        }elseif(!empty($this->registry->load)){
            $this->registry->load->library('Utility')->print_array($object,$removeRegistry);
        }else{
            //No utility and no loader
            if(!empty($this->registry->phpStack)){
                if($this->registry->phpStack->isRecordingTrace()){
                    if(!class_exists('Core\Library\UtilityTrace')){
                        print '<pre>'.print_r($object,true).'</pre>';
                    }else{
                        $u = new UtilityTrace();
                        $u->print_array($object, $removeRegistry);
                    }
                }else{
                    if(!class_exists('Core\Library\Utility')){
                        require_once(CORE_DIR_LIBRARY.'utility.php');
                    }
                    $u = new Utility();
                    $u->print_array($object, $removeRegistry);
                }
            }else{
                //No utility, no loader, and no phpStack loaded in the registry...
                print '<pre>'.print_r($object,true).'</pre>';
            }
            
            
        }
    }
    
    /**
     * Alias of the {@see \Core\Library\Utility::trace()} method
     *
     * @return array The backtrace data
     
     */
    public function trace(){
        
        if(!empty($this->registry->utility)){
            return $this->registry->utility->trace();
        }else{
            if(!class_exists('Core\Library\Utility')){
                require_once(CORE_DIR_LIBRARY.'utility.php');
            }
            $u = new Utility();
            return $u->trace();
            
        }
    }
}
?>