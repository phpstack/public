<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library;
/**
 * Manages users in the framework
 * 
 * Class uses, and requires the {@see \Core\Module\Admin\User\Model\User} 
 * @package \Core\Library\User
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class User extends BaseObject{
    /**
     * The {@see \Core\Module\Admin\User\Anonymous\Model\User} object used to access the user data
     */
    private $userModel=null;
    
    /**
     * The id of the current user
     * @var int
     */
    private $id=null;
    
    /**
     * Initializes the user by loading their information
     *
     * If the user id is not in the session, then the user data can not be loaded until they log in.
     *
     * @todo finish setting up the user with data from the database.
     *
     * @return null
     */
    public function init(){
        //$this->userModel = $this->load->model('User', array(), 'Admin\User', false);
        if(!empty($this->request->session) && isset($this->request->session->user_id)) {
            $this->id = $this->request->session->user_id;
        }
    }
    
    /**
     * Determine if the user is logged into the system
     *
     * @return bool true if the user is logged in, otherwise false
     */
    public function isLoggedIn(){
        return !empty($this->id);
    }
    
    /**
     * Determine if the user has permission to the $page->$method()
     *
     * @todo Implement permissions and permision levels
     * 
     * @return bool true if the user has permission, false otherwise
     * @param mixed $page The page to check permission for
     * @param string $method The method to check permission for
     * @param string $permissionLevel (optional) The minimum permission level to check for. Defaults to read access
     */
    public function hasPermission($page, $method, $permissionLevel=null){
        return true;
    }
    
    /**
     * Determine if the user needs to do a password reset
     *
     * @return bool True if the user needs to reset the password, false otherwise
     */
    public function needsPasswordReset(){
        return false;
    }
    
    /**
     * Log in the user
     *
     * @todo Implement the login method
     *
     * @return bool Indicates if the login was successful
     * @param string $username the username of the user to login
     * @param string $password The password of the user to login
     */
    public function login($username, $password){
        if($username == 'jwagner'){
            $this->request->session->user_id=1;
            $this->id = 1;
            return true;
        }
        return false;
    }
    
    /**
     * Log out the user
     *
     * @return null
     */
    public function logout(){
        unset($this->request->session->user_id);
        $this->id = null;
    }
}
?>