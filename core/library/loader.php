<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library;
/**
 * A factory class used to load other classes within the framework.
 *
 * The {@see \Core\Library\Loader} class is used as a factory to create instances of other classes within the framework using the loadClass method.
 * It is not necessary to follow any naming convention within classes in order to load them. This class is created
 * and placed into the @see \Core\Library\Registry in the startup.php file, giving global access to it.
 *
 * This utilizes a combination of File parsing, and the PHP Reflection class to determine how to load the requested classes.
 * The {@see \Core\Library\Cache class should be created and placed in the Registry before this class is created, as is used in this class
 *
 * @see \Core\Library\Cache
 *
 * @todo Add load methods for 'database', 'http', 'library', 'module'
 * @todo Change all classes to use new loader methods
 * @todo Update loader to load overloads before core classes (the "top most" class). Application namespace should be used, if it exists
 * 
 * @package \Core\Library\Loader
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Loader extends BaseObject{
    
    /**
     * A constant to define the "Core" namespace
     * @var string
     */
    const NAMESPACE_APPLICATION_CORE='Core';
    
    /**
     * A constant to define the "Application" namespace
     * @var string
     */
    const NAMESPACE_APPLICATION_APPLICATION='Application';
    
    /**
     * A constant to define the "Library" namespace
     * @var string
     */
    const NAMESPACE_LOCATION_LIBRARY='Library';
    
    /**
     * A constant to define the "Module" namespace
     * @var string
     */
    const NAMESPACE_LOCATION_MODULE='Module';
    
    /**
     * A constant to define the "Authenticated" namespace
     * @var string
     */
    const NAMESPACE_PERMISSION_AUTHENTICATED='Authenticated';
    
    /**
     * A constant to define the "Anonymous" namespace
     * @var string
     */
    const NAMESPACE_PERMISSION_ANONYMOUS='Anonymous';
    
     /**
     * A constant to define the "Page" namespace
     * @var string
     */
    const NAMESPACE_TYPE_PAGE='Page';
    
    /**
     * A constant to define the "Page" namespace
     * @var string
     */
    const NAMESPACE_TYPE_BLOCK='Block';
    
    /**
     * A constant to define the "Model" namespace
     * @var string
     */
    const NAMESPACE_TYPE_MODEL='Model';
    
    
    /**
     * A constant to define the "Database" namespace
     * @var string
     */
    const NAMESPACE_LIBRARY_DATABASE='Database';
    
    /**
     * A constant to define the "Http" namespace
     * @var string
     */
    const NAMESPACE_LIBRARY_HTTP='Http';

    /**
     * A constant to define the "Base" module namespace
     * @var string
     */
    const NAMESPACE_LIBRARY_BASE='Base';
    
    /**
     * The global Registry object
     * @var \Core\Library\Registry
     */
    public $registry;
    /**
     * An array containing information on each class in the project
     * @var array
     */
    private $classes = array();

    /**
     * Construct a new instance of the Loader class. Calls loadClassList
     *
     * @see \Core\Library\Loader::loadClassList()
     *
     * @return \Core\Library\Loader
     * @param \Core\Library\Registry
     */
    public function __construct($registry){
        $this->registry = $registry;
        $this->loadClassList();
    }
    
    /**
     * Get all the class information
     *
     * @return array All the class information for all the classes
     */
    public function getClasses(){
        return $this->classes;
    }
    
    /**
     * Get all the conrollers for the given module
     *
     * @return array The data for all the pages in $module
     * @param string $module The name of the module to get pages for
     */
    public function getModulePageData($module){
        $classModules = array_column($this->classes, 'module');
        $classKeys = array_keys($this->classes);
        $found = array_keys($classModules, $module);
        $rtn = array();
        foreach($found as $k){
            $classInfo = $this->classes[$classKeys[$k]];
            if(strpos($classInfo['namespace'],'Core\Module\\') !== false || strpos($classInfo['namespace'],'Application\Module\\') !== false){
                $rtn[] = $classInfo;
            }
        }
        
        return $rtn;
    }
    
    /**
     * Check if a class exists. 
     *
     * @return bool Boolean indicating if the class exists
     * @param string $className The name of the class (fully namespaced)
     * 
     */
    public function classExists($className){
        if(array_key_exists($className, $this->classes)){
            return true;
        }
        return false;
    }
    
    /**
     * Returns information about all the classes that are defined in $file. Utilized {@see \Core\Library\Cache} class
     * to speed future instantiations of this class. Cache is updated if it is older than the modified date of $file
     *
     * @return array An array containing information about the classes defined in $file
     * @param string $file The path to a file to get class information about
     */
    private function getClassesInFile($file){
        $cacheKey='getClassesInFile-'.md5($file);
        //If the cache doesn't exists, or the file has been modified since it was created
        if(true || empty($this->registry->cache) || !$this->registry->cache->exists($cacheKey) || $this->registry->cache->getCreatedDate($cacheKey) <= filemtime($file)){
            $tokens = token_get_all(file_get_contents($file));
            $count = count($tokens);
            $spl = new \SplFileObject($file);
            $classes = array();
            $namespaces = array();
            for ($i = 2; $i < $count; $i++) {
                if(($tokens[$i - 2][0] == T_CLASS && $tokens[$i - 1][0] == T_WHITESPACE && $tokens[$i][0] == T_STRING) ||
                   ($tokens[$i - 2][0] == T_INTERFACE && $tokens[$i - 1][0] == T_WHITESPACE && $tokens[$i][0] == T_STRING) ||
                   ($tokens[$i - 2][0] == T_NAMESPACE && $tokens[$i - 1][0] == T_WHITESPACE && $tokens[$i][0] == T_STRING)
                ){
                    
                    if(($tokens[$i - 2][0] == T_NAMESPACE && $tokens[$i - 1][0] == T_WHITESPACE && $tokens[$i][0] == T_STRING)){
                        $c=0;
                        $namespace = '';
                        while(is_array($tokens[($i+$c)]) || (!is_array($tokens[($i+$c)]) && $tokens[($i+$c)] != ';')){
                            $namespace .= $tokens[($i+$c)][1];
                            $c++;
                        }
                        $namespaces[$i] = $namespace;
                    }else{
                        
                        $class = $tokens[$i][1];
                        $spl->seek($tokens[$i][2]-1);
                        $classLine = $spl->current();
                        if(strpos($classLine, 'extends') !== false){
                            $extends = trim(str_replace($class.' extends ','',trim(str_replace(array('class','{'),'',$classLine))));
                        }else{
                            $extends = null;
                        }
        
                        if(strpos($extends, 'implements') !== false){
                            $parts = explode('implements',$extends);
                            $extends = trim($parts[0]);
                            $implements = trim($parts[1]);
                        }else{
                            $implements = null;
                        }
                        if(strpos($implements,',') !== false){
                            $implements = explode(',',$implements);
                            foreach($implements as $index=>$ii){
                                $implements[$index] = trim($ii);
                            }
                        }
                        $isInterface = false;
                        if($tokens[$i - 2][0] == T_INTERFACE && $tokens[$i - 1][0] == T_WHITESPACE && $tokens[$i][0] == T_STRING){
                            $isInterface=true;
                        }
                        $classes[$i] = array(
                            'class'=>$class,
                            'extends'=>$extends,
                            'implements'=>$implements,
                            'is_interface'=>$isInterface
                        );
                    }
                }
            }
            
            if(count($classes) == 1 && count($namespaces) == 1){
                $keys = array_keys($classes);
                $key = array_shift($keys);
                $classes[$key]['namespace'] = array_shift($namespaces);
            }elseif(count($classes) == 1 && empty($namespaces)){
                $keys = array_keys($classes);
                $key = array_shift($keys);
                $classes[$key]['namespace'] = "\\";
            }else{
                if(count($classes) > 0){
                    //We have more than one class and/or more than one interface
                    throw new \Exception("More than one class in a file is not currently supported. file: '".$file."'");
                }
            }
            //Update the class names with their namespaces
            $return = array();
            while($class = array_shift($classes)){
                if(strpos($class['class'],'\\') === false){
                    $namespaceClass = rtrim((!empty($class['namespace']) ? $class['namespace'] : ''),'\\').'\\'.$class['class'];
                    $class['namespace_class'] = $namespaceClass;
                }
                if(is_array($class['implements'])){
                    foreach($class['implements'] as $index=>$ii){
                        if(strpos($class['implements'][$index],'\\') === false){
                            if(!empty($class['implements'][$index])){
                                $class['implements'][$index] = rtrim((!empty($class['namespace']) ? $class['namespace'] : ''),'\\').'\\'.$class['implements'][$index];
                            }
                        }
                    }
                }else{
                    if(strpos($class['implements'],'\\') === false){
                        if(!empty($class['implements'])){
                            $class['implements'] = rtrim((!empty($class['namespace']) ? $class['namespace'] : ''),'\\').'\\'.$class['implements'];
                        }
                    }
                }
                if(strpos($class['extends'],'\\') === false){
                    if(!empty($class['extends'])){
                        $class['extends'] = rtrim((!empty($class['namespace']) ? $class['namespace'] : ''),'\\').'\\'.$class['extends'];
                    }
                }
                $return[] = $class;
                
            }
            if(!empty($this->registry->cache)){
                $this->registry->cache->delete($cacheKey);
                $this->registry->cache->set($cacheKey, $return);
            }
            return $return;
        }else{
            return $this->registry->cache->get($cacheKey);
        }
    }

    /**
     * Loads information for all classes in the project.
     *
     * Method recursively loops through all the files in the projects root directory and list all the declared classes
     * along with information about each class, which is used to create new instances of classes
     *
     * @return null
     */
    public function loadClassList(){
        $it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator(DIR_ROOT), \RecursiveIteratorIterator::SELF_FIRST);
        foreach($it as $name=>$file){
            if($name != '.' || $name != '..'){
                if(strtolower($file->getExtension()) === 'php'){
                    $filePath = $file->getPathname();
                    $classes = $this->getClassesInFile($filePath);
                    foreach($classes as $class){
                        if(empty($this->classes[$class['namespace_class']])){
                            $this->classes[$class['namespace_class']] = array(
                                'class'=>$class['class'],
                                'namespace_class'=>$class['namespace_class'],
                                'file'=>$filePath,
                                'is_interface'=>$class['is_interface'],
                                'loaded'=>(class_exists($class['class']) || class_exists($class['namespace_class'])),
                                'namespace'=>(!empty($class['namespace']) ? $class['namespace'] : '\\'),
                                'overloads'=>array(),
                                'overloaded_by'=>array()
                            );
                        }else{
                            //Class is loaded, perhaps because it was extended by a class processed earlier.
                            //Update the information without clobbering it first
                            
                            if(empty($this->classes[$class['namespace_class']]['class']) || $this->classes[$class['namespace_class']]['class'] != $class['class']){
                                $this->classes[$class['namespace_class']]['class'] = $class['class'];
                            }
                            
                            if(empty($this->classes[$class['namespace_class']]['namespace_class'])){
                                $this->classes[$class['namespace_class']]['namespace_class'] = $class['namespace_class'];
                            }
                            
                            if(empty($this->classes[$class['namespace_class']]['file'])){
                                $this->classes[$class['namespace_class']]['file'] = $filePath;
                            }
                            
                            if(empty($this->classes[$class['namespace_class']]['namespace'])){
                                $this->classes[$class['namespace_class']]['namespace'] = (!empty($class['namespace']) ? $class['namespace'] : '\\');
                            }
                            
                            if(empty($this->classes[$class['namespace_class']]['is_interface'])){
                                $this->classes[$class['namespace_class']]['is_interface'] = (!empty($class['is_interface']) ? $class['is_interface'] : false);
                            }
                            
                        }
                        if(!empty($class['extends'])){
                            $this->classes[$class['namespace_class']]['overloads'][] = $class['extends'];
                            
                            if(empty($this->classes[$class['extends']])){
                                $this->classes[$class['extends']] = array(
                                    'class'=>$class['extends'],
                                    'namespace_class'=>'',
                                    'file'=>'',
                                    'is_interface'=>false,
                                    'loaded'=>class_exists($class['extends']),
                                    'namespace'=>'',
                                    'overloads'=>array(),
                                    'overloaded_by'=>array()
                                );
                            }
                        }
                        if(!empty($class['implements'])){
                            if(!is_array($class['implements'])){
                                $class['implements'] = array($class['implements']);
                            }
                            foreach($class['implements'] as $ii){
                                $this->classes[$class['namespace_class']]['overloads'][] = $ii;
                                if(empty($this->classes[$ii])){
                                    $this->classes[$ii] = array(
                                        'class'=>$ii,
                                        'namespace_class'=>'',
                                        'file'=>'',
                                        'is_interface'=>false,
                                        'loaded'=>class_exists($ii),
                                        'namespace'=>'',
                                        'overloads'=>array(),
                                        'overloaded_by'=>array()
                                    );
                                }
                            }
                        }
                        
                        //Determine which "type" of class this is (database, http, library, module)
                        
                        $parts = explode('\\',$class['namespace']);
                        //Core or Application
                        $this->classes[$class['namespace_class']]['namespaces']['application'] = array_shift($parts);
                        //Module or Library
                        $this->classes[$class['namespace_class']]['namespaces']['location'] = array_shift($parts);
                        
                        if($this->classes[$class['namespace_class']]['namespaces']['location'] == self::NAMESPACE_LOCATION_LIBRARY){
                            if(count($parts) > 0){
                                $this->classes[$class['namespace_class']]['namespaces']['location'] .= '\\'.implode('\\',$parts);
                            }
                            $this->classes[$class['namespace_class']]['namespaces']['type'] = '';
                            $this->classes[$class['namespace_class']]['namespaces']['permission'] = '';
                            $this->classes[$class['namespace_class']]['namespaces']['module'] = '';
                        }else{
                            //Model or Block or Page
                            $this->classes[$class['namespace_class']]['namespaces']['type'] = array_pop($parts);
                            //Frontend or Admin
                            $this->classes[$class['namespace_class']]['namespaces']['permission'] = array_pop($parts);
                            
                            //The namespace for the module
                            $this->classes[$class['namespace_class']]['namespaces']['module'] = implode('\\',$parts);
                        }
                    }
                }
            }
        }
        //Now all the classes are loaded, lets go through them one more time to get some insight
        foreach($this->classes as $index=>$classInfo){
            $this->classes[$index]['overloaded_by'] = array();
            if(!empty($classInfo['overloads'])){
                foreach($classInfo['overloads'] as $o){
                    if(array_key_exists($o,$this->classes)){
                        $this->classes[$o]['overloaded_by'][] = $classInfo['namespace_class'];
                    }
                }
            }
        }
    }
    
    /**
     * Creates a PHP Reflection class for the given $className, and extracts information about $className
     *
     * Method will use the PHP reflection class to determine the number of parameters needed to instantiate $className
     * and will determine which parameter (if any) are the {@see \Core\Library\Registry} object. The PHP Reflection class is included
     * in the return value.
     *
     * @return array An array containing the reflection information collected, including the reflection class
     * @param string $className The name of the class for which to get Reflection information
     */
    public function getClassReflect($className){
        if(!array_key_exists('reflect',$this->classes[$className])){
            $this->classes[$className]['reflect'] = array();
            $reflect = new \ReflectionClass($className);
            $constructor = $reflect->getConstructor();
            if(!empty($constructor)){
                $params = $constructor->getParameters();
            }else{
                $params = array();
            }
            $totalParams = 0;
            $minParams = 0;
            $regIndex=null;
            foreach($params as $index=>$param){
                if($param->getName() == 'registry'){
                    $regIndex = $index;
                }
                $totalParams++;
                if(!$param->allowsNull()){
                    $minParams++;
                }
            }

            $this->classes[$className]['reflect']['construct'] = array(
                'parameter_count'=>$totalParams,
                'min_parameter_count'=>$minParams,
                'registry_index'=>$regIndex,
            );
            $this->classes[$className]['reflect']['reflect_class'] = $reflect;
        }
        return $this->classes[$className]['reflect'];
    }
    
    /**
     * Gets all the available class information for the given $name
     *
     * @return array An array containing all the information gathered about the given class
     * @param string $name The name of the class to get information about
     * 
     */
    public function getClassInfo($name){
        $classInfo=array();
        if(substr($name,-5) == 'Trace'){
            $name = substr($name,0, -5);
        }
        
        $pos = array_search($name,array_column($this->classes,'namespace_class'));
        if($pos === false){
            $pos = array_search($name,array_column($this->classes,'class'));
        }
        if($pos !== false){
            //This is a valid class
            $keys = array_keys($this->classes);
            $key = $keys[$pos];
            $classInfo = $this->classes[$key];
        }
        return $classInfo;
    }
    
    /**
     * Method will include_once any file required in order to create an instance of $name
     * including extends classes.
     *
     * @return null 
     * @param string $name The name of the class to include files for
     */
    public function includeRequired($name){
        $classInfo = $this->getClassInfo($name);
        if(empty($classInfo)){
            throw new \Exception("Could not load undefined class '".$name."'");
        }
        //If there are dependancies, then load them
        if(!empty($classInfo['overloads'])){
            foreach($classInfo['overloads'] as $req){
                if(!class_exists($req)){
                    $this->includeRequired($req);
                }
            }
        }
        //If the class begins with a slash (checking to see if it's in the global namespace)
        if(substr($classInfo['class'], 0,1) == '\\' && interface_exists($classInfo['class'])){
            return;
        }
        if(($this->phpStack->isRecordingTrace() && !class_exists($classInfo['namespace_class'].'Trace')) || (!$this->phpStack->isRecordingTrace() && !class_exists($classInfo['class']) && !class_exists($classInfo['namespace_class']) && !interface_exists($classInfo['class']))){
            //still need to include the code file
            if(file_exists($classInfo['file'])){
                if($this->phpStack->isRecordingTrace() && !$classInfo['is_interface']){
                    $cacheKey = $classInfo['namespace_class'].'Trace';
                    
                    if(!$this->cache->exists($cacheKey) || ($this->cache->exists($cacheKey) && $this->cache->getCreatedDate($cacheKey) <= filemtime($classInfo['file']))){
                        $classContent = "<"."?php ".$this->phpStack->getTraceClassCode($classInfo['class'], $classInfo['namespace'], $classInfo['overloads'], $classInfo['file'])." ?".">";
                        
                        
                        
                        $this->cache->set($cacheKey,$classContent);
                    }
                    $file = $this->cache->getCacheFile($cacheKey);
                    require_once($file);
                }else{
                    
                    require_once($classInfo['file']);
                }
            }else{
                $this->print_array($classInfo);
                throw new \Exception("Invalid code file for class '".$classInfo['namespace_class']."': '".$classInfo['file']."'");
            }
        }
        
    }

    /**
     * Include required code files, and return an object of type $name
     *
     * For classes that extend the BaseObject, the registry will be set using the setRegistry() method. Once the registry has been set, if the
     * class has an init() method, it will be called automatically.
     * 
     * For classes that do NOT extend the BaseObject A class may have no parameters, but if it has any parameters, one of them should be a
     * registry object The registry object will automatically be included in the parameters, if it is required. Therefore, a Registry object
     * should not be included in the $args parameter.
     *
     * @return mixed A new instance of the class requested with $name
     * @param string $name The name of the class to be created and returned.
     * @param array $args An array of arguments needed to instantiate the class, in the order they are required.
     */
    public function loadClass($name, $args=array()){
        
        if(!is_array($args)){
            throw new \Exception("The \$args parameter of \Core\Library\Loader::loadClass() must be an array");
        }
        $this->includeRequired($name);
        $classInfo = $this->getClassInfo($name);
        //If the class still doesn't exist, then it wasn't actually in the code file
        if(class_exists($classInfo['namespace_class']) || class_exists($classInfo['class'])){
            
            if(!$classInfo['is_interface'] && $this->phpStack->isRecordingTrace()){
                $newClass = $classInfo['namespace_class'].'Trace';
            }else{
                $newClass = $classInfo['namespace_class'];
                
            }
            $reflect = $this->getClassReflect($classInfo['namespace_class']);
            if(count($args) >= $reflect['construct']['min_parameter_count']){
                //We have the right number of parameters
                
                if($reflect['construct']['parameter_count'] == 0){
                    //Class doesn't have any parameters
                    $obj = new $newClass();
                }elseif($reflect['construct']['parameter_count'] == 1 && $reflect['construct']['registry_index'] !== null){
                    //Class only takes one parameter, which is the registry
                    $obj = new $newClass($this->registry);
                }elseif($reflect['construct']['registry_index'] === null){
                    //Class doesn not take a registry object
                    $obj = $reflect['reflect_class']->newInstanceArgs($args);
                }else{
                    //Class takes parameters, and one of them is a registry
                    //Splice in the registry, and call the constructor
                    $newArgs = $args;

                    array_splice($newArgs, $reflect['construct']['registry_index'], 0, array($this->registry));
                    $obj = $reflect['reflect_class']->newInstanceArgs($newArgs);
                }
                if(method_exists($obj,'setRegistry') && is_callable(array($obj,'setRegistry'))){
                    call_user_func_array(array($obj,'setRegistry'),array($this->registry));
                }
                if(is_callable(array($obj,'parent::init'))){
                    call_user_func_array(array($obj,'parent::init'),array());
                }
                if(method_exists($obj,'init') && is_callable(array($obj,'init'))){
                    call_user_func_array(array($obj,'init'),array());
                }
                return $obj;

            }else{
                throw new \Exception("Incorrect number of constructor parameters for class '".$name."'. Expected ".$reflect['construct']['min_parameter_count']." and received ".count($args)."");
            }
        }else{
            //$this->print_array($classInfo);
           throw new \Exception("Class '".$classInfo['namespace_class']."' is not declared in the file '".$classInfo['file']."'");
        }
    }
    
    /**
     * Takes a class name, without the namespace, and returns class info for all implementations of that clas
     *
     * @return array An array of classes with their info, or null if it isn't found
     * @param string the name of the class to find
     * @param string $location Only consider classes with this "location" namespace. Locations are defined constants of this class and are 'Library' or 'Module'
     */
    public function findClassInfo($className, $location=null){
        if($location !== null && $location != self::NAMESPACE_LOCATION_LIBRARY && $location != self::NAMESPACE_LOCATION_MODULE){
            throw new \Exception("The class type '".$location."' is not valid. Valid values are '".NAMESPACE_LOCATION_LIBRARY."', and '".NAMESPACE_LOCATION_MODULE."'.");
        }
        $positions = array_keys(array_column($this->classes,'class'), $className);
        if(!empty($positions)){
            $rtn = array();
            $keys = array_keys($this->classes);
            foreach($positions as $key){
                if($location==null || ($location !== null && $this->classes[$keys[$key]]['namespaces']['location'] == $location)){
                    $rtn[$keys[$key]] = $this->classes[$keys[$key]];
                }
            }
            return $rtn;
        }
        return array();
    }
    
    /**
     * Loads a library class.
     *
     * If $allowOverload is true this method will load the class in the 'Application' namespace (if it exists) allowing for the class
     * returned to be the overloaded class, and not the base class.
     * 
     * Note: If a library has a namespace deeper than Core\Library or Application\Library, then the namespace below that level
     * must be included.
     *
     * <b>Example:</b>
     *
     * ```php
     *
     * //the Registry is in the namespace Core\Library, so we only need the class name.
     * $registry = $this->load->library('Registry');
     * 
     * //The Request object is in the Core\Library\Http namespace and since the Http namespace is below the Core\Libray namespace we need to include it as part of the classname
     * $request = $this->load->library('Http\Request');
     * 
     * ```
     *
     * @param string $className The name of the class to be loaded, without the full namespace
     * @param array @params The parameters to use to construct the class
     * @param bool $forceCore (optional) Boolean to determine if overloaded classes should be returned, or just the core class. Defaults to true
     */
    public function library($className, $params=array(), $forceCore=false){
        $appClass = self::NAMESPACE_APPLICATION_APPLICATION.'\\'.self::NAMESPACE_LOCATION_LIBRARY.'\\'.$className;
        if(!$forceCore && array_key_exists($appClass, $this->classes)){
            return $this->loadClass($this->classes[$appClass]['namespace_class'], $params);
        }else{
            $coreClass = self::NAMESPACE_APPLICATION_CORE.'\\'.self::NAMESPACE_LOCATION_LIBRARY.'\\'.$className;
            if(array_key_exists($coreClass,$this->classes)){
                return $this->loadClass($this->classes[$coreClass]['namespace_class'], $params);
            }else{
                throw new \Exception("Can not load the library '".$coreClass."'. No such library exists");
            }
        }
    }
    
    /**
     * Loads a class from a module
     *
     * Namespaces should be in the form <applicationNamespace>/self::NAMESPACE_LOCATION_MODULE/<moduleNamespace>/<permissionLevel>/<classTypeNamespace>/<className>
     *
     * $className               : The name of the class to return
     *
     * $params                  : An array of parameters to be passed to the new class' constructor
     *
     * $applicationNamespace    : If the $applicationNamespace is not provided, this method will return the class from the
     *                            self::NAMESPACE_APPLICATION_APPLICATION namespace if it exists. If it doesn't exist, then a class from
     *                            self::NAMESPACE_APPLICATION_CORE will be returned. If the $applicationNamespace IS provided only classes in
     *                            that namespace will be considered.
     *
     * $moduleNamespace         : If $moduleNamespace is provided, then only classes within the module's namespace will be considered.
     *                            If it is not provided, then classes from all namespaces will be considered, however, if there is
     *                            more than one class with the same $className, this method will throw an Exception.
     *
     * $classTypeNamespace      : This should be either {@see \Core\Library\Loader::NAMESPACE_TYPE_PAGE}, {@see \Core\Library\Loader::NAMESPACE_TYPE_MODEL} or {@see \Core\Library\Loader::::NAMESPACE_TYPE_BLOCK}. Defaults to {@see \Core\Library\Loader::NAMESPACE_TYPE_PAGE}
     *
     * $isAdmin                 : Boolean indicating if the class is an admin class, or not.
     *
     * This method is called by other loader functions, which should be used whenever possible in order to load classes with fewer parameters
     *
     * @see \Core\Library\Loader::page()
     * @see \Core\Library\Loader::model()
     *
     * @return mixed An object of the requested class
     *
     * @param string $className The name of the class to return
     * @param array $params An array of parameters to be passed to the new class' constructor
     * @param string $applicationNamespace The namespace for the application (either self::NAMESPACE_APPLICATION_CORE or self::NAMESPACE_APPLICATION_APPLICATION). Defaults to self::NAMESPACE_APPLICATION_CORE
     * @param string $moduleNamespace The namespace of the specific module from which to load the class. Defaults to self::NAMESPACE_LIBRARY_BASE
     * @param string $classTypeNamespace The namespace of the type of class (either self::NAMESPACE_TYPE_PAGE or self::NAMESPACE_TYPE_MODEL or self::NAMESPACE_TYPE_BLOCK). Defaults to self::NAMESPACE_CONTROLLER
     * @param bool $isAuthenticated (optional) Determines if the page class to load is an authenticated class. Defaults to false
     */
    public function moduleClass($className, $params=array(), $applicationNamespace=null, $moduleNamespace=null, $classTypeNamespace=null, $isAuthenticated=null){
        if($applicationNamespace === null){
            $applicationNamespace = self::NAMESPACE_APPLICATION_CORE;
        }
        if($classTypeNamespace === null){
            $classTypeNamespace = self::NAMESPACE_TYPE_PAGE;
        }
        //Only consider classes in the specified module
        if($moduleNamespace !== null){
            $class = self::NAMESPACE_LOCATION_MODULE.'\\'.$moduleNamespace.'\\'.($isAuthenticated ? self::NAMESPACE_PERMISSION_AUTHENTICATED : self::NAMESPACE_PERMISSION_ANONYMOUS).'\\'.$classTypeNamespace.'\\'.$className;
            if(array_key_exists(self::NAMESPACE_APPLICATION_APPLICATION.'\\'.$class, $this->classes)){
                return $this->loadClass(self::NAMESPACE_APPLICATION_APPLICATION.'\\'.$class, $params);
            }elseif(array_key_exists(self::NAMESPACE_APPLICATION_CORE.'\\'.$class, $this->classes)){
                return $this->loadClass(self::NAMESPACE_APPLICATION_CORE.'\\'.$class, $params);
            }else{
                throw new \Exception("Module '".$className."' could not be found at '".self::NAMESPACE_APPLICATION_APPLICATION.'\\'.$class."' or '".self::NAMESPACE_APPLICATION_CORE.'\\'.$class."'.");
            }
        }else{
            //See if we can find the class $className. Must exist in only one namespace, or an exception is thrown
            $classes = $this->findClassInfo($className, self::NAMESPACE_LOCATION_MODULE);
            if(!empty($classes)){
                $moduleClasses = array();
                foreach($classes as $class){
                    //$this->print_array($class);
                    if($classTypeNamespace===null || $class['namespaces']['type'] == $classTypeNamespace){
                        if($applicationNamespace=== null || $class['namespaces']['application'] == $applicationNamespace){
                            if($isAuthenticated === null || $this->isAuthenticated($class['namespace_class']) == $isAuthenticated){
                                if(!array_key_exists($class['namespaces']['module'], $moduleClasses)){
                                    $moduleClasses[$class['namespaces']['module']] = array();
                                }
                                $moduleClasses[$class['namespaces']['module']][] = $class;
                            }
                        }
                    }
                }
                if(count($moduleClasses) < 1){
                    throw new \Exception("Module ".$classTypeNamespace." '".$className."' has not been declared.");
                }elseif(count($moduleClasses) > 1){
                    throw new \Exception("Module ".$classTypeNamespace." '".$className."' is ambiguous. Include the \$moduleNamespace parameter to disambiguate which module ".$classTypeNamespace." to load.");
                }
                $moduleClasses = array_shift($moduleClasses);
                if(count($moduleClasses) == 1){
                    $class = array_shift($moduleClasses);
                    return $this->loadClass($class['namespace_class'], $params);
                }else{
                    throw new \Exception("Module ".$classTypeNamespace." '".$className."' is ambiguous. Consider the \$moduleNamespace or \$isAuthenticated parameter to disambiguate which module ".$classTypeNamespace." to load.");
                }
            }else{
                throw new \Exception("Module ".$moduleNamespace." '".$className."' has not been declared.");
            }
        }
    }
    
    /**
     * load a page class from a module and return it. See {@see \Core\Library\Loader::moduleClass()} for more information
     *
     *
     * @return mixed The requested class
     * @param string $className The name of the class to return. Can be fully namespaced, but not required.
     * @param array $params (optional) The parameters to pass to the constructor of $className. Must be included if setting $moduleNamespace or $isAuthenticated
     * @param string $moduleNamespace (optional) Only consider classes in the $moduleNamespace namespace.
     * @param bool $isAuthenticated (optional) Determines if the page class to load is an authenticated class. Defaults to false
     */
    public function page($className, $params=array(), $moduleNamespace=null, $isAuthenticated=null){
        if(!empty($this->classes[$className])){
            $isAuthenticated = $this->isAuthenticated($this->classes[$className]['namespace_class']);
            if($moduleNamespace === null){
                $moduleNamespace = $this->classes[$className]['namespaces']['module'];
            }
            $className = $this->classes[$className]['class'];
        }
        return $this->moduleClass($className, $params, null, $moduleNamespace, self::NAMESPACE_TYPE_PAGE, $isAuthenticated);
    }
    
    /**
     * load a model class from a module and return it. See {@see \Core\Library\Loader::moduleClass()} for more information
     *
     *
     * @return mixed The requested class
     * @param string $className The name of the class to return. Can be fully namespaced, but not required.
     * @param array $params (optional) The parameters to pass to the constructor of $className. Must be included if setting $moduleNamespace or $isAuthenticated
     * @param string $moduleNamespace (optional) Only consider classes in the $moduleNamespace namespace.
     * @param bool $isAuthenticated (optional) Determines if the model to load is an authenticated model. Defaults to false
     */
    public function model($className, $params=array(), $moduleNamespace=null, $isAuthenticated=null){
        if(!empty($this->classes[$className])){
            $isAuthenticated = $this->isAuthenticated($this->classes[$className]['namespace_class']);
            if($moduleNamespace === null){
                $moduleNamespace = $this->classes[$className]['namespaces']['module'];
            }
            $className = $this->classes[$className]['class'];
        }
        return $this->moduleClass($className, $params, null, $moduleNamespace, self::NAMESPACE_TYPE_MODEL, $isAuthenticated);
    }
    
    /**
     * load a block class from a module and return it. See {@see \Core\Library\Loader::moduleClass()} for more information
     *
     *
     * @return mixed The requested class
     * @param string $className The name of the class to return. Can be fully namespaced, but not required.
     * @param array $params (optional) The parameters to pass to the constructor of $className. Must be included if setting $moduleNamespace or $isAuthenticated
     * @param string $moduleNamespace (optional) Only consider classes in the $moduleNamespace namespace.
     * @param bool $isAuthenticated (optional) Determines if the block class to load is an authenticated model. Defaults to false
     */
    public function block($className, $params=array(), $moduleNamespace=null, $isAuthenticated=null){
        if(!empty($this->classes[$className])){
            $isAuthenticated = $this->isAuthenticated($this->classes[$className]['namespace_class']);
            if($moduleNamespace === null){
                $moduleNamespace = $this->classes[$className]['namespaces']['module'];
            }
            $className = $this->classes[$className]['class'];
        }
        return $this->moduleClass($className, $params, null, $moduleNamespace, self::NAMESPACE_TYPE_BLOCK, $isAuthenticated);
    }
    
    /**
     * Loads a class from the {@see \Core\Library\Database} or Application\Library\Database namespace
     * Note: If a library has a namespace deeper than Core\Library\Database or Application\Library\Database, then the namespace below that level
     * must be included.
     *
     * <b>Example:</b>
     *
     * ```php
     *
     * //the DB object is in the namespace Core\Library\Database, so we only need the class name.
     * $db = $this->load->database('DB', array('PDOMySQL'));
     * 
     * //The Request object is in the Core\Library\Database\Driver namespace and since the Driver namespace is below the Core\Libray\Database namespace we need to include it as part of the classname
     * $driver = $this->load->library('Driver\PDOMySQL', array($db, $host, $user, $password, $database));
     * 
     * ```
     *
     * @param string $className The name of the class to be loaded, without the full namespace
     * @param array @params The parameters to use to construct the class
     * @param bool $forceCore (optional) Boolean to determine if overloaded classes should be returned, or just the core class. Defaults to false
     */
    public function database($className, $params=array(), $forceCore=false){
        return $this->library(self::NAMESPACE_LIBRARY_DATABASE.'\\'.$className, $params, $forceCore);
    }
    
    /**
     * Determine if a page class needs authentication
     *
     * @return bool true if $className is an authenticated page, otherwise false
     * @param string $className The fully namespaced class name
     */
    public function isAuthenticated($className){
        if(substr($className,-5) == 'Trace'){
            $className = substr($className,0, -5);
        }
         if(!empty($this->classes[$className])){
            if($this->classes[$className]['namespaces']['permission'] == self::NAMESPACE_PERMISSION_AUTHENTICATED){
                return true;
            }else{
                return false;
            }
         }
         return false;
    }
    
    /**
     * Determine if a module class exists.
     *
     * @return bool true if the class exists, false otherwise
     * @param string $className The name of the class to check
     * @param string $moduleNamespace (optional) Only consider classes in the $moduleNamespace namespace.
     * @param string $classTypeNamespace The namespace of the type of class (either {@see \Core\Library\Loader\::NAMESPACE_TYPE_PAGE} or {@see \Core\Library\Loader\::NAMESPACE_TYPE_MODEL} or {@see \Core\Library\Loader\::NAMESPACE_TYPE_BLOCK}). Defaults to {@see \Core\Library\Loader\::NAMESPACE_TYPE_PAGE}
     * @param bool $isAuthenticated (optional) Determines if the class to load is an authenticated class. Defaults to false
     */
    public function moduleClassExists($className, $moduleNamespace=null, $classTypeNamespace=null, $isAuthenticated=null){
        if(!empty($this->classes[$className])){
            $isAuthenticated = $this->isAuthenticated($this->classes[$className]['namespace_class']);
            $classTypeNamespace = $this->classes[$className]['namespaces']['type'];
            if($moduleNamespace === null){
                $moduleNamespace = $this->classes[$className]['namespaces']['module'];
            }
            $className = $this->classes[$className]['class'];
        }
        
        if($moduleNamespace !== null){
            $class = self::NAMESPACE_LOCATION_MODULE.'\\'.$moduleNamespace.'\\'.($isAuthenticated ? self::NAMESPACE_PERMISSION_AUTHENTICATED : self::NAMESPACE_PERMISSION_ANONYMOUS).'\\'.$classTypeNamespace.'\\'.$className;
            if(array_key_exists(self::NAMESPACE_APPLICATION_APPLICATION.'\\'.$class, $this->classes) || array_key_exists(self::NAMESPACE_APPLICATION_CORE.'\\'.$class, $this->classes)){
                return true;
            }else{
                return false;
            }
        }else{
            //See if we can find the class $className. Must exist in only one namespace, or an exception is thrown
            $classes = $this->findClassInfo($className, self::NAMESPACE_LOCATION_MODULE);
            if(!empty($classes)){
                //Arrange the found classes into their module's namespaces
                $moduleClasses = array();
                foreach($classes as $class){
                    if($classTypeNamespace===null || $class['namespaces']['type'] == $classTypeNamespace){
                        if($isAuthenticated === null || $this->isAuthenticated($class['namespace_class']) == $isAuthenticated){
                            if(!array_key_exists($class['namespaces']['module'], $moduleClasses)){
                                $moduleClasses[$class['namespaces']['module']] = array();
                            }
                            $moduleClasses[$class['namespaces']['module']][] = $class;
                        }
                    }
                }
                //If we don't find exactly one class, then return false
                if(count($moduleClasses) < 1 || count($moduleClasses) > 1){
                    return false;
                }
                //We have exactly one class
                $moduleClasses = array_shift($moduleClasses);
                if(count($moduleClasses) == 1){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    }
    
    /**
     * Determine if a module page class exists.
     *
     * @return bool true if the page class exists, false otherwise
     * @param string $className The name of the class to check
     * @param string $moduleNamespace (optional) Only consider classes in the $moduleNamespace namespace.
     * @param bool $isAdmin (optional) Determines if the page class to load is an authenticated page. Defaults to false
     */
    public function pageExists($className, $moduleNamespace=null, $isAdmin=null){
        return $this->moduleClassExists($className, $moduleNamespace, self::NAMESPACE_TYPE_PAGE, $isAuthenticated);
    }
    
    /**
     * Determine if a module model exists.
     *
     * @return bool true if the model exists, false otherwise
     * @param string $className The name of the class to check
     * @param string $moduleNamespace (optional) Only consider classes in the $moduleNamespace namespace.
     * @param bool $isAdmin (optional) Determines if the model to load is an authenticated page. Defaults to false
     */
    public function modelExists($className, $moduleNamespace=null, $isAuthenticated=null){
        return $this->moduleClassExists($className, $moduleNamespace, self::NAMESPACE_TYPE_MODEL, $isAuthenticated);
    }
}   
?>