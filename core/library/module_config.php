<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library;
/**
 * Provides read only access to a Module's config.php information.
 * Class will standardize parts of the config.php data to ensure uniformity across modules.
 *
 * @todo setStandardConfig should automatically add foreign keys for relationships
 * @todo Change the config file to use a config class. Might need a whole namespace for {@see \Core\Module\Config}
 *
 * @package \Core\Library\ModuleConfig
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class ModuleConfig extends BaseObject{
    /**
     * The name of the module who's config.php is being accessed
     * @var string 
     */
    private $module;
    /**
     * Storage of the module's config.php information.
     * @var array
     */
    private $configs;
    
    /**
     * Keeps track of the unix timestamp for the last time the config file was edited.
     * @var int
     */
    private $updatedAt;
    
    /**
     * Constructs a new instance, loads and standardizes the modules config.php
     * @see \Core\Library\ModuleConfig::load()
     * @see \Core\Library\ModuleConfig::setStandardConfig()
     * @return \Core\Library\ModuleConfig
     * @param string $module The name of the module who's config.php is to be accessed
     * @param array $config When constructing this class without a module this paramerter should contain what would normally be in a config.php file
     */
    public function __construct($module=null, $config=array()){
        
        if($module !== null){
            $this->load($module);
        }
        if(!empty($config)){
            $this->configs = $config;
            $this->updatedAt = time();
        }
        $this->setStandardConfig();
    }
    
    /**
     * Loads the module's config.php, and sets the $configs property
     *
     * @return null
     * @param string $module The name of the module who's config.php should be loaded.
     */
    public function load($module){
        
        if(is_dir(CORE_DIR_MODULE.$module)){
            $this->module = $module;
            if(file_exists(CORE_DIR_MODULE.$module.DIRECTORY_SEPARATOR.'config.php')){
                $this->updatedAt = filemtime(CORE_DIR_MODULE.$module.DIRECTORY_SEPARATOR.'config.php');
                include(CORE_DIR_MODULE.$module.DIRECTORY_SEPARATOR.'config.php');
                if(!empty($config)){
                    $this->configs = $config;
                }else{
                    $this->configs = array();
                }
            }else{
                $this->updatedAt=0;
                $this->configs = array();
            }
        }else{
            throw new \Exception("Module ".$module." could not be found at '".CORE_DIR_MODULE.$module."'");
        }
    }
    
    /**
     * Get the unix timestamp of the last time the config file was updated.
     */
    public function getLastUpdate(){
        return $this->updatedAt;
    }
    
    /**
     * Function will fill in any missing configuration values, based on existing values and
     * standardize the configuration across modules.
     * For database tables, all Unique fields will each have an index, and Primary Key fields will have a PRIMARY index
     *
     * @return null
     */
    public function setStandardConfig(){
        if(array_key_exists('tables',$this->configs)){
            foreach($this->configs['tables'] as $tableIndex=>$table){
                $primary=array();
                $unique=array();
                foreach($table['fields'] as $field){
                    if($field['primary']){
                        $primary[$field['name']]=$field;
                    }
                    if($field['unique']){
                        $unique[$field['name']] = $field;
                    }
                }
                if(count($primary) > 1){
                    throw new \Exception("Table '".$table['name']."' in module '".$this->module."' has multiple primary keys defined in the config. Each Table should have only ONE primary key.");
                }
                //Make sure the primary index exists in the config
                foreach($primary as $p){
                    $exists=false;
                    $hasPrimaryIndexDefined=false;
                    if(array_key_exists('indexes',$table)){
                        foreach($table['indexes'] as $indexData){
                            if($indexData['name'] == 'PRIMARY'){
                                $hasPrimaryIndexDefined=true;
                                if(in_array($p['name'], $indexData['columns'])){
                                    $exists=true;
                                    break;
                                }
                            }
                        }
                    }
                    if(!$exists){
                        //Add the index to the config
                        $this->configs['tables'][$tableIndex]['indexes']['PRIMARY'] = array(
                            'unique'        => true,
                            'name'          => 'PRIMARY',
                            'columns'       => array($p['name']),
                            'type'          => 'BTREE',
                            'collation'     => 'A',             //optional
                            'cardinality'   => '0',             //optional
                            'sub_part'      => null,            //optional
                            'packed'        => null,            //optional
                            'null'          => '',              //optional
                            'comment'       => '',              //optional
                            'index_comment' => '',              //optional
                            
                        );
                    }else{
                        if($hasPrimaryIndexDefined){
                            throw new \Exception("Table '".$table['name']."' in module '".$this->module."' has a primary index defined, but it's not for the field '".$p['name']."' which is defined as a primary field.");
                        }
                    }
                }
                //Make sure all unique fields have an index (mysql adds them automatically)
                //All unique indexes ar put together into one big unique index.
                $columns = array();
                $shouldCreate=true;
                if(array_key_exists('indexes', $table)){
                    foreach($table['indexes'] as $ii=>$indexData){
                        if($indexData['name'] != 'PRIMARY' && $indexData['unique'] == true){
                            $hasAllUnique=true;
                            foreach($unique as $u){
                                if(in_array($u['name'], $indexData['columns']) === false){
                                    $hasAllUnique=false;
                                }
                            }
                            if($hasAllUnique){
                                $shouldCreate=false;
                            }
                        }
                        //Set some defaults
                        if(empty($indexData['type'])){
                            $this->configs['tables'][$tableIndex]['indexes'][$ii]['type'] = 'BTREE';
                        }
                        if(empty($indexData['collation'])){
                            $this->configs['tables'][$tableIndex]['indexes'][$ii]['collation'] = 'A';
                        }
                        if(empty($indexData['cardinality'])){
                            $this->configs['tables'][$tableIndex]['indexes'][$ii]['cardinality'] = '0';
                        }
                        if(empty($indexData['sub_part'])){
                            $this->configs['tables'][$tableIndex]['indexes'][$ii]['sub_part'] = null;
                        }
                        if(empty($indexData['packed'])){
                            $this->configs['tables'][$tableIndex]['indexes'][$ii]['packed'] = null;
                        }
                        if(empty($indexData['null'])){
                            $this->configs['tables'][$tableIndex]['indexes'][$ii]['null'] = '';
                        }
                        if(empty($indexData['comment'])){
                            $this->configs['tables'][$tableIndex]['indexes'][$ii]['comment'] = '';
                        }
                        if(empty($indexData['index_comment'])){
                            $this->configs['tables'][$tableIndex]['indexes'][$ii]['index_comment'] = '';
                        }
                    }
                }
                if($shouldCreate){
                    $name='';
                    $columns = array();
                    foreach($unique as $u){
                        $name .= $u['name'].'_';
                        $columns[] = $u['name'];
                    }
                    $name = rtrim($name, '_');
                    if(!array_key_exists('indexes', $this->configs['tables'][$tableIndex])){
                        $this->configs['tables'][$tableIndex]['indexes'] = array();
                    }
                    $this->configs['tables'][$tableIndex]['indexes'][$name] = array(
                        'unique'        => true,
                        'name'          => $name,
                        'columns'       => $columns,
                        'type'          => 'BTREE',
                        'collation'     => 'A',             //optional
                        'cardinality'   => '0',             //optional
                        'sub_part'      => null,            //optional
                        'packed'        => null,            //optional
                        'null'          => '',              //optional
                        'comment'       => '',              //optional
                        'index_comment' => '',              //optional
                        
                    );
                }
                
            }
        }
    }
    
    /**
     * Magic method to allow access to config options using -> nomanclature.
     * 
     * @param string $name The config value to be returned.
     * @return mixed The value of the config option requested
     */
    public function __get($name){
        return $this->get($name);
    }
    
    /**
     * Get a value from the module's config.php file. If $name is an array, then subvalues will be returned.
     *
     * @see \Core\Library\ModuleConfig\getValue()
     * @return mixed The value\subvalue requested
     * @param string|array $name The name of the value, or an array containing the path to the value
     */
    public function get($name){
        return $this->getValue($name, $this->configs);
    }
    
    /**
     * Determine if a value exists in the config
     *
     * @param string $name The name of the config option to check
     */
    public function valueExists($name, $configs=array()){
        if(is_array($name)){
            $get = array_shift($name);
        }else{
            $get = $name;
        }
        if(empty($configs)){
            $configs = $this->configs;
        }
        if(array_key_exists($get, $configs)){
            $value = $configs[$get];
            if(!is_array($name) || empty($name)){
                return true;
            }else{
                return $this->valueExists($name,$value);
            }
        }else{
            return false;
        }
    }
    
    /**
     * Get a value from the $configs array. If $name is an array, then subvalues will be returned.
     * If $configs is not provided, then the Module's configs will be used.
     *
     * Example:
     * ```php
     * $conf = $this->registry->load->library('ModuleConfig', array('router'));
     * $tableName = $conf->getValue(array('tables', 'url', 'name')); //Equivilent to $configs['table']['url']['name'] or $conf->table->url->name
     * ```
     *
     * @return mixed The value requested
     * @param string|array $name The name or path of the value requested
     * @param array $configs (optional) an array of config values from which to return the requested value
     */
    private function getValue($name, $configs=array()){
        if(is_array($name)){
            $get = array_shift($name);
        }else{
            $get = $name;
        }
        if(empty($configs)){
            $configs = $this->configs;
        }
        if(array_key_exists($get, $configs)){
            $value = $configs[$get];
            if(!is_array($name) || empty($name)){
                return $value;
            }else{
                return $this->getValue($name,$value);
            }
        }else{
            throw new \Exception("Invalid configuration item: '".$get."'");
        }
    }
}