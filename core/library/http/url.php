<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Http;
use Core;
/**
 * Class handles converting page/method to URL, and URL to page/method
 *
 * @package \Core\Library\Http\URL
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class URL extends Core\Library\BaseObject{
    
    /**
     * Convert an {@see \Core\Library\Http\Action} object to a URL
     *
     * @todo Make sure this method is working as expected
     * 
     * @return string a url
     * @param \Core\Library\Http\Action $action The action for which to get a url
     * @param string $requestMethod The request method (GET, POST, PUT) of the URI
     */
    public function fromAction($action, $requestMethod){
        $model = $this->load->library('Http\Route', array($action, $requestMethod));
        $uri = $model->getURIFromAction($action, $requestMethod);
        return $uri;
    }
    
    /**
     * Convert a URI to an {@see \Core\Library\Http\Action} object, containing the page, and method
     *
     * @return \Core\Library\Http\Action|null The action object represented by the URI. Null is returned if the URI can't be found
     * @param string $uri The URI to convert to an action object
     * @param string $requestMethod The request method (GET, POST, PUT) of the URI
     */
    public function toAction($uri, $requestMethod){
        $model = $this->load->model('Route', array($uri, $requestMethod),'Route', false);
        $record = $model->getAction();
        if(!empty($record)){
            $action = $this->load->library('Http\Action');
            $action->setPage($this->load->page($record->getRelatedRecords('route_to_route_action')[0]->page));
            $action->setMethod($record->getRelatedRecords('route_to_route_action')[0]->method);
            return $action;
        }else{
            return null;
        }
    }
}
?>