<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Http;
use Core;
/**
 * Handles the HTTP request
 *
 * This class is loaded by {@see \Core\phpStack::initRegistry()}, so it is available to all code. It is the entry point for
 * processing requests, and responsible for setting receiving the HTTP request, and routing it to the appropriate page to be
 * processed. This class also provides access to the various HTTP request information such as $_POST, $_GET, $_SERVER, $_SESSION and
 * $_COOKIE.
 *
 * This class sets up the "onRequestReceived" Event, which is used to load the "Action" ({@see \Core\Library\Http\Action}) specified by the
 * requestd URL, and then executes the action. The "Action" subsequently loads the appropriate application page, and returns it's
 * result to this request ({@see \Core\Library\Http\Request}). If a page returns another "Action" object, the request will be "forwarded"
 * to the new "Action". This will continue until no more "Actions" need to be processed.
 *
 * This class is also used to "redirect" the request to another URL.
 *
 * <b>Events</b>
 * 
 * beforeRequest    : triggered after the framework has been fully loaded, but before the request is processed
 * afterRequest     : triggered after the request has been processed.
 *
 * 
 *
 * @package \Core\Library\Http\Request
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Request extends Core\Library\BaseObject{
    
        /**
     * The $_GET information
     * @var \Core\Library\Http\RequestVar
     */
    public $get;
    
    /**
     * The $_POST information
     * @var \Core\Library\Http\RequestVar
     */
    public $post;
    
    /**
     * The $_REQUEST information
     * @var \Core\Library\Http\RequestVar
     */
    public $request;
    
    /**
     * The $_SESSION information
     * @var \Core\Library\Http\RequestVar
     */
    public $session;
    
    /**
     * The $_SERVER information
     * @var \Core\Library\Http\RequestVar
     */
    public $server;
    
    /**
     * The $_COOKIE information
     * @var \Core\Library\Http\RequestVar
     */
    public $cookie;
    
    /**
     * Initializes the class, and registers events. Triggers the onRequestReceived event
     *
     * @return null
     */
    public function init(){
        $this->get = $this->registry->load->library('Http\RequestVar',array(&$_GET));
        $this->request = $this->registry->load->library('Http\RequestVar',array(&$_REQUEST));
        $this->post = $this->registry->load->library('Http\RequestVar',array(&$_POST));
        $this->session = $this->registry->load->library('Http\RequestVar',array(&$_SESSION));
        $this->server = $this->registry->load->library('Http\RequestVar',array(&$_SERVER));
        $this->cookie = $this->registry->load->library('Http\RequestVar',array(&$_COOKIE));
        
        $this->registerEvents();
    }
    
    /**
     * Registers the events for this object. See class definition for event details
     *
     * @return null
     */
    public function registerEvents(){
        $this->registry->event->registerEvent('beforeRequest', \Core\Library\Event::CONTEXT_GLOBAL);
        $this->registry->event->registerEvent('afterRequest', \Core\Library\Event::CONTEXT_GLOBAL);
    }
    
    /**
     * Process the "Action" ({$see \Core\Library\Http\Action}) requested the _route_ querystring parameter (set by .htaccess);
     * If the "Action" returns another action, then continue processing until no "Action" is returned.
     * At the end of this function, the response ({@see \Core\Library\Http\Response}) should have been fully populated by the pages
     * that the "Action" processed.
     *
     * @return null
     */
    public function process(){
        $this->event->trigger('beforeRequest',\Core\Library\Event::CONTEXT_GLOBAL);
        //Use the router module to get the action object
        $router = $this->load->library('Http\Route', array($this->getURI(), $this->server->REQUEST_METHOD));
        $action = $router->routeRequest();
        //Process actions until they stop returning more actions.
        while($action){
            $action = $action->execute();
        }
        //The response object should be fully populated at this point.
        $this->event->trigger('afterRequest',\Core\Library\Event::CONTEXT_GLOBAL);
    }
    
    /**
     * Get the requested URI
     * @return string The URI requested
     */
    public function getURI(){
        $route = $this->get->_route_;
        $route = (empty($route) ? '/' : $route);
        return $route;
    }
    
    
    /**
     * Redirect a request to a new URL.
     *
     * $redirectTo Can either be a url or a {@see \Core\Library\Http\Action} object.
     *
     * @return null
     * @param \Core\Library\Http\Action|string $redirectTo Either the Action object, or the URL to which the request should be redirected
     * @param string|int $redirectCode The redirect code. valid values are '301' (Permenant) or '302' (Temporary)
     */
    public function redirect($redirectTo, $redirectCode=302){
        if($redirectCode != '301' && $redirectCode != '302'){
            throw new \Exception("'".$redirectCode."' is not a valid redirect code. Please use either '301' or '302'");
        }
        if(is_object($redirectTo)){
            if(get_class($redirectTo) == "Core\Library\Http\Action" || is_subclass_of($redirectTo,"Core\Library\Http\Action")){
                $newURL = $this->url->fromAction($redirectTo, "GET");
            }else{
                throw new \Exception("Core\Library\Http\Request::redirect() expects the first parameter to either be a URL, or an object that is (or extends) 'Core\Library\Http\Action'");
            }
        }else{
            $newURL = $redirectTo;
        }
        header('Location: '.$newURL, true, $redirectCode);
		exit;
    }
    
    /**
     * Creates a {@se \Core\Library\Http\Action} object routed to the $page and $method
     *
     * This is meant to have it's value returned from a page, which will then forward the request to the new $page and $method
     *
     * @todo Make sure this method still works as expected.
     * $page can be one of:
     *      1) A page object
     *      2) A string name of a page object
     *      3) A URI Route
     */
    public function forward($page, $method){
        $action = $this->load->library('Http\Action');
        $action->setMethod($method);
        if(is_object($page)){
            $action->setPage($page);
        }else{
            if($this->load->pageExists($page)){
                $action->setPage($this->load->page($page));
            }else{
                $router = $this->load->library('Http\Route', array($page, $method));
                $action = $router->routeRequest();
                
            }
        }
        return $action;
    }
}
?>