<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Http;
use Core;
/**
 * The generic template class. This class is a utility class used to work with templates for objects which require them
 * This class is responsible for
 * 1. Determining the specific template file for a class
 * 2. passing variables to/from templates
 * 3. Rendering the template and returning it's HTML
 *
 *
 * @package \Core\Library\Http\Template
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Template extends Core\Library\BaseObject{
    /**
     * The name of the default theme
     * @var string
     */
    const THEME_DEFAULT='default';   
    /**
     * The name of the default directory in which templates are stored
     * @var string
     */
    const TEMPLATE_DIR_NAME='templates';
    /**
     * The name of the default directory in which template assets are stored
     * @var string
     */
    const TEMPLATE_ASSETS_DIR='assets';
    /**
     * The name of the default directory in which template Javascript assets are stored
     * @var string
     */
    const TEMPLATE_SCRIPT_DIR='js';
    
    /**
     * The name of the default directory in which template CSS assets are stored
     * @var string
     */
    const TEMPLATE_STYLE_DIR='css';
    
    /**
     * Storage for the directory for the module housing this page
     * @var string
     */
    private $moduleDir;
    
    /**
     * The name of the theme for this block's content to be loaded from. View files should be stored within a directory of this name
     * @var string
     */
    protected $theme='default';
    
    /**
     * The specific template for this page's output
     * @var string
     */
    protected $template;
    
    /**
     * The name of the directory housing the template file.
     * @var string
     */
    protected $templateDir=null;
    
    /**
     * The parent object to which this template belongs
     * @var mixed
     */
    protected $parent=null;
    
    /**
     * Storage array for the template variables
     */
    protected $templateVars=array();
    
    /**
     * Constructor
     * @return \Core\Library\Http\Template
     * @param mixed $parent The parent object to which this template belongs
     */
    public function __construct($parent){
        if(!is_object($parent)){
            throw new \Exception("A Core\Library\Http\Template object must be constructed with an object");
        }
        $this->parent = $parent;
    }
    
    /**
     * Initialize the template object
     * @return null
     */
    public function init(){
    }
    
    /**
     * Change the theme to use for this page. Template files should be stored within the module in a directory that is the same as the theme name.
     *
     * @return null
     * @param string $newDir The new directory to find view files. This is not a full path, but the name of the directory within the module.
     */
    public function setTheme($theme){
        $this->theme = $theme;
    }
    
    /**
     * Get the name of the theme for this page.
     *
     * @see Core\Module\Base\Anonymous\Page\Page::setTheme()
     * @see Core\Module\Base\Anonymous\Page\Page::$theme
     * @return string The name of the theme for this page
     */
    public function getTheme(){
        return $this->theme;
    }
    
    /**
     * Set a variable to be used in the template for this page
     * @return null
     * @param string $name The name of the variable to set
     * @param mixed $value The value of the view variable
     */
    public function setTemplateVar($name, $value){
        $this->templateVars[$name] = $value;
    }
    
    /**
     * Get the value of a template variable
     * @return mixed
     * @param string $name The name of the variable to get
     */
    public function getTemplateVar($name){
        if(array_key_exists($name,$this->templateVars)){
            return $this->templateVars[$name];
        }else{
            return null;
        }
    }
    
    /**
     * Change the directory name (sub directory within the module's base directory) that this page will look in to find view
     * files. This does not need to be called if the view files are stored in the default 'view' directory. Within this directory, there
     * should be a directory for each theme, inside of which that theme's view files should be stored.
     *
     * @return \Core\Library\Http\Template Return value is $this for chainability
     * @param string $newDir The new directory to find view files. This is not a full path, but the name of the directory within the module.
     */
    public function setTemplateDir($newDir){
        if(!file_exists($newDir) || !is_dir($newDir)){
            throw new \Exception("Template Directory must be a valid directory");
        }
        $this->templateDir = rtrim($newDir, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
        return $this;
    }
    
    /**
     * Get the directory for this page's template. If the directory was explicitly set, then it will be returned, otherwise the method will check:
     * 1. the currently set theme in the "application" folder
     * 2. the "default" theme in the "application" folder
     * 3. the currently set theme in the "core" folder
     * 4. the "default" theme in the "core" folder
     *
     * If the template file has been set, then this method will return the first directory from above encountered with the template file existing. If the template
     * file has not yet been set, this method will return the first directory from above encountered that exists.
     *
     * If a directory can not be located, an exception will be thrown.
     * 
     * @return string A directory name in the $moduleDir which houses this page's template(s)
     */
    public function getTemplateDir(){
        if(empty($this->templateDir)){
            $classInfo = $this->load->getClassInfo(get_class($this->parent));
            $temp = explode(DIRECTORY_SEPARATOR,str_replace(array(CORE_DIR_MODULE, APP_DIR_MODULE), '', dirname($classInfo['file'])));
            $moduleDirName = array_shift($temp);
            $appTemplatePath = APP_DIR_MODULE.$moduleDirName.DIRECTORY_SEPARATOR.self::TEMPLATE_DIR_NAME.DIRECTORY_SEPARATOR;
            $coreTemplatePath = CORE_DIR_MODULE.$moduleDirName.DIRECTORY_SEPARATOR.self::TEMPLATE_DIR_NAME.DIRECTORY_SEPARATOR;
            $currentTheme = $this->getTheme();
            $currentThemeFile = $this->getTemplate();
            
            if(file_exists($appTemplatePath) && is_dir($appTemplatePath)){
                if(file_exists($appTemplatePath.$currentTheme) && is_dir($appTemplatePath.$currentTheme)){
                    //The application namespace has a theme folder defined.
                    if(file_exists($appTemplatePath.$currentTheme.DIRECTORY_SEPARATOR.$currentThemeFile)){
                        //We found our template file!
                        $this->templateDir = $appTemplatePath.$currentTheme.DIRECTORY_SEPARATOR;
                    }
                }elseif($currentTheme !== self::THEME_DEFAULT && file_exists($appTemplatePath.self::THEME_DEFAULT) && is_dir($appTemplatePath.self::THEME_DEFAULT)){
                    if(file_exists($appTemplatePath.self::THEME_DEFAULT.DIRECTORY_SEPARATOR.$currentThemeFile)){
                        //We found our template file!
                        $this->templateDir = $appTemplatePath.self::THEME_DEFAULT.DIRECTORY_SEPARATOR;
                    }
                }
            }elseif(file_exists($coreTemplatePath) && is_dir($coreTemplatePath)){
                if(file_exists($coreTemplatePath.$currentTheme) && is_dir($coreTemplatePath.$currentTheme)){
                    //The application namespace has a theme folder defined.
                    if(file_exists($coreTemplatePath.$currentTheme.DIRECTORY_SEPARATOR.$currentThemeFile)){
                        //We found our template file!
                        $this->templateDir = $coreTemplatePath.$currentTheme.DIRECTORY_SEPARATOR;
                    }
                }elseif($currentTheme !== self::THEME_DEFAULT && file_exists($coreTemplatePath.self::THEME_DEFAULT) && is_dir($coreTemplatePath.self::THEME_DEFAULT)){
                    if(file_exists($coreTemplatePath.self::THEME_DEFAULT.DIRECTORY_SEPARATOR.$currentThemeFile)){
                        //We found our template file!
                        $this->templateDir = $coreTemplatePath.self::THEME_DEFAULT.DIRECTORY_SEPARATOR;
                    }
                }
            }
            if(empty($this->templateDir)){
                throw new \Exception("Could not locate the template directory at '".$appTemplatePath.$currentTheme.DIRECTORY_SEPARATOR."', ".($currentTheme !== self::THEME_DEFAULT ? "'".$appTemplatePath.self::THEME_DEFAULT.DIRECTORY_SEPARATOR."', " : '').($currentTheme == self::THEME_DEFAULT ? "or " : '')."'".$coreTemplatePath.$currentTheme.DIRECTORY_SEPARATOR."'".($currentTheme !== self::THEME_DEFAULT ? ", or '".$coreTemplatePath.self::THEME_DEFAULT.DIRECTORY_SEPARATOR."'" : ''));
            }
        }
        
        return $this->templateDir;
    }
    
    /**
     * Get the URL for this template
     *
     * @return string The URL to this templates directory
     */
    public function getURL(){
        return str_replace(DIRECTORY_SEPARATOR,'/',HTTP_ROOT.str_replace(DIR_ROOT, '', $this->getTemplateDir()));
    }
    
    /**
     * Get the URL for the javascript assets for this template
     *
     * @return string The URL to this templates javascript assets
     */
    public function getScriptURL(){
        return $this->getAssetsURL().self::TEMPLATE_SCRIPT_DIR."/";
    }
    
    /**
     * Get the URL for the CSS assets for this template
     *
     * @return string The URL to this templates CSS assets
     */
    public function getStyleURL(){
        return $this->getAssetsURL().self::TEMPLATE_STYLE_DIR."/";
    }
    
    /**
     * Get the URL for the assets for this template
     *
     * @return string The URL to this templates assets directory
     */
    public function getAssetsURL(){
        return $this->getURL().self::TEMPLATE_ASSETS_DIR."/";
    }
    
    /**
     * Get the path to the template file for this template. If no template file is set, or the template file can not be found in the template directory
     * an exception is thrown.
     *
     * @see \Core\Library\Http\Template::getTemplateDir()
     * 
     * @return string The path for this templates's template file.
     */
    public function getTemplatePath(){
        
        $templateFile = $this->getTemplate();
        if(empty($templateFile)){
            throw new \Exception("You must set a template file before getting the path to it. Use ".get_class($this)."::setFile() to set the template file to use.");
        }
        $templateDir = $this->getTemplateDir();
        if(file_exists($templateDir.$templateFile)){
            return $templateDir.$templateFile;
        }else{
            throw new \Exception("The template file could not be located at '".$templateDir.$templateFile."'. If the template file is in a different location, use ".get_class($this)."::setTemplateDir() to set the directory of the template file.");
        }

    }
    
    /**
     * Sets the template file name that should be used to generate this pages portion of the output. This file should reside in the template path
     * returned by $this->getTemplateDir()
     *
     * @see \Core\Libray\Http\Template::getTemplateDir()
     * 
     * @return \Core\Library\Http\Template Return value is $this for chainability
     * @param string $template The template file that should be used.
     */
    public function setFile($template){
        $this->template = $template;
        return $this;
    }
    
    /**
     * Get the template file name for this template object
     * @return string The file name of the template for this object
     */
    public function getTemplate(){
        return $this->template;
    }
    
    /**
     * Get the HTML output of this template
     * @return string HTML output of this template
     */
    public function getHTML(){
        $file = $this->getTemplatePath();
        ob_start();
        include_once($file);
        $response = ob_get_contents();
        ob_end_clean();
        return $response;
    }
    
    /**
     * Convert the object into an array with the important properties
     * @return array An array representation of this object
     */
    public function toArray(){
        $data = array();
        $data['file'] = $this->getTemplatePath();
        $data['theme'] = $this->getTheme();
        $data['vars'] = $this->templateVars;
        return $data;
    }
    
}