<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Http;
use Core;
/**
 * This is the main class of the framework.
 *
 * It should be included in the applications index.php file. It is responsible for initializing up the phpStack framework
 *
 * <b>Events</b>
 *
 * afterFrameworkInit       : triggered after the framework is ready, during the {@see \Core\phpStack::run()} method
 *
 *
 * @package \Core\Library\Http\Action
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Action extends Core\Library\BaseObject{
    
    /**
     * The page object for this action.
     * @var \Core\Module\Base\Anonymous\Page\Page
     */
    private $page=null;
    
    /**
     * The method to execute for this action
     * @var string
     */
    private $method=null;
    
    
    /**
     * This method will load the page and execute the action requestd by the route, returning any return value
     *
     * @return mixed The return value of the executed action.
     */
    public function execute(){
        if(is_callable(array($this->page, $this->method))){
            return call_user_func_array(array($this->page, $this->method), array());
        }
    }
    
    /**
     * Set the page for this action
     *
     * Returns self for chainablity
     *
     * @return \Core\Libray\Http\Action
     * @param mixed $page The page object to set
     */
    public function setPage($page){
        $this->page = $page;
        return $this;
    }
    
    /**
     * Set the page's method for this action
     *
     * Returns self for chainablity
     *
     * @return \Core\Libray\Http\Action
     * @param string $method The page's method to set
     */
    public function setMethod($method){
        $this->method = $method;
        return $this;
    }
    
    /**
     * Get the page for this action
     * @return mixed|null The page object for this action or null if one can't be determined
     */
    public function getPage(){
        if(empty($this->page)){
            $this->loadRoute();
        }
        return $this->page;
    }
    
    /**
     * Get the page's method for this action
     * @return string The page's method for this action, or null if one can't be determined
     */
    public function getMethod(){
        if(empty($this->method)){
            $this->loadRoute();
        }
        return $this->method;
    }
    
    /**
     * Convert the object into an array with the important properties
     * @return array An array representation of this object
     */
    public function toArray(){
        $data = array(
            'page'=>get_Class($this->getPage()),
            'method'=>$this->getMethod()
        );
        
        return $data;
    }
}
?>