<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Http;
use Core;
/**
 * Class handles converting page/method to URL, and URL to page/method
 *
 * @package \Core\Library\Http\URL
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Route extends Core\Library\BaseObject{
    /**
     * Constant defining the page to use when a route can't be found
     * @var string
     */
    const NOT_FOUND_CONTROLLER='Core\Module\Error\Anonymous\Page\NotFound';
    
    /**
     * Constant defining the method to use from NOT_FOUND_URI when a route can't be found
     * @var string
     */
    const NOT_FOUND_METHOD='index';
    /**
     * Constant defining the page to use when access to a route is denied
     * @var string
     */
    const ACCESS_DENIED_CONTROLLER='Core\Module\Error\Anonymous\Page\AccessDenied';
    
    /**
     * Constant defining the method to use from ACCESS_DENIED_CONTROLLER when access to a route is denied
     * @var string
     */
    const ACCESS_DENIED_METHOD='index';
    
    /**
     * Constant defining the page to use when access to a route is denied
     * @var string
     */
    const LOGIN_CONTROLLER='Core\Module\Admin\User\Anonymous\Page\User';
    
    /**
     * Constant defining the method to use from ACCESS_DENIED_CONTROLLER when access to a route is denied
     * @var string
     */
    const LOGIN_METHOD='login';
    
    /**
     * Constant defining the page to use when access to a route is denied
     * @var string
     */
    const PASSWORD_CHANGE_CONTROLLER='Core\Module\Admin\User\Anonymous\Page\User';
    
    /**
     * Constant defining the method to use from ACCESS_DENIED_CONTROLLER when access to a route is denied
     * @var string
     */
    const PASSWORD_CHANGE_METHOD='changePassword';
    
    /**
     * The name of the URI to route
     * @var string
     */
    public $uri=null;
    
    /**
     * The Record object for this URI and requestType
     * @var \Core\Library\Database\Record
     */
    public $record=null;
    
    /**
     * The request type of the URI to route ('GET', 'POST', 'PUT', etc.)
     * @var string
     */
    public $requestType=null;
    
    /**
     * The name of the route table
     * @var string
     */
    public $routeTable='route';
    
    /**
     * The model for the route table. set in {@see \Core\Library\Http\Route::init()}
     * @var string
     */
    public $routeModel=null;
    
    /**
     * The model for the layout_to_route table. set in {@see \Core\Library\Http\Route::init()}
     * @var string
     */
    public $layoutToRouteModel=null;
    
    /**
     * The name of the route table
     * @var string
     */
    public $routeActionTable='route_action';
    
    /**
     * The name of the layout_to_route table
     * @var string
     */
    public $layoutToRouteTable='layout_to_route';
    
    /**
     * The model for the route_action table. set in {@see \Core\Library\Http\Route::__construct()}
     * @var string
     */
    public $routeActionModel=null;
    
    /**
     * Create a new instance of this class
     * @return \Core\Library\Http\Route
     * @param string $uri The URI for this route
     * @param string $requestType The the request type for this route ('GET', 'POST', 'PUT', etc.)
     */
    public function __construct($uri, $requestType){
        $this->uri = $uri;
        $this->requestType = $requestType;
    }
    
    /**
     * Initialize the {@see \Core\Library\Http\Route::routeModel} and {@see \Core\Library\Http\Route::routeActionModel}
     * @return null
     */
    public function init(){
        $config = $this->getConfig();
        $config = $this->load->library('ModuleConfig', array(null, $config));
        $this->routeModel = $this->load->moduleClass('Model', array($this->routeTable, null, $config), Core\Library\Loader::NAMESPACE_APPLICATION_CORE, 'Base', Core\Library\Loader::NAMESPACE_TYPE_MODEL);
        $this->routeActionModel = $this->load->moduleClass('Model', array($this->routeActionTable, null, $config), Core\Library\Loader::NAMESPACE_APPLICATION_CORE, 'Base', Core\Library\Loader::NAMESPACE_TYPE_MODEL);
        $this->layoutToRouteModel = $this->load->library('Layout\Layout')->getLayoutToRouteModel();
    }
    
    /**
     * Get the config array needed to construct a {@see \Core\Module\Base\Anonymous\Model\Model} object
     * @return array An array holding the config needed to create a {@see \Core\Module\Base\Anonymous\Model\Model} object
     */
    private function getConfig(){
        $config = array(
        //############################################## TABLES ##############################################
            'tables'=>array(
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ route ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                'route'=>array(
                    'name'      => 'route',
                    'indexes'   => array(
                        'uri_method_unique' => array(
                            'name' => 'uri_method_unique',
                            'unique' => true,
                            'type'=> 'BTREE',
                            'columns'=> array('uri','type')
                        )
                    ),
                    'relations' => array(
                        array(
                            'name'          => 'route_to_route_parameter',
                            'table'         => 'route_parameter',
                            'local_field'   => 'id',
                            'remote_field'  => 'route_id',
                        ),
                        array(
                            'name'          => 'route_to_route_action',
                            'table'         => 'route_action',
                            'local_field'   => 'route_action_id',
                            'remote_field'  => 'id',
                        ),
                    ),
                    'fields'                => array(
                        'id'                => array('name' => 'id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => true,'primary' => true,'unique' => true),
                        'uri'               => array('name' => 'uri','datatype' => 'varchar','size' => '255','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'route_action_id'   => array('name' => 'route_action_id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'type'              => array('name' => 'type','datatype' => 'enum','size' => 'GET,PUT,POST,DELETE,PATCH','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'redirect_route_id' => array('name' => 'redirect_route_id','datatype' => 'int','size' => '11','null' => true,'auto_inc' => false,'primary' => false,'unique' => false)
                    )
                ),
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ route_parameter ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                'route_parameter' => array(
                    'name'      => 'route_parameter',
                    'indexes'   => array(),
                    'relations' => array(
                        array(
                            'name'          => 'route_parameter_to_route',
                            'table'         => 'route',
                            'local_field'   => 'route_id',
                            'remote_field'  => 'id',
                        )
                    ),
                    'fields'    =>array(
                        'id'            => array('name' => 'id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => true,'primary' => true,'unique' => true),
                        'route_id'      => array('name' => 'route_id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'parameter'     => array('name' => 'parameter','datatype' => 'varchar','size' => '255','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'value'         => array('name' => 'value','datatype' => 'varchar','size' => '255','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                    )
                ),
                'route_action' => array(
                    'name'      => 'route_action',
                    'indexes'   => array(),
                    'relations' => array(
                        array(
                            'name'          => 'route_action_to_route',
                            'table'         => 'route',
                            'local_field'   => 'id',
                            'remote_field'  => 'route_action_id',
                        )
                    ),
                    'fields'    =>array(
                        'id'            => array('name' => 'id','datatype' => 'int','size' => '11','null' => false,'auto_inc' => true,'primary' => true,'unique' => true),
                        'page'    => array('name' => 'page','datatype' => 'varchar','size' => '255','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                        'method'        => array('name' => 'method','datatype' => 'varchar','size' => '255','null' => false,'auto_inc' => false,'primary' => false,'unique' => false),
                    )
                ),
            )
        );
        return $config;
    }
    
    /**
     * Method creates the appropriate Action object and returns it for processing
     *
     * If $route cannot be found the action will be for self::NOT_FOUND_CONTROLLER self::NOT_FOUND_METHOD
     * If $this->uri does NOT need authentication, then the action for the $route is returned
     *
     * If $this->uri DOES need authentication, and the user is NOT logged in, the action will be for self::LOGIN_CONTROLLER self::LOGIN_METHOD
     * If $this->uri DOES need authentication, and the user IS logged in and:
     * 1. the user has permission for the route, then the action for $this->uri is returned
     * 2. the user does NOT have permission for $this->uri, then the action will be for self::ACCESS_DENIED_CONTROLLER self::ACCESS_DENIED_METHOD
     *      
     * This method is called by Request object's process() method.
     * @see \Core\Library\Http\Request::process()
     * @see \Core\Library\Http\Action
     *
     * @return \Core\Library\Http\Action The action to be executed
     */
    public function routeRequest(){
        
        if(!empty($this->uri)){
            if($this->requestType===null){
                $this->requestType = $this->request->server->REQUEST_METHOD;
            }
            $action = $this->getAction();
            if(!empty($action)){
                if($this->load->isAuthenticated(get_class($action->getPage()))){
                    if($this->user->isLoggedIn()){
                        if($this->user->hasPermission($action->getPage(), $action->getMethod())){
                            return $action;
                        }else{
                            $action->setPage($this->load->page(self::ACCESS_DENIED_CONTROLLER));
                            $action->setMethod(self::ACCESS_DENIED_METHOD);
                            return $action;
                        }
                    }else{
                        $action->setPage($this->load->page(self::LOGIN_CONTROLLER));
                        $action->setMethod(self::LOGIN_METHOD);
                        return $action;
                    }
                }else{
                    return $action;
                }
            }else{
                $action = $this->load->library('Http\Action');
                $action->setPage($this->load->page(self::NOT_FOUND_CONTROLLER));
                $action->setMethod(self::NOT_FOUND_METHOD);
                return $action;
            }
        }else{
            throw new \Exception("Can not load an empty route, unless a page and method have been set");
        }
        
    }
    
    /**
     * Query the table and get the record for the $uri
     *
     * Route will be found with or without a trailing slash
     * If the URI is not found in the route table, a record for the NOT_FOUND_CONTROLLER page will be returned for rendering.
     *
     * @return \Core\Library\Http\Action
     */
    public function getAction(){
        $record = $this->getRouteRecord();
        
        if(!empty($record)){
            $action = $this->load->library('Http\Action');
            $page = $record->getRelatedRecords('the_route')[0]->page;
            $page = $this->load->page($page);
            
            $action->setPage($page);
            $action->setMethod($record->getRelatedRecords('the_route')[0]->method);
            $page->setRoute($this);
            return $action;
        }else{
            return null;
        }
    }
    
    /**
     * Get the record from the database 'route' table for this route.
     * @return \Core\Library\Database\Record The record object for this route's URI and requestMethod
     * @param bool $loadRelationships (optional) Boolean indicating if the relationship data should be loaded. Defaults to true
     */
    public function getRouteRecord($loadRelationships=true){
        if(empty($this->record) || (!empty($this->record) && $loadRelationships==true && empty($this->record->relationships))){
            $query = $this->load->database('Query',array($this->routeModel->getTable()));
            $records = $query->type($query::QUERY_TYPE_SELECT)
                ->fields('*')
                ->where(array(
                    array('field'=>'uri','comparison'=>'=','value'=>rtrim(ltrim($this->uri,'/'),'/').'/'),
                    array('field'=>'type','comparison'=>'=','value'=>$this->requestType)
                    ), 'AND')
                ->orWhere(array(
                    array('field'=>'uri','comparison'=>'=','value'=>rtrim(ltrim($this->uri,'/'),'/')),
                    array('field'=>'type','comparison'=>'=','value'=>$this->requestType)
                    ), 'AND');
            if($loadRelationships){
                $query->join($this->routeActionModel->getTable(),$this->routeModel->getTable()->getField('route_action_id'), $this->routeActionModel->getTable()->getPrimaryField(),'the_route');
                $query->join($this->layoutToRouteModel->getTable(),$this->routeModel->getTable()->getPrimaryField(), $this->layoutToRouteModel->getTable()->getField('route_id'),'layout_ids');
            }
            $records = $query->execute($loadRelationships);
            if($records->numRecords() > 0){
                $this->record = $records->current();
            }else{
                return null;
            }
        }
        return $this->record;
    }
    
    /**
     * Get a page URI from an {@see \Core\Library\Http\Action} object
     * @return string The URI for the page
     * @param \Core\Library\Http\Action The action object to convert into a URI
     * @param string $requestMethod The request method ({@see \Core\Library\Http\Request::request) used to call the URI ('GET', 'POST', 'PUT', etc.)
     */
    public function getURIFromAction($action, $requestMethod){
        $pageName = get_class($action->getPage());
        $pageName = (substr($pageName,-5) == 'Trace' ? substr($pageName,0, -5) : $pageName);
        $query = $this->load->database('Query',array($this->routeActionModel->getTable()));
        $records = $query->type($query::QUERY_TYPE_SELECT)
            ->fields('*')
            ->join($this->routeModel->getTable(),$this->routeActionModel->getTable()->getPrimaryField(), $this->routeModel->getTable()->getField('route_action_id'),'the_route')
            ->where(array(
                array('field'=>'page','comparison'=>'=','value'=>$pageName),
                array('field'=>'method','comparison'=>'=','value'=>$action->getMethod()),
                array('field'=>$this->routeModel->getTable()->getField('type'), 'comparison'=>'=', 'value'=>$requestMethod)
                ), 'AND')
            ->execute(true);
        if($records->numRecords() == 1){
            $record = $records->current();
            $related = $record->getRelatedRecords('the_route');
            if(count($related) > 0){
                $related = array_shift($related);
                return '/'.rtrim(ltrim($related->uri,'/'),'/').'/';
            }else{
                return null;
            }
            
        }else{
            return null;
        }
    }
    
    /**
     * Get the URI for this route
     * @return string The URI this route represents
     */
    public function getURI(){
        return $this->uri;
    }
}
?>