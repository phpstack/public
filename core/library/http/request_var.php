<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library\Http;
use Core;
/**
 * Generic class to provide OOP access to the PHP variable $_GET, $_REQUEST, $_POST, $_SERVER, $_COOKIE
 *
 * @package \Core\Library\Cache
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class RequestVar extends Core\Library\BaseObject{
    
    /**
     * The data that this class is representing
     * @var array
     */
    public $data;
    
    /**
     * Construct a new \Core\Library\Http\RequestVar object. Constructor expects that the data for this class is passed into it as an array
     * of $key=>$value pairs.
     *
     * @return \Core\Library\Http\RequestVar
     * @param array $data The data that this class represents
     */
    public function __construct(&$data){
        $this->data =& $data;
    }
    
    /**
     * Return an array representation of this object
     * @return array The key=>value array representation of this object.
     */
    public function toArray(){
        return $this->data;
    }
    
    /**
     * Magic method to allow ->property access to data variables
     *
     * @throws \Exception If accessing a property that doesn't exist.
     * 
     * @return mixed The value of the requested property
     * @param string $name The name of the property
     */
    public function __get($name){
        if(!array_key_exists($name, $this->data)){
            return null;
        }else{
            return $this->data[$name];
        }
    }
    
    /**
     * Magic method to allow ->property access to data variables
     *
     * 
     * @return null
     * @param string $name The name of the property
     * @param mixed $value The value to set $name to
     */
    public function __set($name, $value){
        $this->data[$name] = $value;
    }
    
    /**
     * Magic method to allow checking if a property is set
     * @return bool
     * @param string $name The name of the property to check
     */
    public function __isset($name){
        return array_key_exists($name,$this->data);
    }
    
    /**
     * Magic method to allow unsetting a property
     * @return null
     * @param string $name The name of the property to unset
     */
    public function __unset($name){
        if(array_key_exists($name,$this->data)){
            unset($this->data[$name]);
        }
    }
}
?>