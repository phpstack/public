<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library;
/**
 * Class handles the caching of information to the filesystem.
 *
 * Caches are created and accessed using as a key/value pair. This class does not automatically invalidate caches. In order to
 * remain lightweight, the filesystem is not accessed until a specific need arises, and filesystem access is kept to a minimum.
 *
 * The cache creation time is tracked along with the cached data, which can be helpful in invalidating caches. When an existing cache
 * key is set, the creation time is updated to the last setting of the key.
 * Cached data is automatically serialized/unserialized when necessary.
 * 
 *
 * @package \Core\Library\Cache
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Cache extends BaseObject{
    /**
     * The full path to the directory in which cache files are stored. This is set in the constructor
     * @var string
     */
    private $cacheDir;
    /**
     * Storage of meta information relating to different cache keys
     * @var \Core\Library\Registry
     */
    private $cacheMeta=array();
    
    /**
     * Creates a new instance of the Cache class.
     * The $cacheDir property is determined and set
     * If the $cacheDir directory does not exist, it will be created
     *
     * @return \Core\Library\Cache
     */
    public function __construct(){
        $this->cacheDir = CORE_DIR_STORAGE.'cache'.DIRECTORY_SEPARATOR;
        if(!is_dir($this->cacheDir)){
            mkdir($this->cacheDir, 0775, true);
        }
    }

    /**
     * Returns the cache stored at $key. Data will be returned unserialized.
     * If $key does not exist the function will return null
     *
     * @return mixed The value relating to the $key
     * @param string $key The key for the cache to be returned
     */
    public function get($key){
        $meta = $this->getCacheMeta($this->encodeKey($key));
        if(!empty($meta)){

            //If this cache file has not already been read and processed
            if($meta['data']!==false){
                return $meta['data'];
            }else{
              //Read in the cache, and return the value
              if(!empty($meta['path']) && file_exists($meta['path'])){
                $content = file_get_contents($meta['path']);

                if($meta['serialized']){
                    $content = unserialize($content);
                }
                $meta['data'] = $content;
                $this->setCacheMeta($key, $meta);
                return $content;
              }else{
                return null;
              }
            }
        }else{
            return null;
        }
    }

    /**
     * Will serialize (if needed) and store $value with the key $key
     * Boolean values, Objects, and Arrays will be serialized before storage
     * Note: If a serialized string is passed to $value, retrieval of the value will be returned unserialized
     * Note: The created date will be updated if the cache already exists
     *
     * @return null
     * @param string $key The key for accessing the data in $value
     * @param mixed $value The value to be cached.
     */
    public function set($key, $value){
        if(is_bool($value) || is_object($value) || is_array($value)){
            $isSerialized = '1';
        }else{
            if($value != 'b:0' && (is_string($value) || is_int($value) || is_bool($value))){
                $isSerialized = '0';
            }else{
                $isSerialized = '1';
            }
        }
        $encodedKey = $this->encodeKey($key);
        $date = time();
        $storedValue = ($isSerialized=='1' ? serialize($value) : $value);
        $meta = array(
            'key'=>$encodedKey,
            'created'=>$date,
            'serialized'=>(bool)$isSerialized,
            'path'=>$this->cacheDir.$encodedKey.'.'.$isSerialized.'.'.$date.'.cache',
            'data'=>$value
        );
        $existingMeta = $this->getCacheMeta($key);
        if($existingMeta !== null){
            //cache meta data already exists, so let's use the existing file path
            $meta['path'] = $existingMeta['path'];
        }
        $this->setCacheMeta($key,$meta);
        file_put_contents($meta['path'],$storedValue);
    }

    /**
     * returns boolean indicating if the cache exists
     *
     * @return bool
     * @param string $key The cache key to determine if existing
     */
    public function exists($key){
        $meta = $this->getCacheMeta($key);
        if($meta === null){
            //The cache meta data does not exist
            return false;
        }else{
            //We have meta data for the cache, does the file exist?
            if(!empty($meta['path']) && file_exists($meta['path'])){
                return true;
            }else{
                return false;
            }
        }
    }

    /**
     * Deletes an existing cache. Cache meta information AND associated file will be removed.
     * Deletion is permenant.
     * If $key doesn't exist nothing is done.
     *
     * @return null;
     * @param string $key The key for the cache to be deleted
     */
    public function delete($key){
        $meta = $this->getCacheMeta($key);
        if($meta !== null && array_key_exists('path',$meta) && file_exists($meta['path'])){
            unlink($meta['path']);
            $this->unsetCacheMeta($key);
        }
    }
    
    /**
     * Returns the age (in seconds) of the cached data, or null if $key is not a valid cache key
     *
     * @return int|null
     * @param string $key The cache key for which to determine the age
     */
    public function getAge($key){
        $date = $this->getCreatedDate($key);
        
        if($date !== null){
           return time()-$date;
        }else{
            return null;
        }
    }
    
    /**
     * Gets the unix timestamp that the cache for $key was created. Returns null if $key is not a valid cache
     * Note: A caches creation date will be updated each time the $key is set, so the return value of this function will
     * represent the last time that the cache at $key was written.
     *
     * @return int|null
     * @param string $key The cache key for which to determine the creation date
     */
    public function getCreatedDate($key){
        $meta = $this->getCacheMeta($key);
        if($meta !== null && array_key_exists('created',$meta) && strlen($meta['created']) > 0){
            return $meta['created'];
        }else{
            return null;
        }
    }

    /**
     * Load cache meta data for all existing caches
     *
     * @return null
     */
    private function loadCacheMeta(){
        //Meta is in the form [key].[isSerialized].[dateCreated].cache
        $this->cacheMeta = array();
        $files = glob($this->cacheDir.DIRECTORY_SEPARATOR.'*.cache');
        foreach($files as $file){
            $parts = explode('.',basename($file));
            $key = array_shift($parts);
            $isSerialized = array_shift($parts);
            $created = array_shift($parts);
            if(array_key_exists($key,$this->cacheMeta)){
                //If we alread have this key, the keep the newest cache, and remove the rest.
                if($this->cacheMeta[$key]['created'] >= $created){
                    //The existing cache file is newer
                    unlink($file);
                }else{
                    unlink($this->cacheMeta[$key]['path']);
                    $this->cacheMeta[$key] = array(
                        'key'=>$key,
                        'created'=>$created,
                        'serialized'=>(bool)$isSerialized,
                        'path'=>$file,
                        'data'=>false
                    );
                }
            }else{
                $this->cacheMeta[$key] = array(
                    'key'=>$key,
                    'created'=>$created,
                    'serialized'=>(bool)$isSerialized,
                    'path'=>$file,
                    'data'=>false
                );
            }
        }
    }

    /**
     * Encode the key to meet storage requirements.
     * Forward slashes (/), periods (.), and directory separators are removed from the key
     *
     * @return string The updated/cleaned $key
     * @param string $key The key that is to be cleaned for use.
     */
    private function encodeKey($key){
        $cleanKey = str_replace(array('/','.',DIRECTORY_SEPARATOR),array('','',''),$key);  //Remove / and . from the key
        return $cleanKey;
    }

    /**
     *Returns the meta data for the given cache key. If cache does not exist null is returned
     *
     * @return array|null The meta data for $key, or null if the key is not valid.
     * @param string $key The key for which to retrieve meta data
     */
    private function getCacheMeta($key){
        $key = $this->encodeKey($key);
        if(empty($this->cacheMeta) || !array_key_exists($key,$this->cacheMeta)){
            $this->loadCacheMeta();
        }
        if(array_key_exists($key,$this->cacheMeta)){
            return $this->cacheMeta[$key];
        }else{
            return null;
        }
    }

    /**
     * Set the content for the cahe key in the meta array. All meta data will be overwritten with $meta
     *
     * @return null
     * @param string $key The key for which to set the meta data
     * @param array $meta The meta data to set for $key
     */
    private function setCacheMeta($key, $meta){
        $key = $this->encodeKey($key);
        $this->cacheMeta[$key] = $meta;
    }

    /**
     * Remove the meta data from the cacheMeta array.
     * Note: Cache file is NOT removed by this method.
     *
     * @return null
     * @param string $key The key for which to remove the meta data.
     */
    private function unsetCacheMeta($key){
        if(array_key_exists($this->encodeKey($key),$this->cacheMeta)){
            unset($this->cacheMeta);
        }
    }
    
    public function getCacheFile($key){
        $meta = $this->getCacheMeta($key);
        if(!empty($meta)){
            return $meta['path'];
        }else{
            return null;
        }
    }
}
?>