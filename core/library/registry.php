<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library;
/**
 * Registry is used to hold objects that should be globally accessible to other classes
 *
 * This object is created in startup.php, and is passed to most objects automatically by the loader.
 * An object or parameter can be added to the registry, and will then be available to any object which
 * accepts an {@see \Core\Library\Registry} object as a parameter.
 *
 * @package \Core\Library\Registry
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 * 
 */
class Registry{
    /**
     * The storage array for the properties of the Registry
     * @var array
     */
    private $data=array();
    /**
     * Adds a property to the Registry class
     *
     * @return null
     * @param string $name The name of the property to set
     * @param mixed $value The value of the property to set
     */
    public function set($name, $value){
        $this->data[$name] = $value;
    }
    
    /**
     * Get a property from the registry
     *
     * @see \Core\Library\Registry::__get()
     *
     * @return mixed The value of the property requested
     * @param string $name The name of the property to get
     * 
     */
    public function get($name){
        if(array_key_exists($name,$this->data)){
            return $this->data[$name];
        }else{
            return null;
        }
    }
    
    /**
     * Magic Method used to allow -> nomanclature for access to registry values and objects
     *
     * @see \Core\Library\Registry::get()
     *
     * @return mixed
     * @param string $name The name of the property to get
     */
    public function __get($name){
        return $this->get($name);
    }
    
    /**
     * Magic Method used to allow -> nomanclature for access to registry values and objects
     *
     * @see \Core\Library\Registry::set()
     *
     * @return mixed
     * @param string $name The name of the property to get
     */
    public function __set($name, $value){
        return $this->set($name,$value);
    }
    
    /**
     * Removes all data from the Registry and returns it in an array of properties indexed by the property name
     *
     * This method is used in {@see Core\Library\Utility::print_array} to empty the object for printing
     *
     * @return array An array of properties indexed by their name.
     */
    public function unload(){
        $temp = $this->data;
        $this->data = array();
        return $temp;
    }
    
    /**
     * Load the properties of the Registry object from an array indexed by the property names
     *
     * This method is used in {@see Core\Library\Utility::print_array} to restore the object after printing
     *
     * @return null
     * @param array $data An array of properties to add to the Registry, indexed by the property names
     * 
     */
    public function load($data){
        $this->data = $data;
    }
}
?>