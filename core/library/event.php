<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library;
/**
 * Class manages the framework events. Class is part of the base objects in the registry, and is therefore available to all classes
 * which contain the registry.
 *
 * Events can be registered, and executed from anywhere in the system, allowing modules, and libraries to register events, and trigger
 * them as necessary. Events can not be triggered unless they are first registered. It is the job of the registering code to trigger an
 * event at the appropriate time. If a callback for an event in a particular namespace has already been triggerd, then that callback
 * WILL NOT be triggered again by triggering the event again.
 *
 * An event does not return any value, but can accept any number of parameters. Each event will be passed, at a minimum, the global
 * registry class, it is therefore required that an event accept at least one parameter, which is the global registry class.
 *
 * Events are registered in either the 'global' (default) context, or any other user defined context. An event can be scheduled as frequently
 * as needed. 
 *
 * @package \Core\Library\Event
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Event extends BaseObject{
    /**
     * A constant to define the "SELECT" type query
     * @var string
     */
    const CONTEXT_GLOBAL='global';
    
    /**
     * Storage of the events and their related functions
     * @var array
     */
    private $events=array();
    
    /**
     * This is used to map specific events to their location in the events array
     * @var array
     */
    private $idMap=array();
    
    
    /**
     * Register a new event. Events must be registered before scheduling or triggering. Each event can be registered in a specific
     * $context. A context is sort of like a namespace in that it is just an organization system, that allows events of the same
     * name to be registered in different contexts. If no context is provided, the event will be registered in the global context
     * An event can only be registered once for each context.
     *
     * @throws \Exception If the event has already been registered
     * @return null
     * @param string $event The name of the event to register
     * @param string $context The context for the event. Defaults to the global context
     */
    public function registerEvent($event, $context=null){
        if($context === null){
            $context = self::CONTEXT_GLOBAL;
        }
        if(!$this->contextExists($context)){
            $this->events[$context] = array(
                'context'=>$context,
                'events'=>array()
            );
        }
        if(!$this->eventExists($event, $context)){
            $this->events[$context]['events'][$event] = array();
        }
        
    }
    
    /**
     * Method will schedule a $callable function to be run when $event is triggered. Events can not be scheduled or triggered until
     * they have been registered with the {@see \Core\Library\Event\registerEvent()} method.
     *
     * $callable must be a callable function or method, which accepts at a minimum 1 paramter which is the global {@see \Core\Library\Registry}
     * object, which is always the first parameter in the callback. If $params is included when scheduling an event, then the callable
     * must accept each of the parameters defined in $params. If a parameter in $params is an object, then you must either include the
     * object, or the full namespaced name;
     *
     * If the event registration is successful, then a specific callback ID is returned. This ID can be used to reference the specific
     * callback independant of the context.
     *
     * @throws \Exception If the $event has not been registered, or the $callable is not callable.
     *
     * @return null
     * @param string $event The name of the event
     * @param string $context (optional) The context in which to schedule the event.
     * @param string|callable $callback The callable to be executed when the event is triggered.
     * @param array $params (optional) An array of parameters to be passed to the $callable
     */
    public function scheduleEvent($event, $context=null, $callback, $params=array()){
        if($context === null){
            $context = self::CONTEXT_GLOBAL;
        }
        
        if(!$this->contextExists($context)){
            throw new \Exception("The context '".$context."' is not a registered");
        }elseif(!$this->eventExists($event,$context)){
            throw new \Exception("The event '".$event."' is not a registered event in the '".$context."' context.");
        }elseif(!is_callable($callback)){
            throw new \Exception("Can not schedule the event '".$event."' in the '".$context."' context because the callback is not callable.");
        }
        $this->events[$context]['events'][$event][] = array();
        
        end($this->events[$context]['events'][$event]);
        $new = key($this->events[$context]['events'][$event]);
        $callbackId = md5($context.$event.$new);
        $this->events[$context]['events'][$event][$new] = array(
            'id' => $callbackId,
            'callback'=>$callback,
            'params'=>$params,
            'triggered'=>false
        );
        
        $this->idMap[$callbackId]=array(
            'context'=>$context,
            'event'=>$event,
            'callaback'=>$new
        );
        return $callbackId;
    }
    
    /**
     * Cancels an event that has been previously scheduled. If the event has already been triggered, this method will do nothing.
     * Events from the 'global' context CAN NOT be cancelled. 
     *
     * @return null
     * @param string $callbackId The callback id returned when the event was scheduled.
     */
    public function cancelCallback($callbackId){
        if($this->eventIdExists($callbackId)){
            $eventData = $this->events[$this->idMap[$callbackId]['context']]['events'][$this->idMap[$callbackId]['event']][$this->idMap[$callbackId]['callaback']];
            if($eventData['context'] != self::CONTEXT_GLOBAL){
                throw new \Exception("Events in the '".self::CONTEXT_GLOBAL."' context can not be cancelled.");
            }else{
                unset($this->events[$context]['events'][$event]);
            }
        }
    }
    
    /**
     * Method will trigger the specified event. All registered functions for the event will be executed, in the order which they
     * were registered, passing the callable the {@see \Core\Library\Registry} object, and any parameters defined during scheduling.
     *
     * @throws \Exception If the event has not been registered.
     *
     * @return null
     * @param string $event The name of the event to trigger
     * @param string $context The context that the $event is scheduled in.
     */
    public function trigger($event, $context=null){
        if($context === null){
            $context = self::CONTEXT_GLOBAL;
        }
        
        if($this->eventExists($event, $context)){
            foreach($this->events[$context]['events'][$event] as $eIndex=>$e){
                $this->triggerCallback(md5($context.$event.$eIndex));
            }
        }else{
            throw new \Exception("Unrecognized event scheduled for '".$event."' in the '".$context."' context. Events must be of the type string or a callable.");
        }
    }
    
    /**
     * Trigger a specific callback using it's callbackId
     *
     * @return null
     * @param string $callbackId
     */
    public function triggerCallback($callbackId){
       if($this->eventIdExists($callbackId)){
            if(!$this->hasEventIdTriggered($callbackId)){
                $eventData = $this->events[$this->idMap[$callbackId]['context']]['events'][$this->idMap[$callbackId]['event']][$this->idMap[$callbackId]['callaback']];
                if(is_callable($eventData['callback'])){
                    
                    $params = $eventData['params'];
                    array_unshift($params, $this->registry);
                    $this->events[$this->idMap[$callbackId]['context']]['events'][$this->idMap[$callbackId]['event']][$this->idMap[$callbackId]['callaback']]['triggered'] = true;
                    call_user_func_array($eventData['callback'], $params);
                }else{
                    throw new \Exception("Unrecognized event scheduled for '".$eventData['event']."' in the '".$eventData['context']."' context. Events must be of the type string or a callable.");
                }
            }
       }else{
            throw new \Exception("Callback ID '".$callbackId."' is not valid");
       }
    }
    
    /**
     * Determines if a specified $context exists
     *
     * @return bool
     * @param string $context The name of the context to check
     */
    public function contextExists($context){
        if(array_key_exists($context, $this->events)){
            return true;
        }
        return false;
    }
    
    /**
     * Determines if a specified $event exists in the $context. If context is not provided, the global context will be checked.
     * If the context, or the event does not exist false will be returned.
     *
     * @return bool
     * @param string $event The name of the event to check
     * @param string $context The name of the context to check
     */
    public function eventExists($event, $context=null){
        if($context === null){
            $context = self::CONTEXT_GLOBAL;
        }
        if($this->contextExists($context) && array_key_exists($event, $this->events[$context]['events'])){
            return true;
        }
        return false;
    }
    
    /**
     * Check if an event exists using it's unique id
     *
     * @return bool
     * @param string $eventId;
     */
    public function eventIdExists($callbackId){
        if(array_key_exists($callbackId, $this->idMap)){
            return true;
        }
        return false;
    }
    
    /**
     * Determines if a particular event in a given context has already been triggered. If the context, or the event has not been
     * created or scheduled, false will be returned.
     *
     * @return bool
     * @param string $event The name of the event to check
     * @param string $context (optional) The name of the context to check in
     */
    public function hasEventTriggered($event, $context=null){
        if($context === null){
            $context = self::CONTEXT_GLOBAL;
        }
        if(!$this->eventExists($event, $context) || !$this->events[$context]['events'][$event][$eventIndex]['triggered']){
            return false;
        }
        return true;
    }
    
    /**
     * Determines if a particular event in a given has already been triggered using the callback id. If the context, or the
     * event has not been created or scheduled, false will be returned.
     *
     * @return bool
     * @param string $callbackId The callback ID
     */
    public function hasEventIdTriggered($callbackId){
        if($this->eventIdExists($callbackId)){
            $eventData = $this->events[$this->idMap[$callbackId]['context']]['events'][$this->idMap[$callbackId]['event']][$this->idMap[$callbackId]['callaback']];
            return $eventData['triggered'];
        }
        return false;
    }
}

?>