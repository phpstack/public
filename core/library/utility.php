<?php
 /**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Library;
/**
 * Utility is a class intended to hold framework specific utility functions which are helpful to developers
 *
 * This class defines functions that are helpful in the development of other framework elements.
 * 
 * @package \Core\Library\Utility
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 * 
 */
class Utility extends BaseObject{
    
    /**
     * Constructor. Attempts to place this classes methods into the global namespace
     *
     * @param \Core\Library\Registry $registry the global registry object. Included by the loader class
     * @return \Core\Library\Utility
     */
    public function __construct(){
        //$this->registry = $registry;
        
        
    }
    
    /**
     * Takes an object or an array and outputs it to the screen wrapped in <pre> tags using print_r
     * @static
     * 
     * @param mixed $object The object to be printed
     * @param bool $removeRegistry Boolean indicates if the registry object should be printed when encountered
     * @return null
     */
    public function print_array($object, $removeRegistry=true){
        $reload=false;
        if($removeRegistry && is_object($this->registry) && get_class($this->registry) == '\Core\Library\Registry'){
            //Unload the registry, so print_r doesn't show all it's data
            //Since all objects are passed by reference, this will clear data from all objects
            $regd = $this->registry->unload();
            $reload=true;
        }
        print '<pre style="text-align:left;">'.print_r($object,true).'</pre>';
        if($reload){
            //Place the registry back the way it was.
            $this->registry->load($regd);
        }
    }
    
    /**
     * Returns an array containing a modified debug_backtrace
     * @static
     *
     * @returns array An array containing backtrace information
     *
     */
    public function trace(){
        $trace = debug_backtrace();
        foreach($trace as &$t){
            unset($t['args']);
            unset($t['object']);
        }
        return $trace;
    }
    
    /**
     * Remove all comments from PHP code
     * @return string The PHP code with all the comment sections removed
     * @param string $code The PHP code from which to remove the comments.
     */
    public function removeComments($code){
        $commentTokens = array(T_COMMENT);

        if (defined('T_DOC_COMMENT'))
            $commentTokens[] = T_DOC_COMMENT; // PHP 5
        if (defined('T_ML_COMMENT'))
            $commentTokens[] = T_ML_COMMENT;  // PHP 4
        
        $tokens = token_get_all($code);
        $newStr='';
        foreach ($tokens as $token) {    
            if (is_array($token)) {
                if (in_array($token[0], $commentTokens))
                    continue;
        
                $token = $token[1];
            }
        
            $newStr .= $token;
        }
        return $newStr;
    }
}
?>