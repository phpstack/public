<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Module\Base\Anonymous\Page;
use Core;
/**
 * The generic page class. Each page in the application should extend this class. Each page in the application is
 * also responsible for generating it's particular part of the response object
 *
 *
 * @package \Core\Module\Base\Anonymous\Page\Page
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Page extends Core\Library\BaseObject{
    /**
     * Storage for the \Core\Library\Layout\Layout object for this page
     * @var array
     */
    private $layout = null;
    
    /**
     * Storage for the \Core\Library\Http\Route object for this page
     * @var array
     */
    private $route = null;
    
    /**
     * Storage for the header \Core\Module\Base\Anonymous\Block\Block object for this page
     * @var \Core\Module\Base\Anonymous\Block\Block
     */
    private $header = null;
    
    /**
     * Storage for the footer \Core\Module\Base\Anonymous\Block\Block object for this page
     * @var \Core\Module\Base\Anonymous\Block\Block
     */
    private $footer = null;
    
    /**
     * Storage for the \Core\Library\Http\Template object for this page
     * @var \Core\Library\Http\Template
     */
    private $template = null;
    
    /**
     * Storage for scripts and CSS files added to this page before a header is set.
     * @var string[]
     */
    private $assets = array('scripts'=>array(),'styles'=>array());
    
    
    /**
     * Initalize this page object
     * @return null
     */
    public function init(){
        
    }
    
    /**
     * Set the route for this page. The $route parameter can either be Route object or a string. If it is a string it should be a valid URI
     *
     * @see \Core\Library\Http\Route
     * 
     * @return @return \Core\Library\Layout\Layout Return value is $this for chainability
     * @param \Core\Library\Http\Route|string Either the Route object for this page, or a URI string
     * @param string $requestType The type of request ('GET', 'POST', 'PUT', etc.). This parameter is not needed if $route is a Route object
     */
    public function setRoute($route, $requestType='GET'){
        if(is_object($route)){
            $this->route = $route;
        }else{
            $this->route = $this->load->library('Http\Route', array($route, $requestType));
        }
        $this->loadLayout();
        return $this;
    }
    
     /**
     * Set the header block for this page.
     *
     * @see \Core\Module\Base\Anonymous\Block\Block
     * 
     * @return \Core\Library\Layout\Layout Return value is $this for chainability
     * @param \Core\Module\Base\Anonymous\Block\Block The header block for this page
     */
    public function setHeader($headerBlock){
        if(is_object($headerBlock) && (get_class($headerBlock) == 'Core\Module\Base\Anonymous\Block\Block' || is_subclass_of($headerBlock, 'Core\Module\Base\Anonymous\Block\Block'))){
            $this->header = $headerBlock;
            if(!empty($this->assets['scripts'])){
                foreach($this->assets['scripts'] as $script){
                    $this->header->addScript($script);
                }
                $this->assets['scripts'] = array();
            }
            if(!empty($this->assets['styles'])){
                foreach($this->assets['styles'] as $style){
                    $this->header->addScript($style);
                }
                $this->assets['styles'] = array();
            }
        }else{
            throw new \Exception("The page header must be an object overloading the Core\Module\Base\Anonymous\Block\Block class");
        }
        return $this;
    }
    
    /**
     * Set the footer block for this page.
     *
     * @see \Core\Module\Base\Anonymous\Block\Block
     * 
     * @return \Core\Library\Layout\Layout Return value is $this for chainability
     * @param \Core\Module\Base\Anonymous\Block\Block The footer block for this page
     */
    public function setFooter($footerBlock){
        if(is_object($footerBlock) && (get_class($footerBlock) == 'Core\Module\Base\Anonymous\Block\Block' || is_subclass_of($footerBlock, 'Core\Module\Base\Anonymous\Block\Block'))){
            $this->footer = $footerBlock;
        }else{
            throw new \Exception("The page footer must be an object overloading the Core\Module\Base\Anonymous\Block\Block class");
        }
        return $this;
    }
    
    /**
     * Set the layout to be used to render this page
     * @return \Core\Library\Layout\Layout Return value is $this for chainability
     * @param \Core\Library\Layout\Layout $layout The layout object to be used to render this page.
     */
    public function setLayout($layout){
        if(is_object($layout) && (get_class($layout) == 'Core\Library\Layout\Layout' || is_subclass_of($layout, 'Core\Library\Layout\Layout'))){
            $this->layout = $layout;
            $this->layout->setPage($this);
        }else{
            throw new \Exception("The page layout must be an object of the type Core\Library\Layout\Layout or an object which overloads it");
        }
        return $this;
    }
    
    /**
     * Get the layout to be used to render this page
     * @return \Core\Library\Layout\Layout
     */
    public function getLayout(){
        return $this->layout;
    }
    
    /**
     * Load the layout defined for this page. If no layout can be located, then a layout with a single row and a single 12 column block will
     * be used. The loaded layout is returned
     *
     * @return \Core\Library\Layout\Layout The layout that was loaded
     */
    public function loadLayout(){
        $layout = $this->load->library('Layout\Layout')->setPage($this);
        $layoutIds = $this->route->getRouteRecord()->getRelatedRecords('layout_ids');
        if(count($layoutIds) == 1){
            
            $layoutId = array_shift($layoutIds);
            $layout->load($layoutId->getPrimaryFieldValue());
        }else{
            $layout->load();
        }
        $this->setLayout($layout);
        return $layout;
    }
    
    /**
     * Add a javascript file to this page. Javascript files are included in the order they are added. If the file begins with 'http://', 'https://' or '/',
     * then it is assumed that the file will be loaded from that URL. If only a file name is provided, then the file should be located in this module's
     * template directory under 'assets\js'.
     *
     * @return \Core\Module\Base\Anonymous\Block\Block Return value is $this for chainability
     * @param $url The name of the javascript file to include. File should be stored in the blocks 'assets\js\' directory
     */
    public function addScript($file){
        if(substr($file,0,1) == '/' || substr($file,0,7) == 'http://' || substr($file,0,8) == 'https://'){
            $this->response->addScript($file);
        }else{
            if(!empty($this->header)){
                $this->header->addScript($file);
            }else{
                $this->assets['scripts'][] = $file;
            }
        }
        return $this;
    }
    
     /**
     * Render this page and set the content to the Core\Library\Http\Response object
     *
     * @see \Core\Library\Http\Response
     *
     * @return null
     */
    protected function render(){
        if(empty($this->layout)){
            $this->loadLayout();
        }
        if(empty($this->header)){
            throw new \Exception("You must set a header block using ".get_class($this)."::setHeader() before the page can be rendered.");
        }
        
        $this->response->addOutput($this->header->getHTML().$this->layout->getHTML().$this->footer->getHTML());
    }
    
    
}
?>