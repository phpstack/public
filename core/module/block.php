<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Module\Base\Anonymous\Block;
use Core;
/**
 * The generic block class. Each block in the application should extend this class. Each block in the application is
 * also responsible for generating it's particular part of the response object
 *
 *
 * @package \Core\Module\Base\Anonymous\Block\Block
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Block extends Core\Library\BaseObject{
    /**
     * The template object used to render this block
     * @var \Core\Library\Http\Template
     */
    protected $template;
    
    /**
     * The page to which this block belongs. This must be passed when constructing this object
     * @var \Core\Module\Base\Anonymous\Page\Page
     */
    private $page = null;
    
    /**
     * Constructor
     * @return \Core\Module\Base\Anonymous\Block\Block
     * @param \Core\Module\Base\Anonymous\Page\Page $page The page object to which this block belongs
     */
    public function __construct($page){
        $this->page = $page;
    }
    
    /**
     * Initalize this page object
     * @return null
     */
    public function init(){
        $this->setTemplate($this->load->library('Http\Template', array($this)));
    }
    
    /**
     * Each page must have an index action
     *
     * @return \Core\Library\Http\Action|null
     */
    public function index(){

    }
    
    /**
     * Set the template object used to render this page
     *
     * @return \Core\Library\Layout\Layout Return value is $this for chainability
     * @param \Core\Library\Http\Template $template The template object used to render this page
     */
    public function setTemplate($template){
        if(is_object($template) && (get_class($template) == 'Core\Library\Http\Template' || is_subclass_of($template, 'Core\Library\Http\Template'))){
            $this->template = $template;
        }else{
            throw new \Exception("The page template must be an object of type Core\Library\Http\Template or an object which overloads it.");
        }
        return $this;
    }
    
    /**
     * Get the template object used to render this page
     *
     * @return \Core\Library\Http\Template The template object used to render this class
     */
    public function getTemplate(){
        return $this->template;
    }
    
    /**
     * Set a variable to be made available to the template
     * @return \Core\Module\Base\Anonymous\Block\Block Return value is $this for chainability
     * @param string $name The name of the variable to set
     * @param mixed $value The value of the view variable
     */
    public function setTemplateVar($name, $value){
        $this->template->setTemplateVar($name, $value);
        return $this;
    }
    
    /**
     * Get the value of a template variable
     * @return mixed
     * @param string $name The name of the variable to get
     */
    public function getTemplateVar($name){
        return $this->template->getTemplateVar($name);
    }
    
    /**
     * Set the file name for the template file used to render this block. This is an alias for Core\Library\Http\Template::setFile()
     *
     * @see \Core\Library\Http\Template::setFile()
     *
     * @return \Core\Module\Base\Anonymous\Block\Block Return value is $this for chainability
     */
    public function setTemplateFile($file){
        $this->template->setFile($file);
        return $this;
    }
    
    /**
     * Add a javascript file to this block. Javascript files are included in the order they are added. If the file begins with 'http://', 'https://' or '/',
     * then it is assumed that the file will be loaded from that URL. If only a file name is provided, then the file should be located in the template directory
     * under 'assets\js'.
     *
     * @return \Core\Module\Base\Anonymous\Block\Block Return value is $this for chainability
     * @param $url The name of the javascript file to include. File should be stored in the blocks 'assets\js\' directory
     */
    public function addScript($file){
        if(substr($file,0,1) == '/' || substr($file,0,7) == 'http://' || substr($file,0,8) == 'https://'){
            $this->response->addScript($file);
        }else{
            $this->response->addScript($this->template->getScriptURL().$file);
        }
        return $this;
    }
    
    /**
     * Add a CSS file to this block. CSS files are included in the order they are added. If the file begins with 'http://', 'https://' or '/',
     * then it is assumed that the file will be loaded from that URL. If only a file name is provided, then the file should be located in the template directory
     * under 'assets\css'.
     *
     * @return \Core\Module\Base\Anonymous\Block\Block Return value is $this for chainability
     * @param $url The name of the CSS file to include. File should be stored in the blocks 'assets\css\' directory
     */
    public function addStyle($file){
        if(substr($file,0,1) == '/' || substr($file,0,7) == 'http://' || substr($file,0,8) == 'https://'){
            $this->response->addStyle($file);
        }else{
            $this->response->addStyle($this->template->getStyleURL().$file);
        }
        return $this;
    }
    
    /**
     * Get the HTML output of this block
     * @return string HTML output of this block
     */
    public function getHTML(){
        return $this->getTemplate()->getHTML();
    }
    
    /**
     * Convert the object into an array with the important properties
     * @return array An array representation of this object
     */
    public function toArray(){
        $data = array();
        $data['template'] = $this->getTemplate()->toArray();
        return $data;
    }
}