<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Module\Home\Anonymous\Page;
use Core;
/**
 * The home page object for the admin
 *
 * @package \Core\Module\Home\Anonymous\Page\HomePage
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class HomePage extends Core\Module\Base\Anonymous\Page\Page{
    /**
     * The directory for the template of this page
     * @var string
     */
    private $templateDir;
    
    
    /**
     * Initialize this class, and set the templateDir
     */
    public function init(){
        $this->templateDir = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'view'.DIRECTORY_SEPARATOR.$this->getTemplate()->getTheme().DIRECTORY_SEPARATOR.'block'.DIRECTORY_SEPARATOR;
        
    }
    
    /**
     * Main output function for the public facing home page
     * @return null
     */
    public function index(){
        //Set the page template
        $this->getTemplate()->setTemplateDir($this->templateDir)->setFile('home.php');
        $this->setHeader($this->load->block('Header', array($this), 'Home'))->setFooter($this->load->block('Footer', array($this), 'Home'));
        //Build our layout
        $this->setLayout($this->load->library('Layout\Layout')
            ->setVar('testvar','valuefortestvar')
            ->addRow($this->load->library('Layout\Row')
                ->addCol($this->load->library('Layout\Column')
                    ->setContent($this->load->block('Body', array($this), 'Home')
                        ->setTemplateVar('varForTheTemplate', 'variable from the controller')
                    )
                    ->setColumnWidth(12)
                )
            )
        );
        
        $this->render();
    }
}
?>