<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Module\Base\Anonymous\Model;
use Core;
/**
 * The Model object is a system generic object for to be overloaded by module specific models
 *
 * This object provides basic functionality and data access for a model. It is a generic class and is intended
 * to be overloaded by module specific models. The overloaded class must define the $tableName property which describes the 
 * table from which data will be drawn using the $db property. The object will read from the modules config.php file, and ensure
 * that the table structure matches the table definition in the modules config.php.
 *
 * An overloaded class can redefine the $db property of this class in order to provide access to data in a different database
 *
 * @package \Core\Module\Base\Anonymous\Model\Model
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Model extends Core\Library\BaseObject{
    /**
     * The module's config object
     * @var \Core\Library\ModuleConfig
     */
    public $config;
    /**
     * The name of the table that this model is used to access
     * @var array
     */
    public $tableName;
    /**
     * This model's {@see \Core\Library\Database\DB} object. May be overloaded to provide access to another DB
     * @var \Core\Library\ModuleConfig
     */
    public $db; //Allow overloaded class to define a different DB object for this model;
    /**
     * This name of this model's module.
     *
     * Property will be set during init() and is the directory name at \modules\{modulname}
     * @var string 
     */
    public $module;
    /**
     * This model's {@see \Core\Library\Database\Table} object. Class uses overloaded $tableName property to get this property
     * @var \Core\Library\Database\Table
     */
    public $table;
    
    /**
     * Constructs a new instance. Calls the init method.
     * @see \Core\Module\Base\Anonymous\Model::init()
     * @return \Core\Module\Base\Anonymous\Model
     * @param \Core\Library\Registry $registry the global registry object. Included by the loader class
     * @param array $config When a model is created without a module, this is the config options that will be used in place of the config.php file.
     */
    public function __construct($tableName, $db=null, $config=null){
        $this->db = $db;
        $this->config = $config;
        //Ensure we have a tablename property
        if(empty($tableName)){
            throw new \Exception("Missing Required property '".get_class($this)."::tableName'");
        }else{
            $this->tableName = $tableName;
        }
    }
    
    /**
     * Initialize this instance.
     *
     * Sets the \Core\Module\Base\Anonymous\Model\Model::$module property, the \Core\Module\Base\Anonymous\Model\Model::$config property, the \Core\Module\Base\Anonymous\Model\Model::$tableConfig property.
     * Calls the \Core\Module\Base\Anonymous\Model\Model::updateTable() method to ensure that the database table structure is up to date
     *
     * @see \Core\Module\Base\Anonymous\Model::updateTable()
     * @return null
     */
    public function init(){
        
        //If we don't have a db object set, then use the one from the registry
        if(empty($this->db)){
            $this->db = $this->registry->db;
        }
        //Load the module and the module's config
        $this->config = $this->getConfig();
        
        //Load the table for this model
        $this->table = $this->db->getTable($this->tableName, $this);
    }
    
    /**
     * Uses the loader class to get path information about the overloaded class, and parses the path to determine the module's name from which this class is overloaded
     *
     * @return string The name of the module from which this class has been overloaded.
     */
    public function getModule(){
        if(empty($this->module)){
            if(get_class($this) == 'Core\Module\Base\Anonymous\Model\Model'){
                $modules = glob(CORE_DIR_MODULE.'*', GLOB_ONLYDIR);
                $conf = null;
                $moduleName = null;
                foreach($modules as $m){
                    $module = basename($m);
                    if($conf == null){
                        $conf = $this->load->library('ModuleConfig', array($module));
                    }else{
                        $conf->load($module);
                    }
                    if($conf->valueExists('tables')){
                        foreach($conf->tables as $table){
                            if($table['name'] == $this->tableName){
                                $this->module = $module;
                                break 2;
                            }
                        }
                    }
                }
            }else{
                $classInfo = $this->registry->load->getClassInfo(get_class($this));
                $parts = explode(DIRECTORY_SEPARATOR.'module'.DIRECTORY_SEPARATOR,$classInfo['file']);
                $parts = explode(DIRECTORY_SEPARATOR, array_pop($parts));
                $this->module = array_shift($parts);
            }
        }
        return $this->module;
    }
    
    /**
     * Get the config from the the module
     *
     * @return null
     */
    public function getConfig(){
        if(empty($this->config)){
            $this->print_array($this->trace());
            $this->module = $this->getModule();
            
            $this->config = $this->registry->load->library('ModuleConfig', array($this->module));
        }
        return $this->config;
    }
    
    /**
     * Get the unix timestamp of the last time the config was updated.
     *
     * @return int unix timestamp of the last time the config file was updated
     */
    public function getConfigLastUpdate(){
        return $this->getConfig()->getLastUpdate();
    }
    
    /**
     * Returns the {@see \Core\Library\Database\Table} object associated with this Model
     *
     * @return \Core\Library\Database\Table The table object for this Model.
     */
    public function getTable(){
        if(!empty($this->table)){
            if(empty($this->table->getModel())){
                $this->table->setModel($this);
            }
            return $this->table;
        }
        return false;
    }
    
    /**
     * Fetch a record by it's id and return a {@see \Core\Library\Database\Record} object.
     * 
     *
     * @return \Core\Library\Database\Record|false If the record can not be fetched, the function returns false
     * @param string|int $id The ID for the record to fetch
     * @param bool $loadRelationship Boolean value indicating if the Relationships defined in the modules config.php should be included in the return value
     */
    public function fetchRecord($id, $loadRelationship=false){
        return $this->table->fetchRecord($id, $loadRelationship);
    }
    
    /**
     * Function will return an iterable Recordset of all the rows in the table
     *
     * @return \Core\Library\Database\Recordset A Recordset object of all the records in this Model's table
     * @param bool $loadRelationships Boolean indicating if records should contain relationship data. defaults to false
     */
    public function fetchAll($loadRelationships=false){
        return $this->table->fetchAll($loadRelationships);
    }
    
    /**
     * Fetch an arbitrary record set from this Model's table according to the $queryData
     * 
     * @return \Core\Library\Database\Recordset The records requested
     * @param \Core\Library\Database\Query $query The query object to be executed
     * @param bool $loadRelationships Boolean indicating if relationship data should be fetched for the records. defaults to false.
     */
    public function fetchSet($query, $loadRelationships=false){
        return $this->table->fetchSet($query, $loadRelationships);
    }
}
?>