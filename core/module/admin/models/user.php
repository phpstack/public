<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Module\Admin\User\Anonymous\Model;
use Core;
/**
 * A module Model for access to the 'user' table
 *
 * @package \Core\Module\Admin\User\Anonymous\Model\User
 */
class User extends Core\Module\Base\Anonymous\Model\Model{
    /**
     * The name of the table this model is used for. Required
     * @var string
     */
    public $tableName='user';
    
    /**
     * Create a new instance of the User class
     *
     * @returnCore\Module\Admin\User\Anonymous\Model\User
     */
    public function __construct(){
        parent::__construct($this->tableName);
    }
}
?>