<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Module\Admin\User\Anonymous\Page;
use Core;
/**
 * The user page
 *
 * @package \Core\Module\Admin\User\Anonymous\Page\User
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class User extends Core\Module\Base\Anonymous\Page\Page{
    
    /**
     * Display the login page for the admin
     *
     * @return null
     */
    public function login(){
        if(!$this->user->isLoggedIn()){
            $this->response->setTitle('Login - phpStack Admin');
            
            $this->setHeader($this->load->block('Header', array($this), 'Admin'))
            ->setFooter($this->load->block('Footer', array($this), 'Admin'));
            //Build our layout
            $this->setLayout($this->load->library('Layout\Layout')
                ->addRow($this->load->library('Layout\Row')
                    ->addCol($this->load->library('Layout\Column')
                        ->setContent($this->load->block('Login',array($this), 'Admin')
                        )
                        ->setColumnWidth(12)
                    )
                )
            );
            
            $this->render();
        }else{
            return $this->request->redirect($this->load->library('Http\Action')->setPage($this->load->page('HomePage', array(), 'Home', true))->setMethod('index'));
        }
    }
    
    /**
     * Process a login attempt.
     *
     * Upon a successful login, if the user needs to have thier password reset, they will be routed to the password reset dialog.
     * Upon successfule password reset, they will be routed to either the home page, or the resource requested which landed them on the
     * login page.
     * @return null
     * 
     */
    public function processLogin(){
        if($this->request->server->REQUEST_METHOD == 'POST'){
            if(empty($this->request->post->username) || empty($this->request->post->password)){
                $this->request->session->message = "Username and Password are required";
                return $this->request->redirect($this->load->library('Http\Action')->setPage($this)->setMethod('login'));
            }elseif(!$this->user->login($this->request->post->username, $this->request->post->password)) {
                $this->request->session->message = "Invalid Login";
                return $this->request->redirect($this->load->library('Http\Action')->setPage($this)->setMethod('login'));
            }
            //The login was successful
            return $this->request->redirect($this->load->library('Http\Action')->setPage($this->load->page('HomePage', array(), 'Admin\Home', true))->setMethod('index'));
       }else{
            $this->request->session->message = "Please Log in";
            return $this->request->redirect($this->load->library('Http\Action')->setPage($this)->setMethod('login'));
       }
    }
    
    /**
     * Log out the currently logged in user
     * @return null
     */
    public function logout(){
        if($this->user->isLoggedIn()){
            $this->request->session->message = "Log in again";
            $this->user->logout();
        }
        return $this->request->redirect($this->load->library('Http\Action')->setPage($this)->setMethod('login'));
    }
    
    /**
     * Displays the password change dialog
     *
     * @return null
     */
    public function changePassword(){
        print "please change your password.";
    }
}
?>