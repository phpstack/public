<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Module\Admin\Home\Authenticated\Page;
use Core;
/**
 * The home page object for the admin
 *
 * @package \Core\Module\Admin\Home\Authenticated\Page\HomePage
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class HomePage extends Core\Module\Base\Anonymous\Page\Page{
    
    /**
     * Display the login page for the admin
     *
     * @return null
     */
    public function index(){
        if($this->user->isLoggedIn()){
            $this->setHeader($this->load->block('Header', array($this), 'Admin'))->setFooter($this->load->block('Footer', array($this), 'Admin'));
            //Build our layout
            $this->setLayout($this->load->library('Layout\Layout')
                ->addRow($this->load->library('Layout\Row')
                    ->addCol($this->menuLayout())
                    ->addCol($this->load->library('Layout\Column')
                        ->setContent(
                            $this->load->block('Home',array($this), 'Admin', true)
                        )
                        ->setColumnWidth(9)
                    )
                )
            );
            
            
            
            $this->render();
        }
    }
    
    /**
     *
     */
    protected function menuLayout(){
        $content = $this->load->library('Layout\Layout');
        $content->addRow(
            $this->load->library('Layout\Row')
            ->addCol(
                $this->load->library('Layout\Column')
                ->setColumnWidth(12)
                ->setContent('Logo Image')
            )
        )->addRow(
            $this->load->library('Layout\Row')
            ->addCol(
                $this->load->library('Layout\Column')
                ->setColumnWidth(12)
                ->setContent($this->load->block('Menu',array($this), 'Admin', true))
            )
        );
        $col = $this->load->library('Layout\Column')
        ->setContent($content)
        ->setColumnWidth(3);
        return $col;
    }

}
?>