<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Module\Admin\Authenticated\Block;
use Core;
/**
 * The header block for the admin
 *
 * @package \Core\Module\Admin\Authenticated\Block\Menu
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Menu extends Core\Module\Base\Anonymous\Block\Block{
    
    /**
     * Get the HTML output of this block
     * @return string HTML output for this block
     */
    public function getHTML(){
        $this->getTemplate()->setFile('menu.php');
        
        return parent::getHTML();
    }
}
?>