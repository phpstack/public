<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core\Module\Admin\Anonymous\Block;
use Core;
/**
 * The header block for the admin
 *
 * @package \Core\Module\Admin\Authenticated\Block\Header
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class Header extends Core\Module\Base\Anonymous\Block\Block{
    /**
     * Get the HTML output of this block
     * @return string HTML output of this block
     */
    public function getHTML(){
        $this->getTemplate()->setFile('header.php');
        
        
        $this->addStyle('normalize.css');
        $this->addStyle('bootstrap.min.css');
        $this->addStyle('font-awesome.min.css');
        $this->addStyle('themify-icons.css');
        $this->addStyle('flag-icon.min.css');
        $this->addStyle('cs-skin-elastic.css');
        $this->addStyle('style.css');
        $this->addStyle('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800');
        $this->addStyle('jqvmap.min.css');

        $this->addScript('jquery-2.1.4.min.js');
        $this->addScript('popper.min.js');
        $this->addScript('plugins.js');
        $this->addScript('main.js');
        $this->addScript('chart-js/Chart.bundle.js');
        $this->addScript('dashboard.js');
        $this->addScript('widgets.js');
        $this->addScript('vector-map/jquery.vmap.js');
        $this->addScript('vector-map/jquery.vmap.min.js');
        $this->addScript('vector-map/jquery.vmap.sampledata.js');
        $this->addScript('vector-map/jquery.vmap.world.js');
        
        $this->response->addMeta("viewport","width=device-width, initial-scale=1");
        
        return parent::getHTML();
    }
}
?>