<?php
/**
 * This file is part of phpStack
 *
 * phpStack is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any 
 * later version.
 * 
 * phpStack is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more 
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with phpStack.  
 * If not, see <https://www.gnu.org/licenses/>.
 *
 * @author Joshua Wagner <joshua.caleb.wagner@gmail.com>
 * @copyright Copyright (c) 2018-2019, Joshua Wagner
 * @license https://www.gnu.org/licenses/gpl-3.0-standalone.html GPL-3.0-or-later
 */
namespace Core;
/**
 * This is the main class of the framework.
 *
 * It should be included in the applications index.php file. It is responsible for initializing up the phpStack framework
 *
 * <b>Events</b>
 *
 * afterFrameworkInit       : triggered after the framework is ready, during the {@see \Core\phpStack::run()} method
 *
 *
 * @package \Core\phpStack
 * @author Joshua Wagner <jwagner@benningtonmarine.com>
 */
class phpStack{
    /**
     * The global registry class.
     * @var \Core\Library\Registry
     */
    public $registry;
    
    /**
     * The applications document root
     * @var string
     */
    private $docRoot;
    
    /**
     * An array for storage of the registered events
     * @var array
     */
    private $events=array();
    
    /**
     * Boolean indicating if a stack trace with timings should be recorded during execution
     * @var bool
     */
    private $recordTrace=false;
    
    /**
     * An array to hols the stack trace. Only used when {@see \Core\phpStack::recordTrace} is true
     * @var array
     */
    private $traceStack=array();
    
    /**
     * Create an instance of the phpStack framework.
     *
     * @return \Core\phpStacker
     */
    public function __construct($enableTrace=false){
        $this->recordTrace($enableTrace);
        $this->init();
    }
    
    /**
     * Configure the application
     *
     * Method will include the applications main config.php file, setup the PHP environment, and setup the phpStack environment
     * The global registry object will be setup in this method, and the afterRegistryInit method will be triggered.
     *
     * @see \Core\phpStack::initPHPEnvironment()
     * @see \Core\phpStack::initFrameworkEnvironment()
     * @see \Core\phpStack::registerEvents()
     *
     * @return null
     */
    public function init(){
        
        
        //Set up environment variables
        $this->initPHPEnvironment();
        $this->initFrameworkEnvironment();
        
        //Now we need is the registry to be setup
        $this->initRegistry();
        $this->registerEvents();
        
    }
    
    /**
     * Turn stack recording on or off. Returns $this for chainability
     * @return \Core\phpStack
     * @param bool A boolean indicating if trace recording should be enabled.
     */
    public function recordTrace($value){
        $this->recordTrace = (bool) $value;
        return $this;
    }
    
    /**
     * Determine if trace recording is enabled
     * @return bool boolean indicating if trace recording is enabled
     */
    public function isRecordingTrace(){
        return $this->recordTrace;
    }
    
    /**
     * Add a trace to the trace stack
     * @param string $callable The php callable method to add to the trace stack
     * @param array $params The parameters used when calling the $callable
     * @param int $elaps The amount of time in seconds that the $callable took to execute
     */
    public function addTrace($callable, $params, $elaps){
        $callableName = '';
        is_callable($callable, false, $callableName);
        $trace = array(
            'callable'=>$callableName,
            'elaps'=>$elaps,
            'params'=>array()
        );
        foreach($params as $param){
            if(is_object($param)){
                $trace['params'][] = get_class($param);
            }else{
                $trace['params'][] = $param;
            }
        }
        $this->traceStack[] = $trace;
    }
    
    /**
     * Get the code to be added to a class when stack trace is on
     * @return string The code to be added to make a class traceable
     * @param string $className The name of the class (without namespace) to be traced
     * @param string $namespace The namespace in which the class exists
     */
    public function getTraceClassCode($className, $namespace, $overloads, $codeFile){
        $classContent = file_get_contents($codeFile);
        $replaceFrom = array(
            '<?php',
            '<?',
            '?>',
            //'public function ',
            //'protected function ',
            //'private function __',
        );
        $replaceWith = array(
            '',
            '',
            '',
            //'private function ',
            //'private function ',
            //'public function __',
        );
        if(!empty($overloads)){
            foreach($overloads as $over){
                $replaceFrom[] = ' extends '.$over;
                $replaceWith[] = ' extends '.$over.'Trace';
            }
        }
        $classContent = str_replace($replaceFrom,$replaceWith, $classContent);
        $rtn = "
        ".(!class_exists($namespace.'\\'.$className) ? $classContent : '')."
        namespace ".$namespace.";
        class ".$className."Trace extends ".$className."{
            public function __call(\$functionName, \$args=array()){

                \$start = microtime(true);
                \$rtn = call_user_func_array(array(\$this, \$functionName),\$args);
                \$end = microtime(true);
                \$this->phpStack->addTrace(array(\$this, \$functionName), \$args, (\$end-\$start));
                return \$rtn;
            }
        }
        
        
        ";
        return $rtn;
    }
    
    /**
     * Method defines the framework events
     *
     * @return null
     */
    public function registerEvents(){
        $this->registry->event->registerEvent('afterFrameworkInit', \Core\Library\Event::CONTEXT_GLOBAL);
    }
    
    /**
     * Sets the PHP environment variables needed for the framework
     *
     * @return null
     */
    private function initPHPEnvironment(){
        ini_set('memory_limit', '1G');
        set_time_limit(300);
    }
    
    /**
     * Sets up the framework environment, including the registry
     *
     * @see \Core\phpStack::initRegistry();
     * 
     */
    private function initFrameworkEnvironment(){
        //Start up the PHP session
        @session_start();
        
        $siteConfig = $this->getDocRoot().'config.php';
        if(file_exists($siteConfig)){
            include_once($siteConfig);
        }else{
            throw new \Exception("Application is missing a config file at '".$siteConfig."'");
        }
        
        /** The document root for this project. Set in startup.php */
        define('DIR_ROOT', $this->getDocRoot());
        /** The root URL for this project */
        define('HTTP_ROOT', $_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME']."/");
        
        /** The root of the core system */
        define('CORE_DIR_ROOT', DIR_ROOT.'core'.DIRECTORY_SEPARATOR);
        /** The system database directory for this project */
        define('CORE_DIR_DATABASE', CORE_DIR_ROOT.'database'.DIRECTORY_SEPARATOR);
        /** The core http directory for this project */
        define('CORE_DIR_HTTP', CORE_DIR_ROOT.'http'.DIRECTORY_SEPARATOR);
        /** The core library directory for this project. */
        define('CORE_DIR_LIBRARY', CORE_DIR_ROOT.'library'.DIRECTORY_SEPARATOR);
        /** The core module directory for this project */
        define('CORE_DIR_MODULE', CORE_DIR_ROOT.'module'.DIRECTORY_SEPARATOR);
        /** The core storage directory for this project */
        define('CORE_DIR_STORAGE', CORE_DIR_ROOT.'storage'.DIRECTORY_SEPARATOR);
        
        
        /** The root of the application system */
        define('APP_DIR_ROOT', DIR_ROOT.'application'.DIRECTORY_SEPARATOR);
        /** The system database directory for this project */
        define('APP_DIR_DATABASE', APP_DIR_ROOT.'database'.DIRECTORY_SEPARATOR);
        /** The core http directory for this project */
        define('APP_DIR_HTTP', APP_DIR_ROOT.'http'.DIRECTORY_SEPARATOR);
        /** The core library directory for this project. */
        define('APP_DIR_LIBRARY', APP_DIR_ROOT.'library'.DIRECTORY_SEPARATOR);
        /** The core module directory for this project */
        define('APP_DIR_MODULE', APP_DIR_ROOT.'module'.DIRECTORY_SEPARATOR);
        /** The core storage directory for this project */
        define('APP_DIR_STORAGE', APP_DIR_ROOT.'storage'.DIRECTORY_SEPARATOR);
    }
    
    /**
     * Method sets up the standard global registry
     *
     * @return null
     */
    private function initRegistry(){
        if($this->isRecordingTrace()){
            eval($this->getTraceClassCode('BaseObject','Core\Library', array(), CORE_DIR_LIBRARY.'object.php'));
        }else{
            if(file_exists(CORE_DIR_LIBRARY.'object.php')){
                include_once(CORE_DIR_LIBRARY.'object.php');
            }
        }
        
        
        
        //Create the Registry to store global objects and place a reference to itself in itself
        if($this->isRecordingTrace()){
            eval($this->getTraceClassCode('Registry','Core\Library', array(), CORE_DIR_LIBRARY.'registry.php'));
            $this->registry = new \Core\Library\RegistryTrace();
        }else{
            require_once(CORE_DIR_LIBRARY.'registry.php');
            $this->registry = new \Core\Library\Registry();
        }
        
        $this->registry->set('registry',$this->registry);
        
        //Add ourself to the registry
        $this->registry->set('phpStack',$this);
        //Load the error class
        $this->loadLibraryToRegistry('Error', array(), 'error', 'error.php', 'Core\Library', array('Core\Library\BaseObject'));
        
        //Load the cache before the loader,since the loader will use the cache
        $this->loadLibraryToRegistry('Cache', array(), 'cache', 'cache.php', 'Core\Library', array('Core\Library\BaseObject'));
        
        //Add the loader to the registry
        $this->loadLibraryToRegistry('Loader', array($this->registry), 'load', 'loader.php', 'Core\Library', array('Core\Library\BaseObject'));
        
        //Place the utility functions into the registry
        $this->registry->set('utility', $this->registry->load->library('Utility'));
       
        //Load the database class
        $this->registry->set('db',$this->registry->load->database('DB', array(DB_DRIVER)));
        
        //Load the events class
        $this->registry->set('event',$this->registry->load->library('Event'));
        
        //Load the URL class into the registry
        $this->registry->set('url',$this->registry->load->library('Http\URL'));
        
        //Load the HTTP Request class
        $this->registry->set('request',$this->registry->load->library('Http\Request'));
        
        //Load the HTTP Response class
        $this->registry->set('response',$this->registry->load->library('Http\Response'));
        
        //Load the user class into the registry
        $this->registry->set('user',$this->registry->load->library('User'));
    }
    
    /**
     * A basic loader method that is used to get the framework up and running. Should only be used until the loader class is ready
     * @return null;
     * @param string $class The name of the class to construct
     * @param array $params Parameters to pass to the constructor
     * @param string $registryKey The key in the registry that the new class will be placed in
     * @param string $filename The name of the file in CORE_DIR_LIBRARY which contains the class
     * @param string $namespace The namespace to which this class belongs
     * @param array $overloads The class(es) that this object extends
     */
    private function loadLibraryToRegistry($class, $params, $registryKey, $filename, $namespace, $overloads){
        if(empty($this->registry->load)){
            $className = $namespace.'\\'.$class;
            if($this->isRecordingTrace()){
                eval($this->getTraceClassCode($class,$namespace, $overloads, CORE_DIR_LIBRARY.$filename));
                $className = $className.'Trace';
            }else{
                require_once(CORE_DIR_LIBRARY.$filename);
            }
            $reflect = new \ReflectionClass($className);
            $newClass = $reflect->newInstanceArgs($params);
            
            
            if(method_exists($newClass,'setRegistry') && is_callable(array($newClass,'setRegistry'))){
                call_user_func_array(array($newClass,'setRegistry'),array($this->registry));
            }
            if(is_callable(array($newClass,'parent::init'))){
                call_user_func_array(array($newClass,'parent::init'),array());
            }
            if(method_exists($newClass,'init') && is_callable(array($newClass,'init'))){
                call_user_func_array(array($newClass,'init'),array());
            }
            $this->registry->set($registryKey, $newClass);
        }else{
            throw new \Exception("Method '".get_class($this)."::loadLibraryToRegistry()' should only be used until the loader is available.");
        }
    }
    
    /**
     * Gets the document root directory for this application, with a trailing slash
     *
     * @return string The document root directory for this application, with a trailing slash
     */
    public function getDocRoot(){
        if(empty($this->docRoot)){
            if(!defined("DIR_ROOT")){
                $this->docRoot = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR;
            }else{
                $this->docRoot = DIR_ROOT;
            }
        }
        return $this->docRoot;
    }
    
    
    /**
     * Begins the execution of the framework, by triggering the afterFrameworkInit event,
     * and then processing the request with the {@see \Core\Library\Http\Request::receive()} method.
     *
     * @return \Core\Library\Http\Response The populated response object as returned by the request
     */
    public function run(){
        $this->registry->event->trigger('afterFrameworkInit',\Core\Library\Event::CONTEXT_GLOBAL);
        $this->registry->request->process();
        return $this->registry->response;
    }
}